var path      = require("path");
var key    = require(path.join(__dirname, '..', 'config', 'app.json'))['secret'];

var jwt = require('jsonwebtoken');

exports.auth = function(req, res, next) 
{
	var token = req.headers['x-access-token'];

	if (token) {
		jwt.verify(token, key, function(err, decoded) {      
			if (err) {
				return res.json({ success: false, message: 'Failed to authenticate token.' });    
			} else {
				req.decoded = decoded; 
				next(); 
			}
		});

	} else {

		return res.status(403).send({ 
			message: 'Login required.' 
		});

	}
};