exports.notEmpty = function(req, res, next) 
{
	if (req.session.cart.length > 0)
	{
		var currentCart = req.session.cart;

		var total = 0;

		for (var i = currentCart.length - 1; i >= 0; i--) 
		{
			total += currentCart[i].qty * currentCart[i].price;
		}

		req.summary = {
			total: total,
			shippingCost: 15000, 
			payment: 'Transfer', 
			code: null, 
			eta: '1 - 2 minggu '
		}

		return next();
	}
	else
	{
		req.flash('error', 'Keranjang belanja masih kosong');
		return res.redirect('back');
	}

};