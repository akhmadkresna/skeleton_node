'use strict';
var models  = require(__dirname+'/../model');
var jwt = require('jsonwebtoken');
//for super admin
exports.isAuthed = function(req, res, next) 
{
    req.session['visitURL'] = req.protocol + '://' + req.get('host') + req.originalUrl;

    if (req.isAuthenticated())
        return next();

    return res.redirect('/administrator/login');
};
//for user
exports.isUserAuthed = function(req, res, next)
{
    req.session['visitURL'] = req.protocol + '://' + req.get('host') + req.originalUrl;
    console.log("is auted :",req.isAuthenticated())
    if (req.isAuthenticated())
        return next();
    return res.redirect('/login');
};

//check if user is not logged in, redirect to desired url if not
exports.isGuest = function(req, res, next) 
{
    if (!req.isAuthenticated())
        return next();
    
    return res.redirect('/administrator');
};

exports.JWTAuth = function(req, res, next) 
{
    var token = req.headers['x-access-token'];

    var errObj = new Object();

    if (token) {
        jwt.verify(token, global.APP_CONFIGS.app.secret, function(err, decoded) {      
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });    
            } else {
                req.decoded = decoded; 
                models.User.count({
                    where: {
                        id: req.decoded.id,
                        suspended: true
                    }
                })
                .then(function (user) {
                    if (user>0)
                    { 
                        throw {
                            type: 'suspension',
                            message: 'user suspended'
                        }
                    }

                    var searchq = new Object();

                    searchq.finished = 1;
                    searchq['userId'] = req.decoded.id;
                    searchq['reviewDate'] = null;

                    return models.Reservation.findAll({
                        where: searchq,
                        attributes: ['code']
                    });
                })
                .then(function (trips) {
                    var currRoute = req.originalUrl.split('/')[3];

                    var pars = currRoute.indexOf('?');
                    
                    if (currRoute.substring(0, (pars != -1)?pars:currRoute.length) == 'trip') 
                        return next();

                    if (trips.length>0)
                    { 
                        throw {
                            type: 'review',
                            trips: trips
                        }
                    }

                    return next(); 
                })
                .catch(function (err) {
                    if (err.type == 'suspension') 
                        return res.status(403).json({
                            message: 'user suspended'
                        })
                    else if (err.type == 'review') 
                        return res.status(403).json({
                            message: 'review needed',
                            trips: err.trips
                        })
                });

            }
        });

    } else {
        return res.status(403).send({ 
            message: 'Login required.' 
        });
    }
};
