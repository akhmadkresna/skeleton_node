exports.isDev = function(req, res, next) 
{
	if (process.env.NODE_ENV == 'production') 
	{
		return res.redirect('/whoops/404');
	}
	else
	{
		next();
	}
}