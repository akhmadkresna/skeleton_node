'use strict';

exports.formatJsonResponse = function(success, message, result) {
	if (typeof message=='string') {
		var param = (success)?'success':'error';
		message = [{
			param 	: param,
			msg 	: message,
			value	: ''
		}];
	}

	if (typeof result=='undefined' || result == null)
		result = {};

	return { success : success, message : message, result : result };
}