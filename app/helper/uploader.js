'use strict';
var aws = require('aws-sdk');
aws.config = global.APP_CONFIGS.aws;

var multer = require('multer');
var multerS3 = require('multer-s3');

var s3 = new aws.S3()
var models  = require(__dirname+'/../model');

exports.listing = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'roomme-dev',
		key: function (req, file, cb) {
			models.Listing.findOne({
				where: {
					userId: req.decoded.id,
					id: req.params.listing
				},
				attributes: ['id', 'createdAt']
			}).then(function (listing) {
				if (typeof req.identifier == 'undefined') 
					req.identifier = String(new Date().getTime());

				var part1 = new Buffer(String(req.decoded.email)).toString('base64');

				var part2 = (req.body.identifier == null)?req.params.identifier:req.body.identifier;

				var part3 = req.identifier;

				req['filename'] = part1+'/'+part2+'/'+part3+'.jpg'

				cb(null, req.filename);
			});
		}
	})
});

exports.profile = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'roomme-dev',
		key: function (req, file, cb) {
			req.identifier = String(new Date().getTime());

			var part1 = new Buffer(String(req.decoded.email)).toString('base64');

			var part2 = '/'+req.identifier+'.jpg';

			cb(null, part1+part2);
		}
	})
});

exports.verification = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'roomme-dev',
		key: function (req, file, cb) {
			req.identifier = String(new Date().getTime());
			
			var part1 = new Buffer(String(req.decoded.email)).toString('base64');

			var part2 = '/verification/'+req.identifier+'.jpg';

			cb(null, part1+part2);
		}
	})
});

exports.payment = multer({
	storage: multerS3({
		s3: s3,
		bucket: 'roomme-dev',
		key: function (req, file, cb) {
			cb(null, req.filename);
		}
	})
});
	// return module.exports;