'use strict';
const promiseReq = require('request-promise')
, config = require('./../config/'+((process.env.NODE_ENV==='local-dev')?'local-dev/':'')+'nexmo')[process.env.NODE_ENV];

module.exports ={
	textNexmo(recipient, text){
		var options = {
			method: 'POST',
			uri: 'https://rest.nexmo.com/sms/json',
			body: {
				'api_key': config.key,
				'api_secret': config.secret,
				'to': recipient,
				'from': "ROOMME",
				'text': text
			},
			json: true
		};

		return promiseReq(options)
	}

}