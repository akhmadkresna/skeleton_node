var nodemailer = require('nodemailer');

var mailConfig = global.APP_CONFIGS.email.credential;

var smtpTransport = nodemailer.createTransport("SMTP", mailConfig.gmail);

var ejs = require('ejs');

var smsConfig = global.APP_CONFIGS.sms;

var viewLocation = __dirname+'/../../resource/view/emails/';
var fs = require('fs');

var http = require("http");
var https = require("https");


exports.email = function(packet, options) 
{
	var sender = '';

	if (typeof options != 'undefined' && (options.from != null || typeof options.from != 'undefined')) 
		sender = options.from;

	var success = 0
	var fail = 0;

	for (var i = packet.length - 1; i >= 0; i--) 
	{
		if (packet[i].template != null) 
		{
			var template = fs.readFileSync(viewLocation+packet[i].template+'.ejs', 'utf8'); 	
			var html = ejs.render(template, packet[i].data);
		}
		else
		{
			var html = packet[i].content;
		}

		var mailOptions = {
			from: (sender != '')?sender:mailConfig.alias,
			to: packet[i].to,
			subject: packet[i].subject,
			html: html
		};

		smtpTransport.sendMail(mailOptions, function(error, response) {
			if (error) 
			{
				console.log(error)
				fail += 1;
				mailOptions.sent = false;
			}
			else 
			{
				success += 1;
				mailOptions.sent = false;
			}
		});

	}

	return [success, fail];

}

exports.sms = function(message, receiver) 
{
	if (message.length > 160) 
		return false;

	var qobject = { 
		userkey: smsConfig.userkey, 
		passkey: smsConfig.passkey, 
		nohp: receiver, 
		pesan: message
	};

	var target = smsConfig.endpoint+'?'+queryStringify(qobject);

	function queryStringify(obj) {
		var str = [];
		for(var p in obj)
			if (obj.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
			return str.join("&");
		}

	return https.get( target, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
        	return true
            // Data reception is done, do whatever with it!
            // var parsed = JSON.parse(body);
            // callback({
            //     email: parsed.email,
            //     password: parsed.pass
            // });
        });
    });
}