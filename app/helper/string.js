exports.dokuWords = function (amount, code) {
	var dokuConfig = global.APP_CONFIGS.doku;

	var crypto = require('crypto')
	var shasum = crypto.createHash('sha1');

	shasum.update(String(amount)+'.00'+dokuConfig.sharedKey+code);
	return shasum.digest('hex');
}

exports.money = function (amount, currency) 
{
	var numeral = require('numeral');
	
	return currency+' '+numeral(amount).format('0,0');
}

exports.transferCode = function(identifier, lastcode) 
{
	if (lastcode == null) 
		return identifier+'001';
	else
	{
		var awal = ['00', '0', ''];
		var nextnum = parseInt(lastcode) + 1;

		var newindex = '';

		return identifier+awal[(String(nextnum).length) - 1]+String(nextnum);
	}
};

exports.formatedDate = function(dateObjt) 
{
	var year = dateObjt.getFullYear();
	var month = dateObjt.getMonth();
	var day = dateObjt.getDate();
	var hour = dateObjt.getHours();
	var minute = dateObjt.getMinutes();

	var monthList = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
	var formated = day+" "+monthList[month]+" "+year+', '+hour+':'+minute;
	return formated;
}

exports.random = function(length, reps) 
{
	var result = '';
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

	for (var i = 0; i < (length*reps); i++) 
	{
		result += chars[Math.floor(Math.random() * chars.length)];

		if (i==(length-1) && i+1!=length) 
		{
			result+= '-';
		}
	}

	return result;
};

exports.bookCode = function(identifier, lastcode) 
{
	if (lastcode == null) 
		return identifier+'001';
	else
	{
		var awal = ['00', '0', ''];
		var nextnum = parseInt(lastcode) + 1;

		var newindex = '';

		return identifier+awal[(String(nextnum).length) - 1]+String(nextnum);
	}
};


exports.mysqlDate = function(year, month, day) 
{
	if (month.length == 1) 
		month = "0" + month;
	
	if (day.length == 1)
		day = "0" + day;
	
	return year + "-" + month + "-" + day;
}

exports.nameInitial = function(first, second) 
{
	if (second == null || second == '') 
		return first.substr(0 , 1)+first.substr(0, 2);
	else
		return first.substr(0 , 1)+second.substr(0, 2);
}

exports.getDDMMYY = function(year, month, day) 
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;

	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+String(dd)
	} 
	if(mm<10){
		mm='0'+String(mm)
	} 
	return today = dd+mm+String(yyyy).substr(2, 4);
}

exports.reverser = function (string) {
	var o = '';
	for (var i = string.length - 1; i >= 0; i--)
		o += string[i];
	return o;
}

exports.listingImage = function (email, identifier, number) {
	var part1 = new Buffer(String(email)).toString('base64');

	var part2 = identifier

	var part3 = number;
}

exports.tripStatus = function (trip) {
	if (trip.canceled) { 
		return "Canceled";
	} else if (trip.declined) { 
		return "Declined";
	} else if ( trip.finished && trip.reviewDate != null) {
		return "Completed";
	} else if ( trip.finished ) {
		return "Finished";
	} else if ( trip.approved && trip.paid && (trip.startDate <= Date.now())) {
		return "On-going";
	} else if (trip.approved && trip.paid && (trip.startDate > Date.now()) ) {
		return "Paid";
	} else if (trip.approved && !trip.paid ) {
		return "Accepted";
	} else if (!trip.approved) { 
		return "Pending";
	}
}