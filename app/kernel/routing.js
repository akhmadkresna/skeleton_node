module.exports = function(app, express, router, appRoutes) {

	//ADD NEW ROUTE GROUP INSIDE JS OBJECT, USE TEMPLATE AS WRITTEN HERE

	var routeGroups = [
	{
		name: 'cms',
		prefix: 'administrator'
	},
	{
		name: 'apiv1',
		prefix: 'api/v1'
	},
	{
		name: 'apiv2',
		prefix: 'api/v2'
	},
	{
		name: 'open',
		prefix: ''
	}
	];

	//DONT CHANGE LINES OF CODE BELOW
	var mdlwrName = '';

	for (var i = routeGroups.length - 1; i >= 0; i--) 
	{
		appRoutes[routeGroups[i].name] = express.Router();

		app.use('/'+routeGroups[i].prefix, appRoutes[routeGroups[i].name]);

		appRoutes[routeGroups[i].name].theMiddlewares = [];
	}
	exports.appRoutes = appRoutes;

}