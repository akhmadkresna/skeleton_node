module.exports = function(app) {
	var lusca = require('lusca');
	app.use(lusca(global.APP_CONFIGS.security));
	app.disable('x-powered-by');
}