module.exports = function(app) {
	var models  = require(__dirname+'/../model');

	var sender  = require(__dirname+'/../helper/sender');
	var push  = require(__dirname+'/../helper/push');

	var CronJob = require('cron').CronJob;

	var dummy = (process.env.NODE_ENV!='production');

	var cancelTrip = new CronJob({
		cronTime: '1 1 * * * *',
		onTick: function() {

			var codes = [];

			var tokens = [];

			models.Reservation.findAndCountAll({
				where: {
					paid: 0,
					finished: 0,
					canceled:0,
					createdAt: {
						$lte : models.sequelize.literal('DATE_SUB(NOW(), INTERVAL 24 HOUR)')
					}
				},
				include: [{
					model: models.User,
					as: 'Traveller',
					attributes: ["device", "pushToken"]
				}]
			})
			.then(function (trips) {
				console.log(trips.count+' trips canceled');

				for (var i = trips.rows.length - 1; i >= 0; i--)
				{
					codes.push(trips.rows[i].code)

					if (trips.rows[i].Traveller.device == 'ios') 
					{
						if (tokens.indexOf(trips.rows[i].pushToken == -1)) 
							tokens.push(trips.rows[i].pushToken)
					}
				}
				
				return models.Reservation.update({
					canceled: 1
				}, {
					where: {
						code: {
							$in: codes
						}
					}
				})
			})
			.then(function (canceled) {
				if (tokens.length > 0) 
					var test =  push.pushIOS(tokens, 'Your trip(s) are canceled due to payment condition', {});

				return true;

				console.log('trips canceled!!!')
			})
		},
		start: false,
		timeZone: 'Asia/Jakarta'
	});
	cancelTrip.start();

	var finishTrip = new CronJob({
		cronTime: '5 1 14 * * *',
		onTick: function() {

			var codes = [];

			var tokens = [];

			models.Reservation.findAndCountAll({
				where: {
					finished: 0,
					paid: 1,
					canceled: 0,
					endDate: {
						$lte : models.sequelize.fn('NOW')
					}
				},
				include: [{
					model: models.User,
					as: 'Traveller',
					attributes: ["device", "pushToken"]
				}]
			}).then(function (trips)
			{
				console.log(trips.count+' trips finished');


				for (var i = trips.rows.length - 1; i >= 0; i--)
				{
					codes.push(trips.rows[i].code)

					if (trips.rows[i].Traveller.device == 'ios') 
					{
						if (tokens.indexOf(trips.rows[i].pushToken == -1)) 
							tokens.push(trips.rows[i].pushToken)
					}
				}
				
				return models.Reservation.update({
					finished: 1
				}, {
					where: {
						code: {
							$in: codes
						}
					}
				})
			})
			.then(function (success) {

				if (tokens.length > 0) 
					var test =  push.pushIOS(tokens, 'You have finished your trip, please kindly write a review', {});

				return true;

				console.log('trips finished!!!')
			})
		},
		start: false,
		timeZone: 'Asia/Jakarta'
	});
	finishTrip.start();

	// var checkoutReminder = new CronJob('* * * * * *', function()
	// {
	// 	models.Reservation.findAndCountAll({
	// 		where: {
	// 			finished: 0,
	// 			paid: 1,
	// 			canceled: 0,
	// 			endDate: models.sequelize.fn('NOW')
	// 		}
	// 	}).then(function (trips)
	// 	{
	// 		console.log(trips.count+' trips finished');

	// 		var codes = [];

	// 		for (var i = trips.rows.length - 1; i >= 0; i--)
	// 		{
	// 			codes.push(trips.rows[i].code)
	// 		}

	// 		return models.Reservation.update({
	// 			finished: 1
	// 		}, {
	// 			where: {
	// 				code: {
	// 					$in: codes
	// 				}
	// 			}
	// 		})
	// 	}).then(function (success) {
	// 		console.log('trips finished!!!')
	// 	})

	// }, function ()
	// {
	// 	console.log('malah berhenti');
	// },
	// true
	// );

	// var finishTrip = new CronJob('* * * * * *', function()
	// {
	// 	models.Reservation.findAndCountAll({
	// 		where: {
	// 			finished: 0,
	// 			paid: 1,
	// 			canceled: 0,
	// 			endDate: {
	// 				$lte : models.sequelize.literal('CURDATE()')
	// 			}
	// 		}
	// 	}).then(function (trips)
	// 	{
	// 		console.log(trips.count+' trips finished');

	// 		var codes = [];

	// 		for (var i = trips.rows.length - 1; i >= 0; i--)
	// 		{
	// 			codes.push(trips.rows[i].code)
	// 		}

	// 		return models.Reservation.update({
	// 			finished: 1
	// 		}, {
	// 			where: {
	// 				code: {
	// 					$in: codes
	// 				}
	// 			}
	// 		})
	// 	}).then(function (success) {
	// 		console.log('trips finished!!!')
	// 	})

	// }, function ()
	// {
	// 	console.log('malah berhenti');
	// },
	// true
	// );

	// var expireResponseTrip = new CronJob('* * * * * *', function()
	// {
	// 	models.Reservation.findAndCountAll({
	// 		where: {
	// 			finished: 0,
	// 			paid: 0,
	// 			approved: 0,
	// 			canceled: 0,
	// 			endDate: {
	// 				$lte : models.sequelize.literal('CURDATE()')
	// 			}
	// 		}
	// 	}).then(function (trips)
	// 	{
	// 		console.log(trips.count+' trips finished');

	// 		var codes = [];

	// 		for (var i = trips.rows.length - 1; i >= 0; i--)
	// 		{
	// 			codes.push(trips.rows[i].code)
	// 		}

	// 		return models.Reservation.update({
	// 			finished: 1
	// 		}, {
	// 			where: {
	// 				code: {
	// 					$in: codes
	// 				}
	// 			}
	// 		})
	// 	}).then(function (success) {
	// 		console.log('trips finished!!!')
	// 	})

	// }, function ()
	// {
	// 	console.log('malah berhenti');
	// },
	// true
	// );

	// var finishTrip = new CronJob('* * * * * *', function()
	// {
	// 	console.log(12)

	// }, function ()
	// {
	// 	console.log('malah berhenti');
	// },
	// true
	// );

	
}
