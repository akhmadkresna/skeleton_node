module.exports = function(app) {

	var session = require('express-session');
	var flash    = require('connect-flash');
	app.use(flash());
	

	app.use(session({ 
		secret: global.APP_CONFIGS.app.secret,
		resave: true,
		saveUninitialized: true
	}));

}