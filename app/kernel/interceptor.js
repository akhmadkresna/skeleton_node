module.exports = function(app) {

	app.use(function (req, res, next) {
		req.baseurl = req.protocol + '://' + req.get('host'); //get base url

		var postBody = req.flash('postBody')[0];
		
		//intercept post data
		if (postBody != null) 
			app.locals.body = postBody

		if (req.query!=null) 
			app.locals.query = req.query

		app.locals.errMessage = req.flash('formError');

		app.locals.alerts = {
			success: null,
			error: null
		}
		//end of intercept post data
		
		//intercept main flash message
		app.locals.alerts.success = req.flash('success')[0];

		app.locals.alerts.error = req.flash('error')[0];
		
		
		//intercept user and cart data
		return next();
	});
}