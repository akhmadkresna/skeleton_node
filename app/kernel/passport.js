module.exports = function(passport, app) {

    var LocalStrategy   = require('passport-local').Strategy;

    var models  = require(__dirname+'/../model');

    passport.serializeUser(function(user, done) {
        return done(null, user);
    });

    passport.deserializeUser(function(User, done) {
        models.Admin.findById(User.id, {
            attributes: ["id", "groupId", "fname", "lname", "email"]
        }).then(function (admin) {
            if (admin){
                return done(null, admin.dataValues);
            }
            return models.User.findById(User.id, {
                attributes: ["id", "picture", "firstName", "lastName", "email"]
            }).then(function (user) {
                if (user) {
                    return done(null, user.dataValues);
                }
                return done(null, false, req.flash('loginMessage', 'No user found.'));
            }).catch(function (err) {
                return done(err, null);
            });
        }).catch(function (err) {
            return done(err, null);
        });
    });

    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) { 

        models.Admin.findOne({
            where: {
                email: email
            },
            attributes: ['id', 'email', "password"]
        }).then(function (admin) {
            if (!admin)
                return done(null, false, req.flash('loginMessage', 'No user found.'));

            if (!admin.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

            delete admin.dataValues.password;

            return done(null, admin.dataValues);

        }).catch(function (err) {
            return done(err);
        });
    }));

    passport.use('local-user-login', new LocalStrategy({
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {

            models.User.findOne({
                where: {
                    email: email
                },
                attributes: ['id', 'email', "password"]
            }).then(function (user) {
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));

                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

                delete user.dataValues.password;

                return done(null, user.dataValues);

            }).catch(function (err) {
                return done(err);
            });
        }));

    app.use(passport.initialize());
    app.use(passport.session());
};
