'use strict';

module.exports = function(appRoutes, ctrl, passport, router, mdlwr, uploader) {

	//rotues for v2 api
	appRoutes.apiv2.post('/register', ctrl.APIV2.Register.store);

	appRoutes.apiv2.post('/register/fb', ctrl.APIV2.Social.facebookRegister);
	appRoutes.apiv2.post('/register/google', ctrl.APIV2.Social.googleRegister);
	
	appRoutes.apiv2.post('/activation/phone', ctrl.APIV2.ActivationPhone.store);
	appRoutes.apiv2.put('/activation/phone', ctrl.APIV2.ActivationPhone.update);

	appRoutes.apiv2.get('/notification/host/booking', mdlwr.Auth.JWTAuth, ctrl.APIV2.HostNotification.reservation);
	appRoutes.apiv2.get('/host/notification/booking', mdlwr.Auth.JWTAuth, ctrl.APIV2.HostNotification.reservation);

	appRoutes.apiv2.put('/host/trip/:code/finish', mdlwr.Auth.JWTAuth, ctrl.APIV2.TripHost.finish);
	//end

	//routes for cms
	appRoutes.cms.get('/login', ctrl.CMS.Auth.show);

	appRoutes.cms.post('/login', passport.authenticate('local-login', {
        successRedirect : '/administrator', // redirect to the secure profile section
        failureRedirect : '/administrator/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
	
	appRoutes.cms.get('/forgot-password', ctrl.CMS.Password.getForgot);
	// appRoutes.cms.post('/forgot-password', ctrl.CMS.Password.postForgot);
	
	appRoutes.cms.get('/reset-password', ctrl.CMS.Password.getReset);
	// appRoutes.cms.post('/reset-password', ctrl.CMS.Password.postReset);


	// appRoutes.cms.get('/whoops/403', function (req, res) {
	// 	return res.render('error/403', {layout: 'error/master'});
	// });

	appRoutes.cms.get('/whoops/404', function (req, res) {
		return res.render('CMS/error/404', {layout: 'CMS/layout/singleform'});
	});

	appRoutes.cms.get('/whoops/500', function (req, res) {
		return res.render('CMS/error/500', {layout: 'CMS/layout/singleform'});
	});
	
	appRoutes.cms.get('/logout', mdlwr.Auth.isAuthed, ctrl.CMS.Auth.destroy);

	appRoutes.cms.get('/', mdlwr.Auth.isAuthed, ctrl.CMS.Dashboard.index);

	//user routes
	appRoutes.cms.get('/member', mdlwr.Auth.isAuthed, ctrl.CMS.Member.index);

	appRoutes.cms.get('/member/create', mdlwr.Auth.isAuthed, ctrl.CMS.Member.create);
	appRoutes.cms.post('/member', mdlwr.Auth.isAuthed, ctrl.CMS.Member.store);

	appRoutes.cms.get('/member/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Member.edit);
	appRoutes.cms.post('/member/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Member.update);
	
	appRoutes.cms.get('/member/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Member.show);
	appRoutes.cms.post('/member/:id/suspend', mdlwr.Auth.isAuthed, ctrl.CMS.Member.suspend);
	appRoutes.cms.get('/member/:id/unsuspend', mdlwr.Auth.isAuthed, ctrl.CMS.Member.unsuspend);
	appRoutes.cms.get('/member/:id/verification/:idverif/approve', mdlwr.Auth.isAuthed, ctrl.CMS.Verification.approve);
	appRoutes.cms.get('/member/:id/verification/:idverif/decline', mdlwr.Auth.isAuthed, ctrl.CMS.Verification.decline);

	// //admin routes
	appRoutes.cms.get('/admins', mdlwr.Auth.isAuthed, ctrl.CMS.Admin.index);
	
	appRoutes.cms.get('/admins/create', mdlwr.Auth.isAuthed, ctrl.CMS.Admin.create);
	appRoutes.cms.post('/admins', mdlwr.Auth.isAuthed, ctrl.CMS.Admin.store);
	
	// appRoutes.cms.get('/admins/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Admin.show);
	
	// appRoutes.cms.get('/admins/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Admin.edit);
	// appRoutes.cms.put('/admins/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Admin.update);

	// //log routes
	appRoutes.cms.get('/log', mdlwr.Auth.isAuthed, ctrl.CMS.Log.index);
	// appRoutes.cms.get('/log/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Log.show);

	//article routes
	appRoutes.cms.get('/articles', mdlwr.Auth.isAuthed, ctrl.CMS.Article.index);
	appRoutes.cms.get('/articles/create', mdlwr.Auth.isAuthed, ctrl.CMS.Article.create);
	appRoutes.cms.get('/articles/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Article.show);
	appRoutes.cms.get('/articles/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Article.edit);
	appRoutes.cms.post('/articles/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Article.update);
	appRoutes.cms.post('/articles', mdlwr.Auth.isAuthed, ctrl.CMS.Article.store);
	//end

	// //listing routes
	appRoutes.cms.get('/listings', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.index);
	appRoutes.cms.get('/listings/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.edit);
	appRoutes.cms.post('/listings/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.update);

	appRoutes.cms.get('/listings/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.show);
	appRoutes.cms.get('/listings/:id/suspend', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.suspend);
	appRoutes.cms.get('/listings/:id/unsuspend', mdlwr.Auth.isAuthed, ctrl.CMS.Listing.unsuspend);

	// //property type routes
	appRoutes.cms.get('/property-type', mdlwr.Auth.isAuthed, ctrl.CMS.PropertyType.index);
	
	appRoutes.cms.get('/property-type/create', mdlwr.Auth.isAuthed, ctrl.CMS.PropertyType.create);
	// appRoutes.cms.post('/property-type', mdlwr.Auth.isAuthed, ctrl.CMS.PropertyType.store);
	
	// appRoutes.cms.get('/property-type/:id', mdlwr.Auth.isAuthed, ctrl.CMS.PropertyType.show);
	
	// appRoutes.cms.get('/property-type/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.PropertyType.edit);
	// appRoutes.cms.put('/property-type/:id', mdlwr.Auth.isAuthed, ctrl.CMS.PropertyType.update);

	// //listing type routes
	appRoutes.cms.get('/listing-type', mdlwr.Auth.isAuthed, ctrl.CMS.ListingType.index);
	appRoutes.cms.get('/listing-type/create', mdlwr.Auth.isAuthed, ctrl.CMS.ListingType.create);

	// appRoutes.cms.post('/listing-type', mdlwr.Auth.isAuthed, ctrl.CMS.ListingType.store);
	
	// appRoutes.cms.get('/listing-type/:id', mdlwr.Auth.isAuthed, ctrl.CMS.ListingType.show);
	
	// appRoutes.cms.get('/listing-type/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.ListingType.edit);
	// appRoutes.cms.put('/listing-type/:id', mdlwr.Auth.isAuthed, ctrl.CMS.ListingType.update);

	// //amenities routes
	appRoutes.cms.get('/amenities', mdlwr.Auth.isAuthed, ctrl.CMS.Amenities.index);
	appRoutes.cms.get('/amenities/create', mdlwr.Auth.isAuthed, ctrl.CMS.Amenities.create);

	appRoutes.cms.post('/amenities', mdlwr.Auth.isAuthed, ctrl.CMS.Amenities.store);
	
	appRoutes.cms.get('/amenities/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Amenities.show);
	// appRoutes.cms.get('/amenities/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Amenities.edit);
	// appRoutes.cms.put('/amenities/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Amenities.update);

	// //help routes
	appRoutes.cms.get('/help', mdlwr.Auth.isAuthed, ctrl.CMS.Help.index);
	appRoutes.cms.get('/help/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Help.show);
	appRoutes.cms.post('/help/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Help.update);

	// //help routes
	appRoutes.cms.get('/trips', ctrl.CMS.Trip.index);
	appRoutes.cms.get('/trips/:code', ctrl.CMS.Trip.show);
	// appRoutes.cms.get('/trips/:code/edit', mdlwr.Auth.isAuthed, ctrl.CMS.Trip.edit);
	// appRoutes.cms.put('/trips/:code', mdlwr.Auth.isAuthed, ctrl.CMS.Trip.update);

	appRoutes.cms.get('/payments', mdlwr.Auth.isAuthed, ctrl.CMS.Payment.index);
	appRoutes.cms.get('/payments/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Payment.show);
	appRoutes.cms.post('/payments/:id', mdlwr.Auth.isAuthed, ctrl.CMS.Payment.update);
	appRoutes.cms.get('/payments/:id/accept', mdlwr.Auth.isAuthed, ctrl.CMS.Payment.accept);
	appRoutes.cms.get('/payments/:id/decline', mdlwr.Auth.isAuthed, ctrl.CMS.Payment.decline);
	
	appRoutes.cms.get('/report/money', ctrl.CMS.Report.money);
	appRoutes.cms.get('/report/money/today', ctrl.CMS.Report.moneyToday);
	appRoutes.cms.get('/report/traction', ctrl.CMS.Report.traction);
	appRoutes.cms.get('/report/traction/today', ctrl.CMS.Report.tractionToday);
	appRoutes.cms.get('/report/guest', ctrl.CMS.Report.guest);	
	appRoutes.cms.get('/report/guest/today', ctrl.CMS.Report.guestToday);	

	appRoutes.cms.get('/bank-accounts', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.index);	
	appRoutes.cms.get('/bank-accounts/create', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.create);	
	appRoutes.cms.post('/bank-accounts', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.store);	
	appRoutes.cms.get('/bank-accounts/:id', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.show);	
	appRoutes.cms.get('/bank-accounts/:id/edit', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.edit);	
	appRoutes.cms.post('/bank-accounts/:id', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.update);	
	appRoutes.cms.get('/bank-accounts/:id/delete', mdlwr.Auth.isAuthed, ctrl.CMS.BankAcc.destroy);	
	//end of route for cms

	//routes for api
	//account routes
	appRoutes.apiv1.get('/config/price-range', ctrl.API.Config.priceRange);

	appRoutes.apiv1.get('/nyobain/:code/:acceptance', ctrl.API.Auth.nyobain);
	appRoutes.apiv1.post('/nyoba', ctrl.API.Auth.nyoba);


	appRoutes.apiv1.post('/login', ctrl.API.Auth.login);
	appRoutes.apiv1.post('/register', ctrl.API.Register.store);
	// appRoutes.apiv1.get('/coba', ctrl.API.Register.testMail);
	appRoutes.apiv1.post('/activate/phone', ctrl.API.Register.phoneActivation);
	appRoutes.apiv1.post('/activate/email', ctrl.API.Register.emailActivation);
	appRoutes.apiv1.post('/activate/phone/resend', ctrl.API.Register.phoneResend);
	appRoutes.apiv1.post('/activate/email/resend', ctrl.API.Register.emailResend);
	
	appRoutes.apiv1.post('/login/fb', ctrl.API.Auth.loginFB);
	appRoutes.apiv1.post('/register/fb', ctrl.API.Register.storeFB);

	appRoutes.apiv1.post('/login/google', ctrl.API.Auth.loginGoogle);
	appRoutes.apiv1.post('/register/google', ctrl.API.Register.storeGoogle);

	appRoutes.apiv1.put('/push-token', mdlwr.Auth.JWTAuth, ctrl.API.Auth.tokenUpdate);

	appRoutes.apiv1.delete('/logout', mdlwr.Auth.JWTAuth, ctrl.API.Auth.logout);

	//input checker
	appRoutes.apiv1.post('/check/email', ctrl.API.Check.email);
	appRoutes.apiv1.post('/check/phone', ctrl.API.Check.phone);

	//USER DATA ROUTES
	appRoutes.apiv1.get('/profile', mdlwr.Auth.JWTAuth, ctrl.API.User.profile);
	appRoutes.apiv1.put('/profile', mdlwr.Auth.JWTAuth, ctrl.API.User.profileUpdate);
	appRoutes.apiv1.get('/profile/bank', mdlwr.Auth.JWTAuth, ctrl.API.User.bank);
	appRoutes.apiv1.post('/profile/bank', mdlwr.Auth.JWTAuth, ctrl.API.User.bankUpdate);
	appRoutes.apiv1.post('/verify', mdlwr.Auth.JWTAuth, ctrl.API.Verification.store);
	appRoutes.apiv1.put('/profile/picture', mdlwr.Auth.JWTAuth, ctrl.API.User.pictureUpdate);


	appRoutes.apiv1.get('/user/:id', mdlwr.Auth.JWTAuth, ctrl.API.User.show);
	appRoutes.apiv1.get('/user/:id/verification', mdlwr.Auth.JWTAuth, ctrl.API.Verification.show);
	appRoutes.apiv1.get('/user/:id/review', mdlwr.Auth.JWTAuth, ctrl.API.Review.user);
	appRoutes.apiv1.get('/user/:id/listing', mdlwr.Auth.JWTAuth, ctrl.API.User.showListing);

	// appRoutes.apiv1.get('/check', ctrl.API.Check.phone);
	
	appRoutes.apiv1.post('/password/change', mdlwr.Auth.JWTAuth, ctrl.API.Password.change);
	appRoutes.apiv1.post('/password/forgot', ctrl.API.Password.forgot);
	// appRoutes.apiv1.post('/password/reset', ctrl.API.Password.reset);


	//END OF USER DATA ROUTES

	//MESSAGING ROUTES
	appRoutes.apiv1.get('/message', mdlwr.Auth.JWTAuth, ctrl.API.Message.index);
	appRoutes.apiv1.get('/message/:listingid/:userid', mdlwr.Auth.JWTAuth, ctrl.API.Message.show);
	appRoutes.apiv1.get('/last-trip/:listingid/:userid', mdlwr.Auth.JWTAuth, ctrl.API.Message.lastTrip);
	appRoutes.apiv1.post('/message/:listingid/:userid', mdlwr.Auth.JWTAuth, ctrl.API.Message.store);
	appRoutes.apiv1.delete('/message/:id', mdlwr.Auth.JWTAuth, ctrl.API.Message.destroy);
	appRoutes.apiv1.delete('/message/:listingid/:userid', mdlwr.Auth.JWTAuth, ctrl.API.Message.destroyConvo);

	//END OF MESSAGING ROUTES

	//TRAVELLER ROUTES
	//listing routes
	appRoutes.apiv1.get('/explore', mdlwr.Auth.JWTAuth, ctrl.API.ListingTraveller.explore);

	// appRoutes.apiv1.get('/nearby', mdlwr.Auth.JWTAuth, ctrl.API.ListingTraveller.nearby);
	// appRoutes.apiv1.get('/benchmark', ctrl.API.ListingTraveller.benchmark);
	// appRoutes.apiv1.get('/benchmark2', ctrl.API.ListingTraveller.benchmark2);

	appRoutes.apiv1.get('/search/region', ctrl.API.Search.region);
	appRoutes.apiv1.get('/search/listing', mdlwr.Auth.JWTAuth, ctrl.API.Search.listing);

	appRoutes.apiv1.get('/listing/:id', mdlwr.Auth.JWTAuth, ctrl.API.ListingTraveller.show);
	appRoutes.apiv1.get('/listing/:id/review', mdlwr.Auth.JWTAuth, ctrl.API.Review.listing);
	appRoutes.apiv1.get('/listing/:id/availability', mdlwr.Auth.JWTAuth, ctrl.API.ListingTraveller.availability);
	appRoutes.apiv1.get('/listing/:id/special-prices', mdlwr.Auth.JWTAuth, ctrl.API.ListingTraveller.specialPrices);

	appRoutes.apiv1.get('/trip', mdlwr.Auth.JWTAuth, ctrl.API.TripTraveller.index);
	appRoutes.apiv1.post('/trip', mdlwr.Auth.JWTAuth, ctrl.API.TripTraveller.store);
	appRoutes.apiv1.get('/trip-notif', mdlwr.Auth.JWTAuth, ctrl.API.TripTraveller.notif);
	appRoutes.apiv1.get('/trip/:code', mdlwr.Auth.JWTAuth, ctrl.API.TripTraveller.show);
	appRoutes.apiv1.delete('/trip/:code', mdlwr.Auth.JWTAuth, ctrl.API.TripTraveller.destroy);
	appRoutes.apiv1.post('/trip/:code/review', mdlwr.Auth.JWTAuth, ctrl.API.TripTraveller.storeReview);


	appRoutes.apiv1.get('/amenities', mdlwr.Auth.JWTAuth, ctrl.API.Amenities.index);

	// appRoutes.apiv1.post('/lupa-password', controllers.password.lupaPassword);
	//END OF TRAVELLER ROUTES

	//HOST ROUTES
	appRoutes.apiv1.get('/own-listing', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.index);
	appRoutes.apiv1.get('/own-listing/:id', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.show);
	appRoutes.apiv1.put('/own-listing/:id', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.update);
	appRoutes.apiv1.post('/own-listing', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.store);
	appRoutes.apiv1.post('/own-listing/:id/block-date', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.blockDate);
	appRoutes.apiv1.delete('/own-listing/:id/block-date', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.blockDateDelete);
	appRoutes.apiv1.post('/own-listing/:id/special-price', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.specialPrice);
	appRoutes.apiv1.delete('/own-listing/:id/special-price', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.specialPriceDelete);
	appRoutes.apiv1.post('/own-listing/:id/update-image', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.updateImage);

	appRoutes.apiv1.post('/own-listing/:id/unlist', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.unlist);
	appRoutes.apiv1.post('/own-listing/:id/relist', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.relist);

	appRoutes.apiv1.delete('/own-listing/:id', mdlwr.Auth.JWTAuth, ctrl.API.ListingHost.delete);

	appRoutes.apiv1.get('/book-request', mdlwr.Auth.JWTAuth, ctrl.API.TripHost.index);
	appRoutes.apiv1.get('/book-request/:code', mdlwr.Auth.JWTAuth, ctrl.API.TripHost.show);
	appRoutes.apiv1.post('/book-request', mdlwr.Auth.JWTAuth, ctrl.API.TripHost.store);
	appRoutes.apiv1.post('/book-request/:code/:acceptance', mdlwr.Auth.JWTAuth, ctrl.API.TripHost.acceptance);

	appRoutes.apiv1.get('/report/money/:id', mdlwr.Auth.JWTAuth, ctrl.API.Report.money);
	appRoutes.apiv1.get('/report/total-guest/:id', mdlwr.Auth.JWTAuth, ctrl.API.Report.guest);
	appRoutes.apiv1.get('/report/success-books/:id', mdlwr.Auth.JWTAuth, ctrl.API.Report.books);

	appRoutes.apiv1.get('/report-cms/money/:id/:type', ctrl.API.Report.moneyCMS);
	appRoutes.apiv1.get('/report-cms/total-guest/:id/:type', ctrl.API.Report.guestCMS);
	appRoutes.apiv1.get('/report-cms/success-books/:id/:type', ctrl.API.Report.booksCMS);

	appRoutes.apiv1.get('/bank/', mdlwr.Auth.JWTAuth, ctrl.API.Bank.index);

	//END OF HOST ROUTES

	appRoutes.apiv1.get('/setting/notif', mdlwr.Auth.JWTAuth, ctrl.API.Settings.notifShow);	
	appRoutes.apiv1.put('/setting/notif', mdlwr.Auth.JWTAuth, ctrl.API.Settings.notifUpdate);	
	appRoutes.apiv1.post('/help', mdlwr.Auth.JWTAuth, ctrl.API.Help.store);
	
	appRoutes.apiv1.post('/image/upload/listing', mdlwr.Auth.JWTAuth, uploader.listing.fields([
		{ name: 'number', maxCount: 1 }, 
		{ name: 'identifier', maxCount: 1 }, 
		{ name: 'image', maxCount: 1 }
		]), ctrl.API.Image.listing);

	appRoutes.apiv1.post('/image/upload/listing/:identifier/:number', mdlwr.Auth.JWTAuth, ctrl.API.Image.listingUpdate, uploader.listing.single('image'), function (req, res) {
			return res.status(200).json({message: 'success', url: req.imageLoc});
		});	

	appRoutes.apiv1.post('/image/upload/profile', mdlwr.Auth.JWTAuth, uploader.profile.single('image'), ctrl.API.Image.profile);

	appRoutes.apiv1.post('/image/upload/verification/:type', mdlwr.Auth.JWTAuth, uploader.verification.fields([
		{ name: 'image', maxCount: 1}
		]), ctrl.API.Image.verification);

	appRoutes.apiv1.post('/image/upload/payment/:bookcode', mdlwr.Auth.JWTAuth, ctrl.API.Image.payment, uploader.payment.single('image'), function (req, res) {
		return res.status(201).json({message: 'payment submitted'});
	});

	appRoutes.apiv1.get('/image/delete/listing/:identifier/:number', mdlwr.Auth.JWTAuth, ctrl.API.Image.listingDelete);
	//end of routes for api

	//routes for web
	appRoutes.open.get('/cobain', function (req, res) {
		var helperString = require('./helper/string');
		return res.json({
			kode: helperString.random(5,1).toUpperCase(),
			kodelagi: helperString.random(5,1).toUpperCase(),
		});
	});

	appRoutes.open.get('/cobicobi', function (req, res) {

		var ejs = require('ejs');

		var viewLocation = __dirname+'/../resource/view/';
		var fs = require('fs');

		var template = fs.readFileSync(viewLocation+'testing.ejs', 'utf8'); 	

		return res.send(ejs.render(template));
	});
	// appRoutes.open.get('/',  ctrl.WEB.Home.index);
    appRoutes.open.get('/login', ctrl.WEB.Auth.show);
    appRoutes.open.get('/logout', mdlwr.Auth.isUserAuthed, ctrl.WEB.Auth.destroy);

    appRoutes.open.get('/', mdlwr.Auth.isUserAuthed, ctrl.WEB.Home.index);
    appRoutes.open.post('/login', passport.authenticate('local-user-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
	appRoutes.open.get('/terms-and-condition', ctrl.WEB.Home.tnc);

	appRoutes.open.get('/articles', ctrl.WEB.Articles.index);
	appRoutes.open.get('/articles/search', ctrl.WEB.Articles.search);
	appRoutes.open.get('/articles/:slug', ctrl.WEB.Articles.show);

	appRoutes.open.get('/search/city', ctrl.WEB.Search.city);

	appRoutes.open.get('/password/reset/:resetcode', ctrl.WEB.Password.show);
	appRoutes.open.post('/password/reset/:resetcode', ctrl.WEB.Password.store);
	appRoutes.open.get('/password/reset-success', ctrl.WEB.Password.success);

	appRoutes.open.get('/payment/xenditest1', ctrl.WEBV2.Payment.dev1);
	appRoutes.open.get('/payment/xendit', ctrl.WEBV2.Payment.show);
	appRoutes.open.post('/payment/xendit/callback', ctrl.WEBV2.Payment.callback);
	// appRoutes.open.get('/payment/xendit/callback', ctrl.WEBV2.Payment.callback);
	

	appRoutes.open.get('/payment/:bookcode', ctrl.WEB.Payment.show);
	appRoutes.open.post('/payment/doku/verify-payment', ctrl.WEB.Payment.dokuVerification);
	appRoutes.open.post('/payment/doku/notify-payment', ctrl.WEB.Payment.dokuNotify);
	appRoutes.open.post('/payment/doku/redirect', ctrl.WEB.Payment.dokuRedirect);
	appRoutes.open.post('/payment/doku/cancel-payment', ctrl.WEB.Payment.dokuVerification);
	appRoutes.open.get('/payment/doku/close', ctrl.WEB.Payment.dokuClose);
	


	appRoutes.open.get('/whoops/403', function (req, res) {
		return res.render('WEB/error/403', {layout: 'WEB/layout/singleform'});
	});

	appRoutes.open.get('/whoops/404', function (req, res) {
		return res.render('WEB/error/404', {layout: 'WEB/layout/singleform'});
	});

	appRoutes.open.get('/whoops/500', function (req, res) {
		return res.render('WEB/error/500', {layout: 'WEB/layout/singleform'});
	});
	

	//end of routes for web
};
