'use strict';
var models  = require(__dirname+'/../../model');

exports.show = function(req, res) 
{

	return res.render('WEB/login', {
		layout: 'WEB/layout/singleform',
		message: req.flash('loginMessage')

	});
};

exports.destroy = function (req, res) 
{
	req.logout();
	req.user = null
	res.redirect('/login');
}