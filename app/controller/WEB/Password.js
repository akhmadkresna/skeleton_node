'use strict';
var models  = require(__dirname+'/../../model');

exports.show = function(req, res) 
{
	return res.render('WEB/password/reset', {
		layout: 'WEB/layout/singleform',
		resetcode: req.params.resetcode
	});
};

exports.store = function(req, res) 
{
	req.assert('resetcode', 'Invalid id').isUUID();
	req.assert('password', 'Invalid password').isLength({min: 6});
	req.assert('cpassword', 'Invalid cpassword').equals(req.body.password);

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('back');

	models.User.findOne({
		where: {
			resetCode: req.params.resetcode
		}
	})
	.then(function (user) {
		if (user == null) 
			return res.redirect('/whoops/404');
		
		return user.update({
			password: req.body.password
		});
	})
	.then(function (updated) {
		return res.redirect('/password/reset-success');
	});
	
}

exports.success = function(req, res) 
{
	return res.render('WEB/password/success', {
		layout: 'WEB/layout/singleform'
	});

}
