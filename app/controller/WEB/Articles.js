'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	models.Article.findAll({
		order: "createdAt DESC"
	})
	.then(function (articles) {

		if (req.query.inapp != null) 
		{
			return res.render('WEB/article/index', {
				layout: 'WEB/layout/master',
				articles: articles,
				inapp: true
			});
		}
		else
		{
			return res.render('WEB/article/index', {
				layout: 'WEB/layout/master',
				articles: articles
			});
		}	
		
	})
	.catch(function (err) {
		return res.redirect('/whoops/500')
	});
}

exports.search = function(req, res) 
{
	req.assert('slug', 'Invalid slug').isSlug();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404')

	models.Article.findOne({
		where: {
			slug: req.params.slug
		}
	}).then(function(article) {
		if (article == null) 
			throw {
				type: 404
			}

		return res.render('WEB/article/show', {
			layout: 'WEB/layout/master',
			article: article
		});
	})
	.catch(function (err) {
		return res.redirect('/whoops/404')
	});
}

exports.show = function(req, res) 
{
	req.assert('slug', 'Invalid slug').isSlug();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404')

	models.Article.findOne({
		where: {
			slug: req.params.slug
		}
	}).then(function(article) {
		if (article == null) 
			throw {
				type: 404
			}

		if (req.query.inapp != null) 
		{
			return res.render('WEB/article/show', {
				layout: 'WEB/layout/master',
				article: article,
				inapp: true
			});
		}
		else
		{
			return res.render('WEB/article/show', {
				layout: 'WEB/layout/master',
				article: article
			});
		}	

	})
	.catch(function (err) {
		return res.redirect('/whoops/404')
	});
};