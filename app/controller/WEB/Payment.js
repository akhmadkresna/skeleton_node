'use strict';
var models  = require(__dirname+'/../../model');

var string  = require(__dirname+'/../../helper/string');
var sender  = require(__dirname+'/../../helper/sender');

var dokuConfig = global.APP_CONFIGS.doku;

exports.show = function(req, res) 
{
	req.assert('bookcode', 'Invalid bookcode').isAlphanumeric();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			code: req.params.bookcode,
			paid: 0
		},
		include: [{
			model: models.User,
			as: 'Traveller'
		}]
	}).then(function (trip) {

		if (trip == null) 
			return res.redirect('/whoops/404');

		var cost = trip.cost;
		var handling = Math.ceil(trip.cost * 0.04);
		var total = cost+handling;
		
		var words = string.dokuWords(total, trip.code);

		return res.render('WEB/payment/show', {
			layout: 'WEB/layout/master',
			trip: trip,
			doku: dokuConfig,
			words: words,
			cost: cost,
			handling: handling,
			total: total
		});
	})
};

exports.dokuVerification = function(req, res) 
{
	req.assert('TRANSIDMERCHANT', 'Invalid bookcode').notEmpty();
	req.assert('AMOUNT', 'Invalid bookcode').notEmpty();
	req.assert('STOREID', 'Invalid bookcode').notEmpty();
	req.assert('WORDS', 'Invalid bookcode').notEmpty();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			code: req.body.TRANSIDMERCHANT,
			paid: 0
		}
	})
	.then(function (trip) {
		if (trip == null) 
			return res.send("Stop");
		
		var cost = trip.cost;
		var handling = Math.ceil(trip.cost * 0.04);
		var total = cost+handling;

		if (total != req.body.AMOUNT)
			return res.send("Stop");

		var words = string.dokuWords(total, trip.code);

		if (req.body.WORDS != words) 
			return res.send("Stop");

		return res.send("Continue");
	})
}

exports.dokuNotify = function(req, res) 
{
	req.assert('TRANSIDMERCHANT', 'Invalid bookcode').notEmpty();
	req.assert('AMOUNT', 'Invalid bookcode').notEmpty();
	req.assert('RESULT', 'Invalid bookcode').notEmpty();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.sequelize.transaction(function (t) {
		return models.Reservation.findOne({
			where: {
				code: req.body.TRANSIDMERCHANT
			},
			transaction: t
		})
		.then(function (trip) {
			if (trip == null) 
				return res.send("Stop");

			if (req.body.RESULT != 'Success') 
				return res.send("Stop");

			return trip.update({
				paid: true
			}, { transaction: t});
		})
		.then(function (updated) {
			return models.Payment.create({
				reservationCode: req.body.TRANSIDMERCHANT,
				location: null,
				doku: 1,
				status: 2
			}, { transaction: t})
		});
	})
	.then(function (trip) {
		if (trip == null) 
			return res.send("Stop");
		
		if (req.body.RESULT != 'Success') 
			return res.send("Stop");
		
		return trip.update({
			paid: true
		});
	})
	.then(function (updated) {
		return res.send("Continue");
	})
}

exports.dokuRedirect = function(req, res) 
{
	req.assert('TRANSIDMERCHANT', 'Invalid bookcode').notEmpty();
	// req.assert('AMOUNT', 'Invalid bookcode').notEmpty();
	// req.assert('RESULT', 'Invalid bookcode').notEmpty();
	// req.assert('STATUSCODE', 'Invalid bookcode').notEmpty();
	// req.assert('TRANSDATE', 'Invalid bookcode').notEmpty();
	// req.assert('PTYPE', 'Invalid bookcode').notEmpty();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			code: req.body.TRANSIDMERCHANT,
			paid: true
		},
		include: [{
			model: models.Payment
		}, {
			model: models.User,
			as: 'Traveller'
		}]
	})

	.then(function (trip) {
		if (trip == null) 
			return res.redirect("/whoops/404");
		
		if (req.body.RESULT != 'SUCCESS') 
			return res.redirect("/whoops/404");

		return models.Payment.findOne({
			where: {
				reservationCode: {
					$like: req.body.TRANSIDMERCHANT
				}
			},
			include: [{
				model: models.Reservation,
				include: [{
					model: models.User,
					as: 'Traveller'
				}, {
					model: models.Listing
				}]
			}]
		})
	})
	.then(function (payment) {
		var mails = [{
			to: payment.Reservation.Traveller.email,
			subject: 'Payment for '+payment.reservationCode+' approved',
			template: 'payment/approvedDoku',
			data: {
				payment: payment
			}
		}]

		var sendmail =  sender.email(mails);
		
		return res.render('WEB/payment/success', {
			layout: 'WEB/layout/master',
			trip: payment.Reservation
		});

		return res.send('payment success');
	})
}

exports.dokuClose = function(req, res) 
{
	if (typeof req.query.finish == undefined) 
		return res.redirect('/payment/doku/close?finish');

	return res.json({close: true});
}