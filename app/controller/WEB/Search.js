'use strict';
var models  = require(__dirname+'/../../model');

exports.city = function(req, res) 
{
	req.assert('keyword', 'Invalid id').isLength({min: 2});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findAll({
		where:{
			city: {
				$like: '%'+req.body.keyword+'%'
			}
		}
	}).then(function (cities) {
		return res.status(200).json({'cities' : cities});
	});

};
