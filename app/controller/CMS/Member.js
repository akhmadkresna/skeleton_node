'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');

var logger  = require(__dirname+'/../../helper/logger');

exports.index = function(req, res) 
{
	var keyword = '';
	var status = (req.query.status=='' || req.query.status==null)?'all':req.query.status;

	if (status != 'suspended' && status!= 'active') 
		status = 'all';

	var queries = {
		order: [['createdAt', 'DESC']]
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			$or:{
				lastName: {
					$like: '%'+req.query.keyword+'%'
				},
				firstName: {
					$like: '%'+req.query.keyword+'%'
				},
				email: {
					$like: '%'+req.query.keyword+'%'
				}
			}
		};
	}

	if (status != 'all') 
		queries.where['suspended'] = (status=='suspended')?true:false;

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*15;
	queries.limit = 15;

	models.User.findAndCountAll(queries).then(function (members) {

		return res.render('CMS/member/index', {
			layout: 'CMS/layout/master', 
			members: members.rows, 
			keyword:keyword, 
			status:status, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(members.count)/15),
		});
	});
};


exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var data = new Object();

	models.User.findOne({
		where: {
			id: req.params.id
		},
		include: [{
			model: models.Listing
		}, {
			model: models.Verification
		}, {
			model: models.MemberLog
		}],
		order: [
		[ models.MemberLog, 'createdAt', 'DESC' ]
		]
	})
	.then(function (member) {
		if (member === null) 
			return res.redirect('/whoops/404');

		member.verified = 0;

		for (var i = member.Verifications.length - 1; i >= 0; i--) 
		{
			if (member.Verifications[i].status == 2) 
				member.verified += 1;
		}
		data.member = member;

		return models.Reservation.findAll({
			where: {
				userId: req.params.id
			},
			include: [{
				model: models.Listing
			}]
		})
	})
	.then(function (reservation) {
		return res.render('CMS/member/show', {
			layout: 'CMS/layout/master', 
			member: data.member,
			trips: reservation
		});
	});
};

exports.suspend = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});
	req.assert('reason', 'Invalid reason').isLength({min:5});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var suspmember;

	models.User.findOne({
		where: {
			id: req.params.id,
			suspended: false
		}
	}).then(function (member) {
		if (member === null) 
			return res.redirect('/whoops/404');

		console.log(member)

		suspmember = member;
		
		return models.User.update({
			suspended: true
		}, {
			where: {
				id: req.params.id
			}
		});
	})
	.then(function (updated) {
		if (suspmember == null) 
			return res.redirect('/whoops/404');

		var mails = [{
			to: suspmember.email,
			subject: 'User suspension - RoomMe',
			content: req.body.reason
		}]

		var sendmail =  sender.email(mails);
		
		req.flash('success', 'Member has been suspended');

		var log = logger.memberlog(req.params.id, 'user suspension')

		return res.redirect('back');
	})
	// .catch(function (err) {
	// 	return res.redirect('/whoops/404')
	// });
};

exports.unsuspend = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findOne({
		where: {
			id: req.params.id,
			suspended: true
		}
	}).then(function (member) {
		if (member === null) 
			return res.redirect('/whoops/404');

		return models.User.update({
			suspended: false
		}, {
			where: {
				id: req.params.id
			}
		});
	})
	.then(function (updated) {
		req.flash('success', 'Member has been activated');

		var log = logger.memberlog(req.params.id, 'user reactivated')

		return res.redirect('back');
	})
	.catch(function (err) {
		return res.redirect('/whoops/404')
	});
};

exports.edit = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findOne({
		where: {
			id: req.params.id
		}
	}).then(function (member) {
		if (member === null) 
			return res.redirect('/whoops/404');

		member.verified = 0;

		return res.render('CMS/member/edit', {
			layout: 'CMS/layout/master', 
			member: member
		});
	})
}

exports.update = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});
	req.assert('name', 'Invalid id').isInt({min:0});
	req.assert('email', 'Invalid id').isInt({min:0});
	req.assert('suspended', 'Invalid id').isInt({min:0});
	req.assert('bithdate', 'Invalid id').isInt({min:0});
	req.assert('', 'Invalid id').isInt({min:0});
	req.assert('', 'Invalid id').isInt({min:0});
	req.assert('', 'Invalid id').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findOne({
		where: {
			id: req.params.id
		},
		include: [{
			model: models.Listing
		}, {
			model: models.Reservation,
			include: [{
				model: models.Listing
			}]
		}, {
			model: models.Verification
		}]
	}).then(function (member) {
		// return res.json({member:member.Verifications})
		if (member === null) 
			return res.redirect('/whoops/404');

		member.verified = 0;

		for (var i = member.Verifications.length - 1; i >= 0; i--) 
		{
			if (member.Verifications[i].status == 2) 
				member.verified += 1;
		}

		return res.render('CMS/member/show', {
			layout: 'CMS/layout/master', 
			member: member
		});
	})
}

exports.create = function(req, res) 
{
	return res.render('CMS/member/create', {
		layout: 'CMS/layout/master'
	});
}

exports.store = function(req, res) 
{

}