'use strict';
var models  = require(__dirname+'/../../model');

exports.pagination = function(req, res) 
{
	models[modelName].count(queryObj).then(function (total) {
		var thepaginator = '';

		if (parameter.length > 0) 
		{
			for (var i = parameter.length - 1; i >= 0; i--) 
			{
				url	+= '?'+parameter[i].key+'='+parameter[i].value;
			}
		}

		currentPage = parseInt(currentPage);

		var pagerOpen = '<nav><ul class="pagination">';

		if (currentPage === 1) 
		{
			var prevNav = '<li class="disabled">\
			<a href="/'+url+'" aria-label="Previous">\
			<span aria-hidden="true">&laquo;</span>\
			</a>\
			</li>';
		} 
		else 
		{
			var prevNav = '<li>\
			<a href="/'+url+'&page='+(currentPage-1)+'" aria-label="Previous">\
			<span aria-hidden="true">&laquo;</span>\
			</a>\
			</li>';
		}

		if (currentPage === total) 
		{
			var nextNav = '<li class="disabled">\
			<a href="/'+url+'" aria-label="Next">\
			<span aria-hidden="true">&raquo;</span>\
			</a>\
			</li>';
		} 
		else 
		{
			var nextNav = '<li>\
			<a href="/'+url+'&page='+currentPage+1+'" aria-label="Next">\
			<span aria-hidden="true">&raquo;</span>\
			</a>\
			</li>';
		}

		var pagerClose = '</ul></nav>';
		var pages = '';

		if (total > 10) 
		{
			console.log(currentPage)
			if (currentPage <= 7) 
			{
				for (var i = 1; i <= 8; i++) {
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'&page='+i+'">'+i+'</a></li>';
				}

				pages += '<li class="disabled"><span>...</span></li>';
				pages += '<li><a href="/'+url+'&page='+(total-1)+'">'+(total-1)+'</a></li>';
				pages += '<li><a href="/'+url+'&page='+total+'">'+total+'</a></li>';
			}
			else if (currentPage >7 && (currentPage < total-7)) 
			{
				pages += '<li><a href="/'+url+'&page=1">1</a></li>';
				pages += '<li><a href="/'+url+'&page=2">2</a></li>';
				pages += '<li class="disabled"><span>...</span></li>';

				for (var i = currentPage-2; i <= (currentPage+2); i++) 
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'&page='+i+'">'+i+'</a></li>';

				pages += '<li class="disabled"><span>...</span></li>';
				pages += '<li><a href="/'+url+'&page='+(total-1)+'">'+(total-1)+'</a></li>';
				pages += '<li><a href="/'+url+'&page='+total+'">'+total+'</a></li>';
			} 
			else if (currentPage >7) 
			{
				for (var i = 1; i <= 8; i++) 
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'&page='+i+'">'+i+'</a></li>';

				pages += '<li class="disabled"><span>...</span></li>';
				pages += '<li><a href="/'+url+'&page='+(total-1)+'">'+(total-1)+'</a></li>';
				pages += '<li><a href="/'+url+'&page='+total+'">'+total+'</a></li>';

			} 
			else 
			{
				pages += '<li><a href="/'+url+'&page=1">1</a></li>';
				pages += '<li><a href="/'+url+'&page=2">2</a></li>';
				pages += '<li class="disabled"><span>...</span></li>';

				for (var i = total-7; i <= total; i++) 
					pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'&page='+i+'">'+i+'</a></li>';

			}
		}
		else 
		{
			for (var i = 1; i <= total; i++) 
				pages += '<li '+(i === currentPage?'class="active"':'')+'><a href="/'+url+'&page='+i+'">'+i+'</a></li>'

		}
		thepaginator = pagerOpen+prevNav+pages+nextNav+pagerClose;
		return res.render('services/pagination', {layout: 'layout/pagination', paging: thepaginator});
	}).catch(function (err) {
		return res.status(500).json({err: true});
	});
};