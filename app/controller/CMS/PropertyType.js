'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	var keyword = '';
	var queries = {
		order: 'createdAt ASC'
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			$or:{
				name: {
					$like: '%'+req.query.keyword+'%'
				}
			}
		}
	}

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.LocationType.findAndCountAll(queries).then(function (type) {
		return res.render('CMS/propertytype/index', {
			layout: 'CMS/layout/master', 
			type: type.rows, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(type.count)/20),
		});
	});
};

exports.create = function(req, res) 
{
	return res.render('CMS/propertytype/create', {
		layout: 'CMS/layout/master'
	});
};