'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');
var push  = require(__dirname+'/../../helper/push');

exports.approve = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});
	req.assert('idverif', 'Invalid listing').isUUID();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404');

	models.Verification.findOne({
		where: {
			id: req.params.idverif,
			userId: req.params.id,
			status: 1
		},
		include: [{
			model: models.User,
			attributes: [
			"email",
			"firstName",
			"lastName",
			"device",
			"pushToken"
			]
		}]
	}).then(function (verification) {
		if (verification == null) 
			return res.redirect('/whoops/404');

		var mails = [{
			to: verification.User.email,
			subject: 'ID verification approved - Roomme',
			template: 'account/acceptedVerification',
			data: {
				user: verification.User
			}
		}]

		var sendmail =  sender.email(mails);

		if (verification.User.device == 'ios') 
			var test =  push.pushIOS(verification.User.pushToken, 'ID verification approved by roomme', {});
		else if (verification.User.device == 'android') 
			var test =  push.pushGCM(verification.User.pushToken, 'ID verification approved by roomme', {});

		return verification.update({
			status: 2
		});
	}).then(function (updated) {
		req.flash('ID verification approved');
		return res.redirect('back');
	})


};

exports.decline = function (req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});
	req.assert('idverif', 'Invalid listing').isUUID();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404');

	models.Verification.findOne({
		where: {
			id: req.params.idverif,
			userId: req.params.id,
			status: 1
		},
		include: [{
			model: models.User,
			attributes: [
			"email",
			"firstName",
			"lastName",
			"device",
			"pushToken"
			]
		}]
	}).then(function (verification) {
		if (verification == null) 
			return res.redirect('/whoops/404');

		var mails = [{
			to: verification.User.email,
			subject: 'ID verification declined - Roomme',
			template: 'account/declinedVerification',
			data: {
				user: verification.User
			}
		}]

		var sendmail =  sender.email(mails);

		if (verification.User.device == 'ios') 
			var test =  push.pushIOS(verification.User.pushToken, 'ID verification declined by roomme', {});
		else if (verification.User.device == 'android') 
			var test =  push.pushGCM(verification.User.pushToken, 'ID verification declined by roomme', {});

		return verification.update({
			status: 0
		});
	}).then(function (updated) {
		req.flash('ID verification declined');
		return res.redirect('back');
	})
}