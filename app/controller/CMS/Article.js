'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	var keyword = '';
	var queries = {
		order: 'createdAt ASC'
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			title: {
				$like: '%'+req.query.keyword+'%'
			}
		}
	}
	
	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Article.findAndCountAll(queries).then(function (articles) {

		return res.render('CMS/article/index', {
			layout: 'CMS/layout/master', 
			articles: articles.rows, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(articles.count)/20),
		});
	});
}

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404')

	models.Article.findById(req.params.id).then(function(article) {
		if (article == null) 
			throw {
				type: 404
			}
		return res.redirect('/article/'+article.slug);
	})
	.catch(function (err) {
		return res.redirect('/whoops/404')
	});
};

exports.create = function(req, res) 
{
	return res.render('CMS/article/create', {
		layout: 'CMS/layout/master'
	});
}

exports.store = function(req, res) 
{
	req.assert('slug', 'Invalid id').isSlug();
	req.assert('title', 'Invalid id').isLength({min: 3});
	req.assert('content', 'Invalid id').isLength({min: 20});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('back');

	models.Article.count({
		where: {
			slug : {
				$like: req.body.slug
			},
			id : {
				$ne: req.params.id
			}
		}
	})
	.then(function (article) {
		if (article>0) 
			throw {
				type: 403
			}

		return models.Article.create({
			slug: req.body.slug,
			title: req.body.title,
			content: req.body.content
		});
	})
	.then(function (newArticle) {
		req.flash('success', 'Article created');
		return res.redirect('/administrator/articles')
	})
	.catch(function (err) {
		return res.redirect('back');
	});
}

exports.edit = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Article.findById(req.params.id).then(function(article) {
		if (article!=null) 			
			return res.render('CMS/article/edit', {
				layout: 'CMS/layout/master', 
				article: article
			});
		else
			return res.redirect('/whoops/404');
	})
}

exports.update = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt();
	req.assert('slug', 'Invalid id').isSlug();
	req.assert('title', 'Invalid id').isLength({min: 3});
	req.assert('content', 'Invalid id').isLength({min: 20});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Article.findById(req.params.id).then(function(article) {
		if (article == null) 
			throw {
				type: 404
			}

			return models.Article.count({
				where: {
					slug : {
						$like: req.body.slug
					},
					id : {
						$ne: req.params.id
					}
				}
			});
		})
	.then(function (article) {
		if (article>0) 
			throw {
				type: 403
			}

			return models.Article.update({
				slug: req.body.slug,
				title: req.body.title,
				content: req.body.content
			}, {
				where : {
					id: req.params.id
				}
			});
		})
	.then(function (newArticle) {
		req.flash('success', 'Article updated');
		return res.redirect('/administrator/articles')
	})
	.catch(function (err) {
		if (err.type == 404) 
			return res.redirect('/whoops/404');
		else
			return res.redirect('back');
	});
}