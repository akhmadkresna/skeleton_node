'use strict';
var models  = require(__dirname+'/../../model');


exports.index = function(req, res) 
{
	return res.render('CMS/index', {
		layout: 'CMS/layout/master'
	});
};