'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	var keyword = '';
	var queries = {
		order: 'createdAt DESC',
		include: [{
			model: models.Admin
		}]
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			$or:{
				name: {
					$like: '%'+req.query.keyword+'%'
				}
			}
		}
	}

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Log.findAndCountAll(queries).then(function (log) {
		return res.render('CMS/log/index', {
			layout: 'CMS/layout/master', 
			log: log.rows, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(log.count)/20),
		});
	});

};