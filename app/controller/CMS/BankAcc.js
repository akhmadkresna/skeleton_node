'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	models.Bank.findAll({
		order: 'bankName ASC'
	})
	.then(function (banks) {
		return res.render('CMS/bank/index', {
			layout: 'CMS/layout/master',
			banks: banks
		});
	})
}

exports.create = function(req, res) 
{
	return res.render('CMS/bank/create', {
		layout: 'CMS/layout/master'
	});
}

exports.store = function(req, res) 
{
	req.assert('bankname', 'Invalid bank').notEmpty();
	req.assert('accname', 'Invalid bank').notEmpty();
	req.assert('accnum', 'Invalid bank').notEmpty();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Bank.create({
		bankName: req.body.bankname,
	    accountName: req.body.accname,
	    accountNum: req.body.accnum,
	})
	.then(function (newbank) {
		return res.redirect('/administrator/bank-accounts')
	})
	.catch(function (err) {
		return res.json({err: err})
	})
}

exports.edit = function(req, res) 
{
	req.assert('id', 'Invalid bank').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Bank.findById(req.params.id)
	.then(function (bank) {

		if (bank == null) 
			throw {
				type: 404
			}

		return res.render('CMS/bank/edit', {
			layout: 'CMS/layout/master',
			bank: bank
		});
	})
	.catch(function (err) {
		return res.redirect('/whoops/'+err.type)
	})
}

exports.update = function(req, res) 
{
	req.assert('id', 'Invalid bank').isInt({min:1});
	req.assert('bankname', 'Invalid bank').notEmpty();
	req.assert('accname', 'Invalid bank').notEmpty();
	req.assert('accnum', 'Invalid bank').notEmpty();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Bank.update({
		bankName: req.body.bankname,
	    accountName: req.body.accname,
	    accountNum: req.body.accnum,
	}, {
		where: {
			id: req.params.id
		}
	})
	.then(function (newbank) {
		return res.redirect('/administrator/bank-accounts')
	})
	.catch(function (err) {
		return res.json({err: err})
	})
}

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid bank').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Bank.findById(req.params.id)
	.then(function (bank) {

		if (bank == null) 
			throw {
				type: 404
			}

		return res.render('CMS/bank/show', {
			layout: 'CMS/layout/master',
			bank: bank
		});
	})
	.catch(function (err) {
		return res.redirect('/whoops/'+err.type)
	})
};

exports.destroy = function (req, res) 
{
	req.assert('id', 'Invalid bank').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Bank.destroy({
		where: {
			id: req.params.id
		}
	})
	.then(function (bank) {
		return res.redirect('back');
		
	})
	.catch(function (err) {
		return res.redirect('/whoops/'+err.type)
	})
}