'use strict';
var models  = require(__dirname+'/../../model');

exports.getForgot = function(req, res) 
{
	return res.render('CMS/forgot', {
		layout: 'CMS/layout/singleform'
	});
};

exports.getReset = function(req, res) 
{
	return res.render('CMS/reset', {
		layout: 'CMS/layout/singleform'
	});
};