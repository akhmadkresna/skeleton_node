'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	var keyword = '';
	var queries = new Object();

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			name: {
				$like: '%'+req.query.keyword+'%'
			}
		}
	}

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Amenities.findAndCountAll(queries).then(function (amenities) {
		return res.render('CMS/amenities/index', {
			layout: 'CMS/layout/master', 
			amenities: amenities.rows, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(amenities.count)/20),
		});
	});
};

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	console.log(req.params.id)

	models.Amenities.findById(req.params.id,{
		include: [{
			model: models.Listing
		}]
	}).then(function (amenity) {
		if (amenity === null) 
			return res.redirect('/whoops/404');

		return res.render('CMS/amenities/show', {
			layout: 'CMS/layout/master', 
			amenity: amenity
		});
	}).catch(function (err) {
		console.log(err)
		return res.redirect('/whoops/500');
	});
};

exports.create = function(req, res) 
{
	return res.render('CMS/amenities/create', {
		layout: 'CMS/layout/master'
	});
};

exports.store = function(req, res) 
{
	req.assert('name', 'Invalid name').isLength({min: 2});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Amenities.count({
		where: {
			$like: {
				name: req.body.name
			}
		}
	}).then(function (total) {
		if (total>0) 
		{
			return res.redirect('/whoops/500');
		}

		models.Amenities.create({
			name : req.body.name
		}).then(function (user) {
			return res.status(201).json({'user' : user});
			
		}).catch(function (err) {
			return res.status(500).json({'error' : err});
		});

	}).catch(function (err) {
		
	});
};