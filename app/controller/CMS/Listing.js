'use strict';
var models  = require(__dirname+'/../../model');


exports.index = function(req, res) 
{
	var keyword = '';

	var status = (req.query.status=='' || req.query.status==null)?'all':req.query.status;

	if (status != 'suspended' && status!= 'active') 
		status = 'all';

	var queries = {
		order: 'createdAt DESC'
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			$or:{
				name: {
					$like: '%'+req.query.keyword+'%'
				}
			}
		}
	}

	if (status != 'all') 
		queries.where['suspended'] = (status=='suspended')?true:false;

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Listing.findAndCountAll(queries).then(function (listings) {
		return res.render('CMS/listing/index', {
			layout: 'CMS/layout/master', 
			listings: listings.rows, 
			keyword:keyword, 
			status: status,
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(listings.count)/20),
		});
	});

};

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findById(req.params.id, {
		include: [{
			model: models.Type
		},{
			model: models.LocationType
		}, {
			model: models.Amenities
		}, {
			model: models.User
		}, {
			model: models.Reservation,
			include: [{
				model: models.User,
				as : 'Traveller'
			}]
		}]
	}).then(function (listing) {
		if (listing === null)
			return res.status(404).json({'error' : 'Not found'});

		// return res.json({listing: listing});

		return res.render('CMS/listing/show', {
			layout: 'CMS/layout/master', 
			listing: listing
		});
	});
}

exports.edit = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findById(req.params.id, {
		include: [{
			model: models.Amenities
		}]
	}).then(function (listing) {
		if (listing === null)
			return res.status(404).json({'error' : 'Not found'});

		return res.render('CMS/listing/show', {
			layout: 'CMS/layout/master', 
			listings: listings
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.update = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findById(req.params.id, {
		include: [{
			model: models.Amenities
		}]
	}).then(function (listing) {
		if (listing === null)
			return res.status(404).json({'error' : 'Not found'});

		return res.render('CMS/listing/show', {
			layout: 'CMS/layout/master', 
			listings: listings
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.suspend = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findById(req.params.id).then(function (listing) {
		if (listing === null)
			return res.status(404).json({'error' : 'Not found'});

		return listing.update({
			suspended: true
		});
	})
	.then(function (updated) {
		req.flash('success', 'Listing set to suspended');
		return res.redirect('back');
	})
	.catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.unsuspend = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findById(req.params.id).then(function (listing) {
		if (listing === null)
			return res.status(404).json({'error' : 'Not found'});

		return listing.update({
			suspended: false
		});
	})
	.then(function (updated) {
		req.flash('success','Listing set to active');
		return res.redirect('back');
	})
	.catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}