var models  = require(__dirname+'/../../model');

exports.guestToday = function(req, res) 
{
	var q = "SELECT COUNT(finished) AS total FROM reservation WHERE DATE(startDate) =  DATE(NOW())";

	var whereClause = '';

	if (typeof req.query.listing != 'undefined') 
	{
		whereClause += 'AND listingId='+parseInt(req.query.listing)+' AND ownerId='+parseInt(req.query.user);
	}

	models.sequelize.query(q, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total[0].total == null)?0:total[0].total});
	});
}

exports.guest = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);

	if (typeof req.query.listing != 'undefined') 
	{
		req.assert('listing', 'Invalid listing').isInt({min:1});
		req.assert('user', 'Invalid user').isInt({min:1});
	}
	else if (typeof req.query.user != 'undefined') 
		req.assert('user', 'Invalid user').isInt({min:1});


	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( startDate > DATE_SUB(now(), INTERVAL 4 YEAR) )";
	var grouping = " GROUP BY YEAR(startDate) DESC;"

	if (req.query.interval === 'monthly') 
	{
		intervalq = "( startDate > DATE_SUB(now(), INTERVAL 12 MONTH) )";
		grouping = " GROUP BY YEAR(startDate), MONTH(startDate) DESC;"
	}

	if (req.query.interval === 'weekly') 
	{
		intervalq = "( startDate > DATE_SUB(now(), INTERVAL 7 DAY) )";
		grouping = " GROUP BY DATE(startDate) DESC;"
	}

	if (req.query.interval === 'weekly') 
	{
		intervalq = "( createdAt > DATE_SUB(now(), INTERVAL 3 WEEK) )";
		grouping = " GROUP BY WEEKOFYEAR(createdAt) DESC;";
	}

	var q = "SELECT COUNT(finished) AS total, YEAR(startDate) AS theyear, MONTH(startDate) AS themonth, WEEKOFYEAR(createdAt) AS theweek FROM reservation WHERE "+intervalq+grouping;


	var whereClause = '';

	if (typeof req.query.listing != 'undefined') 
	{
		whereClause += 'AND listingId='+parseInt(req.query.listing)+' AND ownerId='+parseInt(req.query.user);
	}

	models.sequelize.query(q, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : transform(req.query.interval, total)});

		return res.status(200).json({'total' : total});
	});
};

exports.moneyToday = function(req, res) 
{
	var q = "SELECT SUM(cost) AS total FROM reservation WHERE paid=1 AND DATE(startDate) = CURDATE()";

	var whereClause = '';

	if (typeof req.query.listing != 'undefined') 
	{
		whereClause += ' AND listingId='+parseInt(req.query.listing)+' AND ownerId='+parseInt(req.query.user);
	}

	models.sequelize.query(q+whereClause, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : total });
	});
}

exports.money = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);

	if (typeof req.query.listing != 'undefined') 
	{
		req.assert('listing', 'Invalid listing').isInt({min:1});
		req.assert('user', 'Invalid user').isInt({min:1});
	}
	else if (typeof req.query.user != 'undefined') 
		req.assert('user', 'Invalid user').isInt({min:1});


	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});


	var selects = " CONCAT( MONTH(startDate), ' - ', YEAR(startDate)) AS month";
	var intervalq = "( startDate > DATE_SUB(now(), INTERVAL 12 MONTH) )";
	var grouping = " GROUP BY months DESC;"

	if (req.query.interval === 'monthly') 
	{
		selects = " CONCAT('week ', (WEEK(startDate,5) - WEEK(DATE_SUB(startDate, INTERVAL DAYOFMONTH(startDate)-1 DAY),5)+1), ' / ', MONTH(startDate), ' ', YEAR(startDate)) as weeks ";
		intervalq = "( startDate > DATE_SUB(now(), INTERVAL 8 WEEK) )";
		grouping = " GROUP BY weeks DESC;"
	}

	if (req.query.interval === 'weekly') 
	{
		selects = " DATE(startDate)";
		intervalq = "( startDate > DATE_SUB(now(), INTERVAL 7 DAY) )";
		grouping = " GROUP BY DATE(startDate) DESC;"
	}

	var q = "SELECT SUM(cost) AS total ,"+selects+" FROM reservation WHERE paid=1 AND "+intervalq+grouping;

	var whereClause = '';

	if (typeof req.query.listing != 'undefined') 
	{
		whereClause += 'AND listingId='+parseInt(req.query.listing)+' AND ownerId='+parseInt(req.query.user);
	}

	models.sequelize.query(q, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		
		return res.status(200).json({'total' : total});
	});
};

exports.tractionToday = function(req, res) 
{
	var q = "SELECT COUNT(id) AS total FROM user WHERE DATE(createdAt) = DATE(NOW())";

	models.sequelize.query(q, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total[0].total == null)?0:total[0].total});
	});
}

exports.traction = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( createdAt > DATE_SUB(now(), INTERVAL 12 MONTH) )";
	var grouping = " GROUP BY YEAR(createdAt) DESC;"
	var addSelect = "";

	if (req.query.interval === 'yearly') 
	{
		intervalq = "( createdAt > DATE_SUB(now(), INTERVAL 4 YEAR) )";
	}

	if (req.query.interval === 'monthly') 
	{
		intervalq = "( createdAt > DATE_SUB(now(), INTERVAL 12 MONTH) )";
		grouping = " GROUP BY YEAR(createdAt), MONTH(createdAt) DESC;"
		addSelect += ", MONTH(createdAt) AS themonth";
	}

	if (req.query.interval === 'weekly') 
	{
		intervalq = "( createdAt > DATE_SUB(now(), INTERVAL 3 WEEK) )";
		grouping = " GROUP BY WEEKOFYEAR(createdAt) DESC;"
		addSelect += ", WEEKOFYEAR(createdAt) AS theweek";
	}

	var q = "SELECT COUNT(id) AS total, YEAR(createdAt) AS theyear "+addSelect+" FROM user WHERE "+intervalq+grouping;

	models.sequelize.query(q, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : transform(req.query.interval, total)});
		return res.status(200).json({'total' : total});
	});
};

function transform(interval, data) 
{
	var returns = [];
	if (interval === 'monthly') {
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

		var now = new Date();
		var bottom = new Date();
		bottom.setFullYear(bottom.getFullYear() - 1);

		for (var i = bottom; i <= now; i.setMonth(bottom.getMonth() + 1)) 
		{
			var currentTotal = 0;
			for (var j = data.length - 1; j >= 0; j--) 
			{
				if (data[j].themonth+' - '+data[j].theyear == (i.getMonth()+1)+" - "+i.getFullYear()) 
					currentTotal = data[j].total
			}

			returns.push({
				y: monthNames[i.getMonth()]+" - "+i.getFullYear(),
				a: currentTotal
			});
		}

		data.forEach(function (data, index) {
			returns
		});
		return returns;
	}
	else if (interval === 'yearly') {
		var bottom = new Date();

		for (var i = 4 - 1; i >= 0; i--) 
		{
			var found = false;
			data.forEach(function (results, index) {
				if (results.theyear == bottom.getFullYear() - i) {
					returns.push({
						y: bottom.getFullYear() - i,
						a: results.total
					});
					found = true;
				}
			})

			if (!found) {
				returns.push({
					y: bottom.getFullYear() - i,
					a: 0
				});
			}
		}

		return returns;
	}
	else if (interval === 'weekly') {
		var now = new Date();
		var bottom = new Date();
		bottom.setDate(bottom.getDate() - 21);

		var returns = []
		,count = 4;

		for (var i = bottom; i <= now; i.setDate(i.getDate()+7)) {
			var found = false;
			var caption = '';
			count -= 1;
			
			if (getWeekNumber(i) == getWeekNumber(now)) {
				caption += 'this week';
			}
			else {
				caption += count+' week before';
			}
			
			data.forEach(function (results, index) {
				if (results.theweek == getWeekNumber(i)) {
					returns.push({
						y: caption,
						a: results.total
					});
					found = true;
				}
			})

			if (!found) {
				returns.push({
					y: caption,
					a: 0
				});
			}
		}
		
		return returns;
	}
}

function getWeekNumber(d) {

	d = new Date(+d);
	d.setHours(0,0,0,0);
	d.setDate(d.getDate() + 4 - (d.getDay()||7));

	var yearStart = new Date(d.getFullYear(),0,1);

	var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);

	return weekNo;
}