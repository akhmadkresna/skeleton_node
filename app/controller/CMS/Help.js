'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');

exports.index = function(req, res) 
{
	var keyword = '';
	var queries = {
		include:[{
			model: models.Admin
		},
		{
			model: models.User
		}]
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			subject: {
				$like: '%'+req.query.keyword+'%'
			}
		}
	}

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Help.findAndCountAll(queries).then(function (help) {
		return res.render('CMS/help/index', {
			layout: 'CMS/layout/master', 
			help: help.rows, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(help.count)/20),
		});
	});
};

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Help.findById(req.params.id,{
		include: [{
			model: models.User
		},{
			model: models.Admin
		}]
	}).then(function (help) {
		if (help === null) 
			return res.redirect('/whoops/404');

		return res.render('CMS/help/show', {
			layout: 'CMS/layout/master', help: help
		});
	}).catch(function (err) {
		return res.redirect('/whoops/404')
	});
};

exports.update = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});
	req.assert('reply', 'Invalid reply').isLength({min:10});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var thehelp;

	models.Help.findById(req.params.id, {
		include: [{
			model: models.User
		}]
	})
	.then(function (help) {
		if (help === null) 
			throw {
				type: 404,
				message: 'not found'
			}	
		
		thehelp = help;

		// return res.json({err: thehelp})

		return models.Help.update({
			reply: req.body.reply,
			adminId: req.user.id
		}, {
			where: {
				id: req.params.id
			}
		})
	})
	.then(function (updated) {
		var mails = [{
			to: thehelp.User.email,
			subject: 'Help request reply - RoomMe',
			template: 'help/reply',
			data: {
				help: thehelp,
				reply: req.body.reply
			}
		}]

		var sendmail =  sender.email(mails);
		return res.redirect('/administrator/help')
	})
	// .catch(function (err) {
	// 	return res.redirect('/whoops/404')
	// });
};