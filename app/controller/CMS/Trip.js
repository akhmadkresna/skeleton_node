'use strict';
var models  = require(__dirname+'/../../model');
var string  = require(__dirname+'/../../helper/string');

exports.index = function(req, res) 
{
	var pendq = '(SELECT COUNT(payment.id) FROM payment WHERE payment.reservationCode=Reservation.code AND payment.status = 1)'

	var keyword = '';
	var queries = {
		order: [['createdAt', 'DESC']],
		attributes: [
		'code',
		'status', 
		'createdAt', 
		'approved', 
		'paid', 
		'canceled',
		'finished', 
		'declined',
        [models.sequelize.literal(pendq), 'pending'],
		],
		include: [{
			model: models.User,
			as: 'Traveller'
		},{
			model: models.Listing
		}]
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
	}

	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Reservation.findAndCountAll(queries).then(function (trips) {

		var returns = [];

		trips.rows.forEach(function (data, index) {
			data.dataValues.status = string.tripStatus(data);

			if (data.dataValues.status === 'Accepted' && data.dataValues.pending>0) 
				data.dataValues.status = 'Payment pending';

			returns.push(data.dataValues);
		})
		// return res.json({data: returns})
		return res.render('CMS/trip/index', {
			layout: 'CMS/layout/master', 
			trips: returns, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(trips.count)/20),
		});
	});
};

exports.show = function(req, res) 
{	
	req.assert('code', 'Invalid code').isAlphanumeric();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404');

	models.Reservation.findOne({
		where: {
			code: req.params.code
		},
		include: [{
			model: models.User,
			as: 'Traveller'
		},{
			model: models.Listing
		},{
			model: models.Payment
		}]
	}).then(function (trip) {

		if (trip == null) 
			return res.status(404).json({err: 'not found'})

		let returns = {};
		
		trip.dataValues.status = string.tripStatus(trip);

		if (trip.dataValues.Payments.length > 0) 
		{
			let pending = 0;

			trip.dataValues.Payments.forEach(function (data, index) {
				if (data.status === 1) 
					pending += 1;
			})			

			if (trip.dataValues.status === 'Accepted' && pending>0) 
				trip.dataValues.status = 'Payment pending';
		}

		returns = trip.dataValues;

		return res.render('CMS/trip/show', {
			layout: 'CMS/layout/master',
			trip: returns
		});
	});

}