'use strict';
var models  = require(__dirname+'/../../model');
var sender  = require(__dirname+'/../../helper/sender');
var push  = require(__dirname+'/../../helper/push');

exports.index = function(req, res) {
	models.Payment.findAll({
		order: 'createdAt DESC'
	})
	.then(function (payments) {
		return res.render('CMS/payment/index', {
			layout: 'CMS/layout/master', 
			payments: payments
		});
	})
} 

exports.show = function(req, res) {
	req.assert('id', 'Invalid code').isUUID();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404');

	models.Payment.findById(req.params.id, {
		include: [{
			model: models.Reservation,
			include: [{
				model: models.User,
				as: 'Traveller'
			}, {
				model: models.Listing
			}]
		}]
	})
	.then(function (payment) {
		return res.render('CMS/payment/show', {
			layout: 'CMS/layout/master', 
			payment: payment
		});
	})
}

exports.update = function(req, res) {

}  

exports.accept = function(req, res) 
{
	req.assert('id', 'Invalid code').isUUID();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404');

	var curpayment;

	models.Payment.findById(req.params.id, {
		include: [{
			model: models.Reservation,
			include: [{
				model: models.User,
				as: 'Traveller'
			}, {
				model: models.User,
				as: 'Host'
			}, {
				model: models.Listing
			}]
		}]
	})
	.then(function (payment) {
		if (payment == null) 
			return res.redirect('/whoops/404');

		curpayment= payment;

		return models.sequelize.transaction(function (t) {
			return payment.update({
				status: 2
			}, {transaction: t})
			.then(function (updated) {
				var q = "UPDATE reservation SET paid = 1 WHERE code = '"+payment.reservationCode+"'";

				return models.sequelize.query(q, {transaction: t});
			})	
		})
	})
	.then(function (result) {
		var mails = [{
			to: curpayment.Reservation.Traveller.email,
			subject: 'Payment for '+curpayment.reservationCode+' approved',
			template: 'payment/approved',
			data: {
				payment: curpayment
			}
		}, {
			to: curpayment.Reservation.Host.email,
			subject: 'Payment for '+curpayment.reservationCode+' approved',
			template: 'payment/approvedHost',
			data: {
				payment: curpayment
			}
		}]

		var sendmail =  sender.email(mails);

		if (curpayment.Reservation.Traveller.device == 'ios') 
			var test =  push.pushIOS(curpayment.Reservation.Traveller.pushToken, 'Payment confirmation received', {});
		else if (curpayment.Reservation.Traveller.device == 'android') 
			var test =  push.pushGCM(curpayment.Reservation.Traveller.pushToken, 'Payment confirmation received', {});

		req.flash('success', 'Payment accepted!')

		return res.redirect('/administrator/trips/'+curpayment.reservationCode)
	});
};

exports.decline = function(req, res) 
{
	req.assert('id', 'Invalid code').isUUID();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.redirect('/whoops/404');

	var curpayment;

	models.Payment.findById(req.params.id, {
		include: [{
			model: models.Reservation,
			include: [{
				model: models.User,
				as: 'Traveller'
			}, {
				model: models.Listing
			}]
		}]
	})
	.then(function (payment) {
		if (payment == null) 
			return res.redirect('/whoops/404');

		curpayment= payment;

		return payment.update({status: 0})
	})
	.then(function (updated) {
		var mails = [{
			to: curpayment.Reservation.Traveller.email,
			subject: 'Payment for '+curpayment.reservationCode+' declined',
			template: 'payment/declined',
			data: {
				payment: curpayment
			}
		}]

		var sendmail =  sender.email(mails);

		if (curpayment.Reservation.Traveller.device == 'ios') 
			var test =  push.pushIOS(curpayment.Reservation.Traveller.pushToken, 'Payment confirmation ('+curpayment.Reservation.code+') declined', {});
		else if (curpayment.Reservation.Traveller.device == 'android') 
			var test =  push.pushGCM(curpayment.Reservation.Traveller.pushToken, 'Payment confirmation ('+curpayment.Reservation.code+') declined', {});

		req.flash('success', 'Payment declined!')

		return res.redirect('/administrator/trips/'+curpayment.reservationCode)
	});
};