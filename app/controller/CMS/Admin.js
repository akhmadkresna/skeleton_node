'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	var keyword = '';
	var queries = {
		include:[{
			model: models.Groups
		}],
		order: 'email ASC'
	};

	if (req.query.keyword != null) 
	{
		req.sanitizeQuery('keyword').escape();

		keyword = req.query.keyword;
		
		queries['where'] = {
			$or:{
				fname: {
					$like: '%'+req.query.keyword+'%'
				},
				lname: {
					$like: '%'+req.query.keyword+'%'
				},
				email: {
					$like: '%'+req.query.keyword+'%'
				}
			}
		}
	}
	
	queries.offset = ((req.query.page === undefined?0:(req.query.page - 1)))*20;
	queries.limit = 20;

	models.Admin.findAndCountAll(queries).then(function (admin) {

		return res.render('CMS/admin/index', {
			layout: 'CMS/layout/master', 
			admin: admin.rows, 
			keyword:keyword, 
			currentpage: (req.query.page === undefined?1:req.query.page),
			totalPage: Math.ceil(parseInt(admin.count)/20),
		});
	});
};

exports.create = function(req, res) 
{
	models.Groups.findAll().then(function (priv) {
		console.log(req.flash('errors'));
		return res.render('CMS/admin/create', {
			layout: 'CMS/layout/master', 
			priv: priv, 
			errors: req.flash('errors'),
			'formdata': req.flash('formData')
		});
	}).catch(function (err) {
		return res.json({err: err});
	});
};

exports.store = function (req, res) 
{
	req.assert('email', 'Invalid email').isEmail({min:0});
	req.assert('password', 'Invalid password').isLength({min:6});
	req.assert('cpassword', 'Invalid cpassword').equals(req.body.password);
	req.assert('fname', 'Invalid fname').isLength({min:2});
	req.assert('lname', 'Invalid lname').isLength({min:2});
	req.assert('privilege', 'Invalid privilege').isInt({min:1});

	var errors = req.validationErrors();

	if (errors.length>0)
	{
		req.flash('errors', errors);
		return res.redirect('/administrator/admins/create');
	}

	models.Admin.count({
		where: {
			email: {
				$like: req.body.email
			}
		}
	}).then(function (total) {
		if (total > 0) 
		{
			req.flash('errors', 'Email already used');
			return res.redirect('/administrator/admins/create');
		}
		
		models.Admin.create({
			groupId: req.body.privilege,
			fname : req.body.fname,
			lname : req.body.lname,
			email: req.body.email,
			password: req.body.password,
		}).then(function (admin) {
			req.flash('success', 'Admin user created successfully!');
			return res.redirect('/administrator/admins');
		});
	});

}