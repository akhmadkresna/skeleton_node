'use strict';

const APIFormat = require(__dirname+'/../../helper/APIFormat')
, models = require(__dirname+'/../../model');

module.exports = {
	reservation(req, res) {
		return models.Reservation.count({
			where: {
				ownerId: req.decoded.id,
				approved : 0,
				canceled : 0
			}
		})
		.then(function (total) {
			return res.status(200).json( APIFormat.response( '', {total: total} ) );
		})
		.catch(function (err) {
			if (err.status != null) 
				return res.status(err.status).json( APIFormat.response(err.message, {}, err.errors) );
			else
				return res.status(500).json( APIFormat.response('internal server error', {}, err) );
		})

	},
}