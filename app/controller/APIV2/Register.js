'use strict';

const APIFormat = require(__dirname+'/../../helper/APIFormat')
, sender = require(__dirname+'/../../helper/sender')
, models = require(__dirname+'/../../model');

module.exports = {
	store(req, res) {
		req.assert('email', 'Invalid email').isEmail();
		req.assert('password', 'Invalid password').isLength(6);
		req.assert('fname', 'Please enter a valid first name').isLength(2);
		req.assert('lname', 'Please enter a valid last name').isLength(2);

		if (req.body.birthdate != null) 
			req.assert('birthdate', 'Invalid birthdate').isLength(6).isDate().isAfter('1899-12-31');

		var errors = req.validationErrors();

		if (errors.length>0) 
			return res.status(400).json( APIFormat.response( 'Form Error', {}, errors ) );
		
		return models.sequelize.transaction(function (t) {
			return models.User.count({
				where : {
					$or: { 
						email: req.body.email
					}
				}
			})
			.then(function (total) {
				if (total>0) {
					throw {
						status: 400,
						message: 'Email already used',
						errors: {}
					}
				}
				
			})
			.then(function (total) {
				if (total > 0) {
					throw {
						status: 400,
						message: 'Contains restricted words',
						errors: {}
					}	
				}
				return models.User.create({
					email : req.body.email,
					password : req.body.password,
					firstName : req.body.fname,
					lastName : req.body.lname,
					birthDate : (req.body.birthdate!=null)?req.body.birthdate:null,
					emailActive : 1
				}, {transaction: t})
			})
			.then(function (user) {
				return models.Settings.create({
					userId: user.id
				}, {transaction: t});
			})
		})
		.then(function (success) {
			return res.status(201).json( APIFormat.response( 'Success', {userId: success.dataValues.userId} ) );
		})
		.catch(function (err) {
			if (err.status != null) 
				return res.status(err.status).json( APIFormat.response(err.message, {}, err.errors) );
			else
				return res.status(500).json( APIFormat.response('internal server error', {}, err) );
		})
	},

}