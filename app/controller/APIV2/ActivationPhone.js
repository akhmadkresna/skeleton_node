'use strict';

const APIFormat = require(__dirname+'/../../helper/APIFormat')
, sender = require(__dirname+'/../../helper/sender')
, message = require(__dirname+'/../../helper/Message')
, models = require(__dirname+'/../../model')
, string  = require(__dirname+'/../../helper/string');


module.exports = {
	store(req, res) {
		req.assert('email', 'Please enter a valid email').isEmail();
		req.assert('phone', 'Please enter a valid phone number').isNumeric();

		var errors = req.validationErrors();

		if (errors.length>0) 
			return res.status(400).json( APIFormat.response( 'Form Error', {}, errors ) );

		const smsCode = string.random(5,1).toUpperCase();

		return models.User.count({
			where: {
				phone: req.body.phone,
				email: {
					$ne: req.body.email
				}
			}
		})
		.then(function (total) {
			if (total > 0) {
				throw {
					status: 400,
					message: 'Phone already used',
					errors: {}
				}
			}

			return models.User.update({
				phone: req.body.phone,
				phoneActivationCode: smsCode
			}, {
				where: {
					email: req.body.email
				}
			});
		})
		.then(function (updated) {
			if (updated[0] == 0) {
				throw {
					status: 404,
					message: 'User not found',
					errors: {}
				}
			}
			sender.sms('Your Roomme verification code is '+smsCode, req.body.phone);
			return true;
			// return message.textNexmo(req.body.phone, 'Your Roomme verification code is '+smsCode);
		})
		.then(function (success) {
			return res.status(200).json( APIFormat.response( 'Activation sent', {phone: req.body.phone} ) );
		})
		.catch(function (err) {
			console.log(err)
			if (err.status != null) 
				return res.status(err.status).json( APIFormat.response(err.message, {}, err.errors) );
			else
				return res.status(500).json( APIFormat.response('internal server error', {}, err) );
		})

	},

	update(req, res) {
		req.assert('code', 'Please enter a valid code').isLength({min: 4, max: 6});
		req.assert('phone', 'Sorry your activation code is incorrect').isNumeric();

		var errors = req.validationErrors();

		if (errors.length>0) 
			return res.status(400).json( APIFormat.response( 'Form Error', {}, errors ) );

		return models.User.update({
			phoneActivationCode: null,
			phoneActive : true
		}, {
			where: {
				phoneActivationCode: req.body.code,
				phone: req.body.phone,
			}
		})
		.then(function (updated) {
			if (updated[0] == 0) {
				throw {
					status: 404,
					message: 'Sorry your activation code is incorrect',
					errors: {}
				}
			}

			return models.User.findOne({
				where: {
					phone : req.body.phone
				},
				attributes: [
				"email",
				"firstName",
				"lastName"
				]
			});

		})
		.then(function (user) {
			var mails = [{
				to: user.email,
				subject: 'Registration Success - RoomMe',
				template: 'account/registration',
				data: {
					name: user.firstName+" "+user.lastName,
				}
			}]

			var sendmail =  sender.email(mails);

			return res.status(200).json( APIFormat.response( 'Phone activated', {phone: req.body.phone} ) );
		})
		.catch(function (err) {
			if (err.status != null) 
				return res.status(err.status).json( APIFormat.response(err.message, {}, err.errors) );
			else
				return res.status(500).json( APIFormat.response('internal server error', {}, err) );
		})

		
		
	},
}