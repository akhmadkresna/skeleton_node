'use strict';

const APIFormat = require(__dirname+'/../../helper/APIFormat')
, models = require(__dirname+'/../../model');

module.exports = {
	finish(req, res) {
		req.assert('code', 'Invalid trip code').notEmpty();

		var errors = req.validationErrors();

		if (errors.length>0) 
			return res.status(400).json( APIFormat.response( 'Form Error', {}, errors ) );

		models.Reservation.update({
			finished: 1
		},{
			where: {
				ownerId: req.decoded.id,
				finished : 0,
				canceled : 0,
				code: req.params.code
			}
		})
		.then(function (updated) {
			if (updated[0] < 1) 
				return res.status(304).json( APIFormat.response( 'No trip updated', {} ) );
			else
				return res.status(200).json( APIFormat.response( 'Trip finished', {} ) );
		})
		.catch(function (err) {
			if (err.status != null) 
				return res.status(err.status).json( APIFormat.response(err.message, {}, err.errors) );
			else
				return res.status(500).json( APIFormat.response('internal server error', {}, err) );
		})

	},
}