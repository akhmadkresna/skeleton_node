'use strict';
var models  = require(__dirname+'/../../model');

exports.notifShow = function(req, res) 
{
	models.Settings.findOne({
		where: {
			userId: req.decoded.id
		}
	}).then(function (setting) {
		return res.status(200).json({setting: setting});
	}).catch(function (err) {
		return res.status(500).json({error: err});
	});
};

exports.notifUpdate = function(req, res) 
{
	req.assert('app', 'Invalid app setting').isInt({min:0, max:1});
	req.assert('sms', 'Invalid sms setting').isInt({min:0, max:1});
	req.assert('email', 'Invalid email setting').isInt({min:0, max:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Settings.findOne({
		where: {
			userId: req.decoded.id
		}
	}).then(function (setting) {
		setting.update({
			notifApp:req.body.app,
			notifMail:req.body.email,
			notifSMS:req.body.sms
		}).then(function (updated) 
		{
			return res.status(200).json({'message' : 'updated', 'setting': updated});
		});
	}).catch(function (err) {
		return res.status(500).json({error: err});
	});
};
