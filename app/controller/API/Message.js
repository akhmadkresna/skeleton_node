'use strict';

var models  = require(__dirname+'/../../model');

var push  = require(__dirname+'/../../helper/push');

exports.index = function(req, res) 
{
	req.assert('type', 'Invalid type').isIn(['host', 'traveller']);
	req.assert('total', 'Invalid total').isInt({min:1});
	req.assert('cursor', 'Invalid cursor').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var operator = '';

	if (req.query.type === 'traveller') 
		var operator = '!';

	var q = 'SELECT m.id, m.content, m.createdAt, m.toId, m.fromId,\
	u.firstName, u.lastName, u.picture, u.id AS userId,\
	l.name as listingName, l.id as listingId, l.userId as hostId,\
	(SELECT COUNT(*) FROM message as m2 WHERE m2.toId = ? AND m2.fromId=u.id AND m2.listingId = l.id AND m2.delivered = 0) as unread\
	FROM message m\
	JOIN (\
	SELECT MAX(createdAt) as createdAt, listingId, CASE \
	WHEN fromId = ? THEN toId\
	WHEN toId = ? THEN fromId\
	END AS otherUser  \
	FROM message\
	GROUP BY listingId, CASE \
	WHEN fromId = ? THEN toId\
	WHEN toId = ? THEN fromId\
	END\
	HAVING otherUser IS NOT NULL\
	) AS other\
	ON m.createdAt = other.createdAt\
	JOIN user u ON ((u.id = m.toId OR u.id = m.fromId) AND u.id !=? )\
	JOIN listing l ON (l.id = m.listingId AND l.userId'+operator+'=?)\
	WHERE m.deletedAt IS NULL\
	LIMIT ? OFFSET ?;';

	models.sequelize.query(q, {
		raw: true, 
		replacements: [
		req.decoded.id, 
		req.decoded.id, 
		req.decoded.id, 
		req.decoded.id, 
		req.decoded.id, 
		req.decoded.id, 
		req.decoded.id, 
		parseInt(req.query.total), 
		parseInt(req.query.total)*parseInt(req.query.cursor)],
		type: models.sequelize.QueryTypes.SELECT
	}).then(function(messages) 
	{
		return res.status(200).json({'messages' : messages, 'page' : parseInt(req.query.cursor), 'countperpage' : parseInt(req.query.total)});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.lastTrip = function(req, res) 
{
	req.assert('userid', 'Invalid user').isInt({min:1});
	req.assert('listingid', 'Invalid listing').isInt({min:1});
	// req.assert('type', 'Invalid type').isIn(['prev', 'next']);
	// req.assert('timestamp', 'Invalid timestamp').isIn(['prev', 'next']);
	// req.assert('total', 'Invalid total').isInt();
	// req.assert('cursor', 'Invalid cursor').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			userId: parseInt(req.params.userid),
			listingId: parseInt(req.params.listingid),
			finished: 0,
			canceled: 0,
			declined: 0,
		},
		order: 'createdAt DESC',
		include: [{
			model: models.Listing,
			attributes: ["id", "image1", "image2", "image3", "image4", "image5", "name", "guest"],
			include: [{
				model: models.Type,
				attributes: ["name"]
			}]
		}]
	})
	.then(function (trip) {
		return res.status(200).json({'trip' : trip});
	})
}

exports.show = function(req, res) 
{
	req.assert('userid', 'Invalid user').isInt({min:1});
	req.assert('listingid', 'Invalid listing').isInt({min:1});
	req.assert('type', 'Invalid type').isIn(['prev', 'next']);
	// req.assert('timestamp', 'Invalid timestamp').isIn(['prev', 'next']);
	// req.assert('total', 'Invalid total').isInt();
	// req.assert('cursor', 'Invalid cursor').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var query = {
		$and: [{
			$or: [{
				toId: req.decoded.id,
			},{
				toId: req.params.userid,
			}]
		},{
			$or: [{
				fromId: req.decoded.id,
			},{
				fromId: req.params.userid,
			}]
		}],
		listingId: req.params.listingid,		
	}

	if (req.query.timestamp != null) 
	{
		if (req.query.type == 'prev') 
		{
			query.createdAt = {
				$lte: req.query.timestamp
			}
		}
		else if (req.query.type == 'next') 
		{
			query.createdAt = {
				$gte: req.query.timestamp
			}	
		}
	}

	var newmessages;

	models.Message.findAll({
		where: query,
		limit: (req.query.timestamp==null)?40:99,
		// offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		attributes: ['id', 'fromId', 'toId', 'listingId', 'content', 'createdAt'],
		order: 'createdAt desc'
	}).then(function (messages) {
		newmessages = messages;
		if (messages.length>0) 
		{
			var oldest = messages[0].createdAt; 

			return models.Message.update({
				delivered: true
			},{
				where: {
					toId: req.decoded.id,
					fromId: req.params.userid,
					listingId: req.params.listingid,
					createdAt : {
						$lte: oldest
					},
					delivered: false

				}
			});
		}
		else
			return true;
	}).then(function (updated) {
		return res.status(201).json({messages: newmessages, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	})
	.catch(function (err) {
		return res.status(500).json({error: err});
	});
}

exports.store = function(req, res) 
{
	req.assert('userid', 'Invalid user').isInt({min:1});
	req.assert('userid', 'Invalid user').notEqual(req.decoded.id);
	req.assert('listingid', 'Invalid listing').isInt({min:1});
	req.assert('message', 'Invalid message').isLength({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var host = false;

	models.Listing.findById(req.params.listingid)
	.then(function (listing) {
		if (listing == null) 
			return res.status(404).json({'message': 'listing not found'});

		host = (req.decoded.id == listing.userId);

		return models.Message.create({
			fromId:req.decoded.id,
			toId: req.params.userid,
			listingId: req.params.listingid,
			content: req.body.message
		})
	})
	.then(function (message) {
		return models.User.findById(req.params.userid, {
			attributes: ["device", "pushToken"]
		});
	})
	.then(function (user) {
		// console.log(user.device)
		// console.log(user.device == 'ios')
		// console.log(user.pushToken)
		// console.log(user.dataValues.pushToken)

		if (user.device == 'ios') 
		{
			var test =  push.pushIOS(user.pushToken, 'Message received ('+(!host?'host':'traveller')+')', {});
		}
		else if (user.device == 'android') 
		{
			var test =  push.pushGCM(user.pushToken, 'Message received ('+(!host?'host':'traveller')+')', {});
		}

		return res.status(201).json({'message': 'sent'});
	})
	// .catch(function (err) {
	// 	return res.status(500).json({'error': err});
	// });
}

exports.destroy = function(req, res) 
{
	req.assert('id', 'Invalid id').isUUID();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Message.findOne({
		where: {
			id: req.params.id,
			fromId: req.decoded.id
		}
	}).then(function (message) {
		if (message == null) 
			return res.status(404).json({'error' : 'not found'});

		return message.destroy();
	}).then(function (deleted) {
		return res.status(200).json({'message' : 'deleted'});
	});

}

exports.destroyConvo = function(req, res) 
{
	req.assert('listingid', 'Invalid id').isInt({min: 1});
	req.assert('userid', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Message.destroy({
		where: {
			listingId: req.params.listingid,
			$or : [{
				toId: req.params.userid,
				fromId: req.decoded.id
			}, {
				toId: req.decoded.id,
				fromId: req.params.userid
			}]
		}
	}).then(function (message) {
		return res.status(200).json({message : 'deleted'});
	});
}