'use strict';
var models  = require(__dirname+'/../../model');

var push  = require(__dirname+'/../../helper/push');
var sender  = require(__dirname+'/../../helper/sender');

var string  = require(__dirname+'/../../helper/string');

var logger  = require(__dirname+'/../../helper/logger');

exports.store = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();
	req.assert('phone', 'Invalid phone').isNumeric();
	req.assert('password', 'Invalid password').isLength(6);
	req.assert('fname', 'Invalid fname').isLength(2);
	req.assert('lname', 'Please enter a valid last name').isLength(2);

	if (req.body.birthdate != null) 
		req.assert('birthdate', 'Invalid birthdate').isLength(6).isDate().isAfter('1899-12-31');


	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var actCode = string.random(5,1).toUpperCase();
	var smsCode = string.random(5,1).toUpperCase();

	models.User.count({
		where : {
			$or: { 
				email: req.body.email,
				phone: req.body.phone
			}
		}
	}).then(function (total) {
		if (total>0) 
			return res.status(401).json({'error' : 'email or phone has been used'});
		else
		{
			models.sequelize.transaction(function (t) {
				return models.User.create({
					email : req.body.email,
					phone : req.body.phone,
					password : req.body.password,
					firstName : req.body.fname,
					lastName : req.body.lname,
					birthDate : (req.body.birthdate!=null)?req.body.birthdate:null,
					phoneActivationCode : smsCode,
					emailActivationCode : actCode,
				}).then(function (user) {
					var newSettings = {
						userId: user.id,
					}

					return models.Settings.create(newSettings);
				});
			}).then(function (result) {
				var mails = [{
					to: req.body.email,
					subject: 'User email activation - RoomMe',
					template: 'account/emailActivation',
					data: {
						name: req.body.fname+" "+req.body.lname,
						actCode: actCode
					}
				}]

				var sendmail =  sender.email(mails);
				
				var sendsms =  sender.sms('Your Roomme verification code is '+smsCode, req.body.phone);

				return res.status(201).json({'message' : 'success'});
			})
		}

	}).catch(function(error) {
		return res.status(500).json({'error' : error});
	});
};

exports.phoneActivation = function(req, res) 
{
	req.assert('phone', 'Invalid phone').isNumeric();
	req.assert('code', 'Invalid code').isLength(5);

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});
	
	var usrId;

	models.User.findOne({
		where: {
			phone: req.body.phone,
			phoneActivationCode: req.body.code
		},
		attributes: ['id', 'phoneActive', 'phoneActivationCode']
	}).then(function (user) {
		if (user == null) 
			return res.status(403).json({'message' : 'Wrong phone activation code'});		

		usrId = user.id;
		user.update({
			phoneActive: true,
			phoneActivationCode: null
		}).then(function (updated) {
			var log = logger.memberlog(usrId, 'activate phone')

			return res.status(200).json({'message' : 'Success'});		
		});
	});
};

exports.emailActivation = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();
	req.assert('code', 'Invalid code').isLength(5);

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});
	
	var usrId;
	
	models.User.findOne({
		where: {
			email: req.body.email,
			emailActivationCode: req.body.code
		},
		attributes: ['id', 'emailActive', 'emailActivationCode']
	}).then(function (user) {
		if (user == null) 
			return res.status(403).json({'message' : 'Wrong email activation code'});		

		usrId = user.id;
		
		user.update({
			emailActive: true,
			emailActivationCode: null
		}).then(function (updated) {
			var log = logger.memberlog(usrId, 'activate phone')

			return res.status(200).json({'message' : 'Success'});		
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});		
	});
};

exports.phoneResend = function(req, res) 
{
	req.assert('phone', 'Invalid phone').isNumeric();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});
	
	var newCode = string.random(5,1).toUpperCase();


	models.User.update({
		phoneActivationCode : newCode
	},{
		where: {
			phone: req.body.phone
		},
		returning : true
	}).then(function (user) {
		var sendsms =  sender.sms('Your Roomme verification code is '+smsCode, req.body.phone);

		return res.status(200).json({'message' : 'success'});
	});
};

exports.emailResend = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});
	
	var newCode = string.random(5,1).toUpperCase();

	var name;

	models.User.findOne({
		where: {
			email: {
				$like: req.body.email
			}
		}
	})
	.then(function (theuser) {
		if (theuser == null) 
			throw {type: 404}

		name = theuser.firstName+' '+theuser.lastName;

		return models.User.update({
			emailActivationCode : newCode
		},{
			where: {
				email: req.body.email
			}
		});

	})
	.then(function (user) {

		var mails = [{
			to: req.body.email,
			subject: 'User email activation - RoomMe',
			template: 'account/emailActivation',
			data: {
				name: name,
				actCode: newCode
			}
		}]

		var sendmail =  sender.email(mails);

		return res.status(200).json({'message' : 'Success'});		
	})
	.catch(function (err) {
		if (err.type == 404) 
			return res.status(404).json({'error' : 'not found'});		

		return res.status(500).json({'error' : err});		
	});
};

exports.storeFB = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();
	req.assert('phone', 'Invalid phone').isNumeric();
	req.assert('password', 'Invalid password').isLength(6);
	req.assert('fname', 'Invalid fname').isLength(2);
	req.assert('lname', 'Invalid lname').isLength(2);
	req.assert('fb_id', 'Invalid fbid').isInt();

	if (req.body.birthdate != null) 
		req.assert('birthdate', 'Invalid birthdate').isLength(6);
	
	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var actCode = string.random(5,1).toUpperCase();
	var smsCode = string.random(5,1).toUpperCase();

	models.User.count({
		where : {
			$or: { 
				email: req.body.email,
				phone: req.body.phone
			}
		}
	})
	.then(function (total) {
		if (total>0) 
		{
			throw {
				type: 401,
				message: 'email or phone has been used'
			}
		}
		return models.Facebook.count({
			where: {
				facebookId: req.body.fb_id
			}
		});
	})
	.then(function (total) {
		if (total>0) 
		{
			throw {
				type: 401,
				message: 'fbId exist'
			}
		}
		else
		{
			return models.sequelize.transaction(function (t) {
				return models.User.create({
					email : req.body.email,
					phone : req.body.phone,
					password : req.body.password,
					firstName : req.body.fname,
					lastName : req.body.lname,
					birthDate : (req.body.birthdate!=null)?req.body.birthdate:null,
					phoneActivationCode : smsCode,
					emailActivationCode : actCode,
				}, {transaction: t})
				.then(function (user) {
					var newSettings = {
						userId: user.id
					}

					return models.Settings.create(newSettings, {transaction: t});
				})
				.then(function (setting) {
					return models.Facebook.create({
						userId: setting.userId,
						facebookId: req.body.fb_id,
						facebookName: req.body.fb_name,
						facebookEmail: req.body.fb_email 
					}, {transaction: t});
				})
			})
		}
	})
	.then(function (success) {
		var mails = [{
			to: req.body.email,
			subject: 'User email activation - RoomMe',
			template: 'account/emailActivation',
			data: {
				name: req.body.fname+" "+req.body.lname,
				actCode: actCode
			}
		}]

		var sendmail =  sender.email(mails);

		var sendsms =  sender.sms('Please insert '+smsCode+' to activate your phone in Roomme', req.body.phone);

		return res.status(201).json({'message' : 'success'});
	})
	.catch(function(error) {
		return res.status(500).json({'error' : error});
	});
};


exports.storeGoogle = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();
	req.assert('phone', 'Invalid phone').isNumeric();
	req.assert('password', 'Invalid password').isLength(6);
	req.assert('fname', 'Invalid fname').isLength(2);
	req.assert('lname', 'Invalid lname').isLength(2);
	req.assert('google_id', 'Invalid googleid').notEmpty();

	if (req.body.birthdate != null) 
		req.assert('birthdate', 'Invalid birthdate').isLength(6);
	
	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var actCode = string.random(5,1).toUpperCase();
	var smsCode = string.random(5,1).toUpperCase();

	models.User.count({
		where : {
			$or: { 
				email: req.body.email,
				phone: req.body.phone
			}
		}
	})
	.then(function (total) {
		if (total>0) 
		{
			throw {
				type: 401,
				message: 'email or phone has been used'
			}
		}
		return models.Google.count({
			where: {
				googleId: req.body.google_id
			}
		});
	})
	.then(function (total) {
		if (total>0) 
		{
			throw {
				type: 401,
				message: 'googleId exist'
			}
		}
		else
		{
			return models.sequelize.transaction(function (t) {
				return models.User.create({
					email : req.body.email,
					phone : req.body.phone,
					password : req.body.password,
					firstName : req.body.fname,
					lastName : req.body.lname,
					birthDate : (req.body.birthdate!=null)?req.body.birthdate:null,
					phoneActivationCode : smsCode,
					emailActivationCode : actCode
				}, {transaction: t})
				.then(function (user) {
					var newSettings = {
						userId: user.id
					}

					return models.Settings.create(newSettings, {transaction: t});
				})
				.then(function (setting) {
					return models.Google.create({
						userId: setting.userId,
						googleId: req.body.google_id,
						fname: req.body.google_given_Name,
						lname: req.body.google_family_Name,
						email: req.body.google_email 
					}, {transaction: t});
				})
			})
		}
	})
	.then(function (success) {
		var mails = [{
			to: req.body.email,
			subject: 'User email activation - RoomMe',
			template: 'account/emailActivation',
			data: {
				name: req.body.fname+" "+req.body.lname,
				actCode: actCode
			}
		}]
		var sendmail =  sender.email(mails);

		var sendsms =  sender.sms('Please insert '+smsCode+' to activate your phone in Roomme', req.body.phone);

		return res.status(201).json({'message' : 'success'});
	})
	.catch(function(error) {
		return res.status(500).json({'error' : error});
	});
};