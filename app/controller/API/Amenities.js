'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	models.Amenities.findAll().then(function (amenities){
		return res.status(200).json({'amenities' : amenities});
	}).catch(function (error){
		return res.status(500).json({'error' : error});
	});
};