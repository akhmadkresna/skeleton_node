'use strict';
var models  = require(__dirname+'/../../model');

exports.priceRange = function(req, res) 
{
	models.Config.findAll({
		where: {
			name: {
				$in: ["priceRangeMin", "priceRangeMax"]
			}
		},
		order: "name ASC"
	}).then(function (configs) {
		return res.status(200).json({
			min: configs[1].value,
			max: configs[0].value
		});
	}).catch(function (err) {
		return res.status(500).json({error: err});
	});
}