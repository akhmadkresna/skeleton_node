'use strict';
var models  = require(__dirname+'/../../model');

var string  = require(__dirname+'/../../helper/string');
var sender  = require(__dirname+'/../../helper/sender');

var logger  = require(__dirname+'/../../helper/logger');

var push  = require(__dirname+'/../../helper/push');

exports.index = function(req, res) 
{
	var searchq = new Object();
	var order = '';

	req.assert('type', 'Invalid type').isIn(['prev', 'next']);

	if (req.query.type === 'prev') 
	{
		searchq['$or'] = {
			canceled: 1,
			finished: 1, 
			declined: 1 
		};
		order = 'createdAt ASC';
	}
	else if (req.query.type === 'next') 
	{
		searchq['$and'] = {
			finished: 0,
			canceled: 0,
			declined: 0,
		};
		order = 'createdAt DESC';
	}

	req.assert('total', 'Invalid total').isInt({min:1});
	req.assert('cursor', 'Invalid cursor').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	searchq['ownerId'] = req.decoded.id;

	models.Reservation.findAll({
		where: searchq,
		order: order,
		limit: parseInt(req.query.total),
		offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		// attributes: ['code', 'startDate', 'endDate', 'approved', 'paid', 'canceled', 'finished'],
		include: [{
			model: models.User,
			as: 'Traveller',
			attributes: [
			'email', 
			'firstName', 
			'lastName', 
			'picture'
			]
		},{
			model: models.Listing,
			attributes: [
			'id', 
			'typeId', 
			'locationTypeId', 
			'name', 
			'address', 
			'country', 
			'province', 
			'city', 
			'district', 
			'coordinate', 
			'dailyPrice', 
			'totalRating', 
			'totalReview', 
			'image1',
			'guest'
			],
			include: [{
				model: models.Type,
				attributes: ["name"]
			}]
		}]
	}).then(function (trips) {
		var returns = [];
		for (var i = trips.length - 1; i >= 0; i--) 
		{
			trips[i].dataValues.status = string.tripStatus(trips[i]);

			if (trips[i].dataValues.status === 'Accepted' && trips[i].dataValues.pending>0) 
				trips[i].dataValues.status = 'Payment pending';

			if (trips[i].dataValues.status === 'Paid') 
				trips[i].dataValues.status = 'Payment Received';

			returns.push(trips[i].dataValues);
		}
		return res.status(200).json({'trips' : returns, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
};

exports.show = function(req, res) 
{
	req.assert('code', 'Invalid code').isLength({min:1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			code: req.params.code,
			ownerId: req.decoded.id
		},
		include: [{
			model: models.User,
			as: 'Traveller',
			attributes: ['email', 'firstName', 'lastName', 'picture', 'id']
		}, {
			model: models.Listing
		}]
	}).then(function (reservation) {
		if (reservation===null) 
			return res.status(404).json({error: 'not found'});

		reservation['status'] = string.tripStatus(reservation);

		return res.status(200).json({reservation: reservation});
	}).catch(function (err) {
		return res.status(500).json({error: 'Internal server error'});
	});
};


exports.store = function(req, res) 
{
	


};



exports.acceptance = function(req, res) 
{
	req.assert('code', 'Invalid code').isLength({min:1});
	req.assert('acceptance', 'Invalid acceptance').isIn(['accept', 'decline']);

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var trip;

	models.Reservation.findOne({
		where: {
			code: req.params.code,
			ownerId: req.decoded.id
		},
		include: [{
			model: models.User,
			as: 'Traveller'
		}, {
			model: models.Listing,
		}]
	}).then(function (reservation) {
		if (reservation===null) 
			return res.status(404).json({error: 'not found'});

		var updateObj;

		trip = reservation;

		if (req.params.acceptance === 'accept') 
			updateObj = {approved: 1};
		else if (req.params.acceptance === 'decline') 
			updateObj = {declined: 1};
		
		reservation.update(updateObj).then(function (updated) 
		{
			var log = logger.memberlog(req.decoded.id, 'book '+req.params.code+((req.params.acceptance === 'accept')?'acceptance':'decline'))

			var tripstat = '';

			if (req.params.acceptance === 'accept') 
			{
				var mails = [{
					to: trip.Traveller.email,
					subject: 'Booking request '+trip.code+' accepted - Roomme',
					template: 'trip/acceptRequest',
					data: {
						trip: trip
					}
				}]
				tripstat = 'accepted';
			}
			else if (req.params.acceptance === 'decline') 
			{
				var mails = [{
					to: trip.Traveller.email,
					subject: 'Booking request '+trip.code+' declined - Roomme',
					template: 'trip/declineRequest',
					data: {
						trip: trip
					}
				}]

				tripstat = 'declined';
			}
			var sendmail =  sender.email(mails);

			if (trip.Traveller.device == 'ios') 
				var test =  push.pushIOS(trip.Traveller.pushToken, 'Book request response received', {});
			else if (trip.Traveller.device == 'android') 
				var test =  push.pushGCM(trip.Traveller.pushToken, 'Book request response received', {});
			
			return res.status(200).json({'message' : req.params.code+' '+tripstat});
		});
		// return res.status(200).json({reservation: reservation});
	}).catch(function (err) {
		return res.status(500).json({error: 'Internal server error'});
	});
};
