'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');
var string  = require(__dirname+'/../../helper/string');

exports.index = function(req, res) 
{
	req.assert('total', 'Invalid total').isInt({min:1});
	req.assert('cursor', 'Invalid cursor').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findAll({
		where: {
			userId: req.decoded.id
		},
		order: 'createdAt DESC',
		limit: parseInt(req.query.total),
		offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		attributes: [
		"id", 
		"name", 
		"province", 
		"address", 
		"city", 
		"district", 
		"coordinate", 
		"dailyPrice", 
		"totalRating", 
		"totalReview", 
		"image1", 
		"unlisted",
		"guest"
		],
		include: [{
			model: models.Type,
			attributes: ["name"]
		}]
	}).then(function (listings) {
		return res.status(200).json({listings: listings, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	}).catch(function (err) {
		return res.status(500).json({error: 'Internal server error'});
	});

};

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		},
		include : [{
			model: models.Amenities
		}]
		// attributes: ["*"]
	}).then(function (listing) {
		if (listing===null) 
			return res.status(404).json({error: 'not found'});

		return res.status(200).json({listing: listing});
	}).catch(function (err) {
		return res.status(500).json({error: 'Internal server error'});
	});
};


exports.store = function(req, res) 
{
	req.assert('type', 'Invalid type').isInt({min:0});
	req.assert('typelocation', 'Invalid typelocation').isInt({min:0});
	
	req.assert('lat', 'Invalid lat').isFloat();
	req.assert('long', 'Invalid long').isFloat();

	req.assert('name', 'Invalid name').isLength(5);
	req.assert('description', 'Invalid description').isLength(20);
	req.assert('price', 'Invalid price').isInt({min: 0});

	req.assert('guest', 'Invalid guest').isInt({min: 1});
	req.assert('bedroom', 'Invalid bedroom').isInt({min: 1});
	req.assert('bed', 'Invalid bed').isInt({min: 1});
	req.assert('bathroom', 'Invalid bathroom').isInt({min: 1});

	req.assert('address', 'Invalid address').isLength({min: 5});
	req.assert('city', 'Invalid city').isLength({min: 2});
	// req.assert('province', 'Invalid province').isLength({min: 2});
	req.assert('postal', 'Invalid postal code').isNumeric();
	req.assert('country', 'Invalid country').isLength({min: 2});

	req.assert('weeklydisc', 'Invalid weekly discount').isInt({min: 0});
	req.assert('monthlydisc', 'Invalid monthly discount').isInt({min: 0});

	if (typeof req.body['amenities'] != 'undefined' && req.body['amenities'].length > 0) 
	{
		req.assert('amenities', 'Invalid amenities').arrayInt(1);
	}

	req.assert('secured', 'Invalid secured').isInt({min: 0, max: 1});
	req.assert('sameday', 'Invalid sameday').isInt({min: 0, max: 1});
	req.assert('preptime', 'Invalid preptime').isInt({min: 0, max: 2});

	req.assert('minday', 'Invalid minday').isInt({min:0, max: (parseInt(req.body.maxday) == 0)?999:parseInt(req.body.maxday)});
	req.assert('maxday', 'Invalid maxday').isInt({min: (parseInt(req.body.maxday) == 0)?0:parseInt(req.body.minday)});

	req.assert('identifier', 'Invalid identifier').isLength({min: 10});

	req.assert('image1', 'Invalid image1').isLength({min: 10});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	req.listingId;
	
	models.sequelize.transaction(function (t) {

		return models.Listing.create({
			userId: req.decoded.id,
			typeId: req.body.type,
			locationTypeId: req.body.typelocation,
			name: req.body.name,
			description: req.body.description,
			address: req.body.address,
			country: req.body.country,
			province: (req.body.province!=null)?req.body.province:null,
			city: req.body.city,
			district: req.body.district,
			coordinate: { type: 'Point', coordinates: [req.body.long, req.body.lat]},
			postal: req.body.postal,
			guest: parseInt(req.body.guest),
			bedroom: parseInt(req.body.bedroom),
			bed: parseInt(req.body.bed),
			bathroom: parseInt(req.body.bathroom),
			dailyPrice: parseInt(req.body.price),
			weeklyDisc: parseInt(req.body.weeklydisc),
			monthlyDisc: parseInt(req.body.monthlydisc),
			secured: parseInt(req.body.secured),
			access: (req.body.access!= null)?req.body.access:null,
			neighborhood: (req.body.neighborhood!= null)?req.body.neighborhood:null,
			rule: (req.body.rule!= null)?req.body.rule:null,
			sameDay: parseInt(req.body.sameday),
			prepDays: parseInt(req.body.preptime),
			minLimit: parseInt(req.body.minday),
			maxLimit: parseInt(req.body.maxday),
			image1: req.body.image1,
			image2: req.body.image2,
			image3: req.body.image3,
			image4: req.body.image4,
			image5: req.body.image5,
			identifier: req.body.identifier
		}, {transaction: t})
		.then(function (newlisting) {
			req.listingId = newlisting.id;

			var listingamenities = [];

			if ( typeof req.body['amenities'] != 'undefined' && req.body['amenities'].length > 0) 
			{
				for (var i = req.body['amenities'].length - 1; i >= 0; i--) 
				{
					listingamenities.push({
						listingId : req.listingId,
						amenityId : req.body['amenities'][i]
					});
				}	
			}


			return models.ListingAmenities.bulkCreate(listingamenities, {transaction: t});
		});

	}).then(function (result) {
		return res.status(201).json({
			'message' : 'listing created', 
			'listingId': req.listingId
		});
	})
};

exports.update = function(req, res) 
{
	req.assert('type', 'Invalid type').isInt({min:0});
	req.assert('typelocation', 'Invalid typelocation').isInt({min:0});
	
	req.assert('lat', 'Invalid lat').isFloat();
	req.assert('long', 'Invalid long').isFloat();

	req.assert('name', 'Invalid name').isLength(5);
	req.assert('description', 'Invalid description').isLength(20);
	req.assert('price', 'Invalid price').isInt({min: 0});

	req.assert('address', 'Invalid address').isLength({min: 5});
	req.assert('city', 'Invalid city').isLength({min: 2});
	// req.assert('province', 'Invalid province').isLength({min: 2});
	req.assert('postal', 'Invalid postal').isNumeric();
	req.assert('country', 'Invalid country').isLength({min: 2});

	req.assert('guest', 'Invalid guest').isInt({min: 1});
	req.assert('bedroom', 'Invalid bedroom').isInt({min: 1});
	req.assert('bed', 'Invalid bed').isInt({min: 1});
	req.assert('bathroom', 'Invalid bathroom').isInt({min: 1});

	req.assert('weeklydisc', 'Invalid weekly discount').isInt({min: 0});
	req.assert('monthlydisc', 'Invalid monthly discount').isInt({min: 0});

	if (typeof req.body['amenities'] != 'undefined' && req.body['amenities'].length > 0) 
	{
		req.assert('amenities', 'Invalid amenities').arrayInt(1);
	}

	req.assert('secured', 'Invalid secured').isInt({min: 0, max: 1});
	req.assert('sameday', 'Invalid sameday').isInt({min: 0, max: 1});
	req.assert('preptime', 'Invalid preptime').isInt({min: 0, max: 2});
	
	req.assert('minday', 'Invalid minday').isInt({min:0, max: (parseInt(req.body.maxday) == 0)?999:parseInt(req.body.maxday)});
	req.assert('maxday', 'Invalid maxday').isInt({min: (parseInt(req.body.maxday) == 0)?0:parseInt(req.body.minday)});

	req.assert('identifier', 'Invalid identifier').isLength({min: 10});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	req.listingId;
	
	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing == null) 
			return res.status(404).json({error: 'not found'});

		req.unlisted = listing.unlisted;

		if (req.body.image1 == null) 
		{
			for (var i = 2; i <= 5; i++) 
			{
				if (req.body['image'+i] != null) 
				{
					req.body.image1 = req.body['image'+i];
					break;
				}
			}
		}
		
		if (req.body.image1 == null && req.body.image2 == null && req.body.image3 == null && req.body.image4 == null && req.body.image5 == null) 
			req.unlisted = true;


		models.sequelize.transaction(function (t) {

			return listing.update({
				typeId: req.body.type,
				locationTypeId: req.body.typelocation,
				name: req.body.name,
				description: req.body.description,
				address: req.body.address,
				country: req.body.country,
				// province: req.body.province,
				city: req.body.city,
				district: req.body.district,
				coordinate: { type: 'Point', coordinates: [req.body.long, req.body.lat]},
				postal: req.body.postal,
				guest: parseInt(req.body.guest),
				bedroom: parseInt(req.body.bedroom),
				bed: parseInt(req.body.bed),
				bathroom: parseInt(req.body.bathroom),
				dailyPrice: parseInt(req.body.price),
				weeklyDisc: parseInt(req.body.weeklydisc),
				monthlyDisc: parseInt(req.body.monthlydisc),
				secured: parseInt(req.body.secured),
				access: (req.body.access!= null)?req.body.access:null,
				neighborhood: (req.body.neighborhood!= null)?req.body.neighborhood:null,
				rule: (req.body.rule!= null)?req.body.rule:null,
				sameDay: parseInt(req.body.sameday),
				prepDays: parseInt(req.body.preptime),
				minLimit: parseInt(req.body.minday),
				maxLimit: parseInt(req.body.maxday),
				image1: req.body.image1,
				image2: req.body.image2,
				image3: req.body.image3,
				image4: req.body.image4,
				image5: req.body.image5,
				unlisted: req.unlisted,
			}, {transaction: t}).then(function (thelisting) {

				return models.ListingAmenities.destroy({
					where: {
						listingId : req.params.id
					},
					transaction: t
				})
			})
			.then(function (deleted) {
				var theamenities = [];

				var listingamenities = [];
				if ( typeof req.body['amenities'] != 'undefined' && req.body['amenities'].length > 0) 
				{

					for (var i = req.body['amenities'].length - 1; i >= 0; i--) 
					{

						listingamenities.push({
							listingId : req.params.id,
							amenityId : req.body['amenities'][i]
						});
					}	
				}

				return models.ListingAmenities.bulkCreate(listingamenities, {transaction: t});
			});
		}).then(function (result) {

			return res.status(201).json({
				'message' : 'listing updated',
				'listingId': req.listingId
			});
		}).catch(function (err) {
			return res.status(500).json({error: err});
		});
		
	});

};

exports.updateImage = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:0});

	for (var i = req.body.url.length - 1; i >= 0; i--) 
	{
		req.assert('url['+i+']', 'Invalid url').isLength({min: 5});
	}

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var newurl = [];

	for (var i = 0; i < req.body.url.length; i++) 
	{
		newurl.push(req.body.url[i])
	}	

	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing === null) 
			return res.status(404).json({'error' : 'not found'});

		listing.update({
			imageLocation: JSON.stringify(newurl)
		}).then(function (updated) {
			return res.status(200).json({'message' : 'listing image updated'});
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.specialPrice = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:0});
	req.assert('start', 'Invalid start').isDate();
	req.assert('end', 'Invalid end').isDate();
	req.assert('price', 'Invalid price').isInt({min: 0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing === null) 
			return res.status(404).json({'error' : 'listing not found'});

		var dates = [];

		var start = req.body.start.split('-');
		var end = req.body.end.split('-');

		var from = new Date(start[0], start[1], start[2]);

		var till = new Date(end[0], end[1], end[2]);

		for (var i =  from; i <= till; i.setDate(i.getDate() + 1)) 
		{
			var current = string.mysqlDate(String(i.getFullYear()), String(i.getMonth()), String(i.getDate()))

			dates.push({
				listingId: req.params.id,
				date: current,
				price: req.body.price,
			});
		}

		models.ExtraPrice.bulkCreate(dates).then(function () {
			return res.status(201).json({'message' : 'extra price set'});
		});

	})
}

exports.specialPriceDelete = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:0});
	req.assert('start', 'Invalid start').isDate();
	req.assert('end', 'Invalid end').isDate();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing === null) 
			return res.status(404).json({'error' : 'listing not found'});

		models.ExtraPrice.destroy({
			where: 
			{
				listingId: req.params.id,
				date: {
					$between : [req.body.start, req.body.end]
				}
			}
		}).then(function (deleted) {
			return res.status(200).json({message: 'deleted'});
		});

	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});

}

exports.blockDate = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:0});
	req.assert('start', 'Invalid start').isDate();
	req.assert('end', 'Invalid end').isDate();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing === null) 
			return res.status(404).json({'error' : 'listing not found'});

		var dates = [];

		var start = req.body.start.split('-');
		var end = req.body.end.split('-');

		var from = new Date(start[0], start[1], start[2]);

		var till = new Date(end[0], end[1], end[2]);

		for (var i =  from; i <= till; i.setDate(i.getDate() + 1)) 
		{
			var current = string.mysqlDate(String(i.getFullYear()), String(i.getMonth()), String(i.getDate()))

			dates.push({
				listingId: req.params.id,
				date: current,
			});
		}

		models.Block.bulkCreate(dates).then(function () {
			return res.status(201).json({'message' : 'dates are blocked'});
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.blockDateDelete = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:0});
	req.assert('start', 'Invalid start').isDate();
	req.assert('end', 'Invalid end').isDate();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			id: req.params.id,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing === null) 
			return res.status(404).json({'error' : 'listing not found'});

		models.Block.destroy({
			where: 
			{
				listingId: req.params.id,
				date: {
					$between : [req.body.start, req.body.end]
				}
			}
		}).then(function (deleted) {
			return res.status(200).json({message: 'unblocked'});
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.unlist = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			userId: req.decoded.id,
			id: req.params.id
		}
	}).then(function (listing) {
		if (listing == null) 
			return res.status(400).json({'error' : 'not found'});

		listing.update({
			unlisted : true
		}).then(function (updated) {
			return res.status(200).json({'message' : 'unlisted'});	
		});
	});
}

exports.relist = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			userId: req.decoded.id,
			id: req.params.id
		}
	}).then(function (listing) {
		if (listing == null) 
			return res.status(404).json({'error' : 'not found'});

		if (listing.image1 == null) 
			return res.status(403).json({'error' : 'unavailable for relist,listing have no image'});

		listing.update({
			unlisted : false
		}).then(function (updated) {
			return res.status(200).json({'message' : 'listed'});	
		});
	});
}

exports.delete = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			userId: req.decoded.id,
			id: req.params.id
		}
	})
	.then(function (listing) {
		if (listing == null) {
			throw {
				type: 404,
				message: "not found"
			}
		}

		return listing.destroy();
	})
	.then(function (deleted) {
		return res.status(200).json({'message' : 'deleted'});	
	})
	.catch(function (err) {
		return res.status(500).json({'message' : 'internal server error'});	
	})
}