'use strict';
var models  = require(__dirname+'/../../model');

exports.email = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.count({
		where: {
			email: req.body.email
		}
	}).then(function (user){
		if (user==0) 
			return res.status(200).json({'message' : 'email available'});
		else
			return res.status(400).json({'error' : 'email already used'});
	}).catch(function (error){
		return res.status(500).json({'error' : error});
	});
};

exports.phone = function(req, res) 
{
	req.assert('phone', 'Invalid phone').isNumeric();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.count({
		where: {
			phone: req.body.phone
		}
	}).then(function (user){
		if (user==0) 
			return res.status(200).json({'message' : 'phone available'});
		else
			return res.status(400).json({'error' : 'phone already used'});
	}).catch(function (error){
		return res.status(500).json({'error' : error});
	});
};