'use strict';
var models  = require(__dirname+'/../../model');

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Verification.findAll({
		where: {
			userId: req.params.id,
			status: 2
		},
		attributes: ['type', 'createdAt']
	}).then(function (ids) {
		var verif = [];
		if (ids.length > 0) 
		{
			for (var i = ids.length - 1; i >= 0; i--) 
			{
				verif.push(ids[i].type);
			}
		}

		return res.status(200).json({'verification': verif});
	}).catch(function (err) {
		return res.status(500).json({'err': err});
	});

}

exports.store = function(req, res) 
{
	req.assert('type', 'Invalid type').isInt({min:1, max:3});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Verification.create({
		userId: req.decoded.id,
		type: req.params.type
	}).then(function (verify, err) {
		return res.status(201).json({message: 'verification submitted'});
	}).catch(function (err) {
		return res.status(500).json({message: err});
	})
}
