'use strict';
var models  = require(__dirname+'/../../model');

exports.listing = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min:1});
	req.assert('total', 'Invalid total').isInt({min:1, max:45});
	req.assert('cursor', 'Invalid cursor').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0)
 		return res.status(400).json({'error' : errors});

	models.Reservation.findAll({
		where: {
			listingId: req.params.id,
			reviewDate: {
				$ne: null
			}
		},
		order: 'reviewDate DESC',
		limit: parseInt(req.query.total),
		offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		attributes: ['userId', 'code', 'reviewDate', 'star', 'review'],
		include: [{
			model: models.User,
			attributes: ['email', 'firstName', 'lastName', 'picture'],
			as: 'Traveller'
		}, {
			model: models.Listing,
			attributes: ['id', 'name']
		}]
	}).then(function(review) 
	{
		return res.status(200).json({'review' : review, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	});


};


exports.user = function(req, res) 
{
	req.assert('id', 'Invalid user').isInt({min:1});
	req.assert('total', 'Invalid total').isInt({min:1, max:50});
	req.assert('cursor', 'Invalid cursor').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findAll({
		where: {
			ownerId: req.params.id,
			reviewDate: {
				$ne: null
			}
		},
		order: 'reviewDate DESC',
		limit: parseInt(req.query.total),
		offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		attributes: ['userId', 'code', 'reviewDate', 'star', 'review'],
		include: [{
			model: models.User,
			attributes: ['email', 'firstName', 'lastName', 'picture'],
			as: 'Traveller'
			
		}, {
			model: models.Listing,
			attributes: ['id', 'name']
		}]
	}).then(function(review) {
		return res.status(200).json({'review' : review, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	})


};

exports.store = function(req, res) 
{
	


};
