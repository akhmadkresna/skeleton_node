'use strict';
var models  = require(__dirname+'/../../model');

var cloudfront = global.APP_CONFIGS.aws.cloudfront;

var fs = require('fs');

var S3FS = require('s3fs');

var logger  = require(__dirname+'/../../helper/logger');

var awsConfig = global.APP_CONFIGS.aws;

var s3fsImpl = new S3FS(awsConfig.bucket, {
	accessKeyId : awsConfig.accessKeyId,
	secretAccessKey : awsConfig.secretAccessKey
});

var sender  = require(__dirname+'/../../helper/sender');

exports.listing = function(req, res) 
{
	var part1 = new Buffer(String(req.decoded.email)).toString('base64');

	var part2 = (req.body.identifier == null)?req.params.identifier:req.body.identifier;

	var part3 = req.identifier;

	return res.status(201).json({message: 'upload success', url: cloudfront+part1+'/'+part2+'/'+part3+'.jpg'});
};

exports.listingUpdate = function(req, res, next )
{
	req.assert('identifier', 'Invalid identifier').isLength({min:5});
	req.assert('number', 'Invalid number').isLength({min:1, max:5});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			identifier: req.params.identifier,
			userId: req.decoded.id
		}
	}).then(function (listing) {
		if (listing === null) 
			return res.status(404).json({error: 'not found'});

		deletePhoto(listing['image'+(req.body.number == null)?req.params.number:req.body.number]);

		if (typeof req.identifier == 'undefined') 
			req.identifier = String(new Date().getTime());

		var part1 = new Buffer(String(req.decoded.email)).toString('base64');

		var part2 = (req.body.identifier == null)?req.params.identifier:req.body.identifier;

		var part3 = req.identifier;

		var update = {};
		update['image'+req.params.number] = cloudfront+part1+'/'+part2+'/'+part3+'.jpg';

		req.imageLoc= update['image'+req.params.number];

		listing.update(update).then(function (listing) {
			return next();
		})

		function deletePhoto(url) {
			s3fsImpl.unlink(url, function (err) 
			{
				return true;
			});
		}
	});

};

exports.listingDelete = function(req, res) 
{
	req.assert('identifier', 'Invalid identifier').isLength({min:10});
	req.assert('number', 'Invalid number').isInt({min:1, max:5});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findOne({
		where: {
			identifier: req.params.identifier
		}
	}).then(function (listing) {

		var part1 = new Buffer(String(req.decoded.email)).toString('base64');

		var part2 = (req.body.identifier == null)?req.params.identifier:req.body.identifier;

		var part3 = (req.body.number == null)?req.params.number:req.body.number;

		if (listing != null) 
		{
			if (listing.userId != req.decoded.id) 
				return res.status(404).json({error: 'not found'});
			
			var update = {};

			update['image'+req.body.number] = null;

			listing.update(update).then(function (listing) {
				deletePhoto(part1+'/'+part2+'/'+part3+'.jpg');
				return res.status(200).json({message: 'deleted'});
			})	
		}
		else
		{
			deletePhoto(part1+'/'+part2+'/'+part3+'.jpg');
			return res.status(200).json({message: 'deleted'});
		}

		function deletePhoto(url) {
			s3fsImpl.unlink(url, function (err) 
			{
				return true;
			});
		}
	});
};

exports.profile = function(req, res) 
{
	models.User.findById(req.decoded.id).then(function (user) {
		deletePhoto(user.picture);

		var part1 = new Buffer(String(req.decoded.email)).toString('base64');

		var part2 = '/'+req.identifier+'.jpg';

		models.User.update({ picture:cloudfront+part1+part2}, {
			where: {
				id: req.decoded.id
			}
		}).then(function (updated) {
			return res.status(200).json({message: 'updated', newImage: cloudfront+part1+part2});
		});

		function deletePhoto(url) {
			s3fsImpl.unlink(url, function (err) 
			{
				return true;
			});
		}
	});
};

exports.verification = function(req, res) 
{
	req.assert('type', 'Invalid type').isInt({min:1, max:3});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var part1 = new Buffer(String(req.decoded.email)).toString('base64');

	var part2 = '/verification/'+req.identifier+'.jpg';

	models.Verification.findOne({
		where: {
			userId: req.decoded.id,
			type: req.params.type
		}
	}).then(function (verif) {
		if (verif  == null) 
		{
			return models.Verification.create({
				userId: req.decoded.id,
				type: req.params.type,
				location: cloudfront+part1+part2
			});
		}
		else 
		{
			if (verif.status == 0 || verif.status == 1)
			{
				return verif.update({
					status: 1,
					location: cloudfront+part1+part2
				});
			}
			return true;
		}
	})
	.then(function (success) {
		var log = logger.memberlog(req.decoded.id, 'user verification')

		var mails = [{
			to: 'info.roomme@gmail.com',
			subject: 'Verification submission',
			template: 'admin/verification',
			data: {
				user: {
					firstName: req.decoded.firstName,
					lastName: req.decoded.lastName
				}
			}
		}]
		var sendmail =  sender.email(mails);

		return res.status(200).json({url: cloudfront+part1+part2});
	});

};

exports.payment = function(req, res, next) 
{
	req.assert('bookcode', 'Invalid bookcode').isAlphanumeric();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			userId: req.decoded.id,
			code: req.params.bookcode
		}
	}).then(function (reservation) {
		if (reservation == null) 
			return res.status(404).json({error: 'not found'});

		var part1 = new Buffer(String(req.decoded.email)).toString('base64');

		var part2 = 'trips/'+req.params.bookcode;

		var part3 = Date.now();

		req.filename = part1+'/'+part2+'/'+part3+'.jpg';

		models.Payment.create({
			reservationCode: req.params.bookcode,
			location: cloudfront+req.filename,
			doku: 0
		})
		.then(function (payment) {
			var log = logger.memberlog(req.decoded.id, 'book '+req.params.bookcode+' payment')

			return models.Payment.findById(payment.id, {
				include: [{
					model: models.Reservation,
					include: [{
						model: models.User,
						as: 'Traveller'
					}, {
						model: models.Listing
					}]
				}]
			})
		})
		.then(function (payment) {
			var mails = [{
				to: 'info.roomme@gmail.com',
				subject: 'Payment for '+payment.reservationCode,
				template: 'admin/payment',
				data: {
					payment: payment
				}
			}]
			var sendmail =  sender.email(mails);
			return next();
		})
	});
}