'use strict';
var models  = require(__dirname+'/../../model');


exports.profile = function(req, res) 
{
	models.User.findById(req.decoded.id, {
		attributes: ['id', 'email', 'firstName', 'lastName', 'about', 'phone', 'birthDate', 'address', 'createdAt', 'picture'],
		include: [{
			model : models.Verification,
			order : 'createdAt desc'
			// limit : 1
		}]
	}).then(function (user) {
		return res.status(200).json({'profile': user});
	}).catch(function (err) {
		return res.status(500).json({'err': err});
	});
}

exports.profileUpdate = function(req, res) 
{
	if (req.body.fname == null && req.body.lname != null && req.body.birth != null && req.body.address != null && req.body.about != null) 
		return res.status(200).json({'message' : 'no update'});

	if (req.body.fname == undefined && req.body.lname != undefined && req.body.birth != undefined && req.body.address != undefined && req.body.about != undefined) 
		return res.status(200).json({'message' : 'no update'});

	var updateObj = [];

	if (req.body.fname != null || req.body.fname != undefined) 
	{
		updateObj['firstName']= req.body.fname;
		req.assert('fname', 'Invalid fname').isLength({min: 2});
	}

	if (req.body.lname != null || req.body.lname != undefined) 
	{
		updateObj['lastName']= req.body.lname;
		req.assert('lname', 'Invalid lname').isLength({min: 2});
	}

	if (req.body.birth != null || req.body.birth != undefined) 
	{
		updateObj['birthDate']= req.body.birth;
		req.assert('birth', 'Invalid birth').isDate();
	}

	if (req.body.address != null || req.body.address != undefined) 
	{
		updateObj['address']= req.body.address;
		req.assert('address', 'Invalid address').isLength({min: 10});
	}

	if (req.body.about != null || req.body.about != undefined) 
	{
		updateObj['about']= req.body.about;
		req.assert('about', 'Invalid about').isLength({min: 5});
	}

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findById(req.decoded.id, {
		attributes: ['id', 'email', 'firstName', 'lastName', 'about', 'phone', 'birthDate', 'address']
	}).then(function (user) 
	{
		user.update(updateObj).then(function (updated) 
		{
			return res.status(200).json({'message' : 'updated', 'user': updated});
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.pictureUpdate = function(req, res) 
{
	req.assert('imageurl', 'Invalid imageurl').isURL();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.update({
		picture: req.body.imageurl
	}, {
		where: {
			id: req.decoded.id
		}
	})
	.then(function (updated) {
		return res.status(200).json({'message': 'updated'});
	})
	.catch(function (err) {
		return res.status(500).json({err: err})
	})
}

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	// var data;

	models.User.findById(req.params.id, {
		attributes: 
		 Object.keys(models.User.attributes).concat([
			'id', 
			'email', 
			'firstName', 
			'lastName', 
			'about', 
			'phone', 
			'birthDate', 
			'address', 
			'createdAt', 
			'emailActive', 
			'phoneActive', 
			'picture',
			[models.sequelize.literal('(SELECT COUNT(*) FROM reservation WHERE ownerId='+req.params.id+' AND reviewDate IS NOT NULL)'), 'totalReview'],
			[models.sequelize.literal('(SELECT COUNT(*) FROM listing WHERE userId='+req.params.id+')'), 'totalListing']
			]),
		include: [{
			model: models.Verification
		}]
	}).then(function (user) {
		if (user == null) 
			return res.status(404).json({err: 'not found'});

		return res.status(200).json({'user': user});
	})
	// .then(function (listing) {
	// 	console.log(listing)
	// 	data['totalListing'] = listing;

	// })
	.catch(function (err) {
		return res.status(500).json({'err': 'internal server error'});
	});

}


exports.showListing = function(req, res) 
{
	req.assert('id', 'Invalid user').isInt({min:1});
	req.assert('total', 'Invalid total').isInt({min:1, max:45});
	req.assert('cursor', 'Invalid cursor').isInt({min:0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findAll({
		where: {
			userId: req.params.id
		},
		limit: parseInt(req.query.total),
		offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		attributes: [
		"id", 
		"name", 
		"province", 
		"address", 
		"city", 
		"district", 
		"coordinate", 
		"dailyPrice", 
		"totalRating", 
		"totalReview", 
		"image1",
		"image2",
		"image3",
		"image4",
		"image5",
		"guest"
		],
		include: [{
			model: models.Type,
			attributes: ["name"]
		}]
	}).then(function(listings) 
	{
		return res.status(200).json({'listings' : listings, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	});
}

exports.bank = function(req, res) 
{
	models.User.findById(req.decoded.id, {
		attributes: ['bankAccNum', 'bankAccName', 'bankName'],
	}).then(function (user) {
		return res.status(200).json({'bank': user});
	}).catch(function (err) {
		return res.status(500).json({'err': err});
	});
}

exports.bankUpdate = function(req, res) 
{
	req.assert('bankname', 'Invalid bankname').isLength({min:2});
	req.assert('accname', 'Invalid accname').isLength({min:2});
	req.assert('accnum', 'Invalid accnum').isLength({min:2});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findById(req.decoded.id, {
		attributes: ['bankAccNum', 'bankAccName', 'bankName'],
	}).then(function (user) {
		if (user == null) 
			return res.status(404).json({'message': 'not found'});

		return models.User.update({
			bankName: req.body.bankname,
			bankAccName: req.body.accname,
			bankAccNum: req.body.accnum
		}, {
			where: {
				id: req.decoded.id
			}
		});
	})
	.then(function (updated) {
		return res.status(200).json({'message': 'updated'});
	})
	.catch(function (err) {
		return res.status(500).json({'err': err});
	});
}