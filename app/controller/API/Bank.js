'use strict';
var models  = require(__dirname+'/../../model');

exports.index = function(req, res) 
{
	models.Bank.findAll().then(function (banks) {
		return res.status(200).json({
			banks: banks
		});
	});
}