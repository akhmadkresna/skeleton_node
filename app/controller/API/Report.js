var models  = require(__dirname+'/../../model');

exports.guest = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( YEAR(startDate) = YEAR(CURDATE())";

	if (req.query.interval === 'monthly') 
		intervalq += "AND MONTH(startDate) = MONTH(CURDATE())";

	if (req.query.interval === 'weekly') 
		intervalq += "AND WEEK(startDate) = WEEK(CURDATE())";

	intervalq += ")";

	var q = "SELECT COUNT(finished) AS total FROM reservation WHERE ownerId=? AND listingId=? AND "+intervalq;

	models.sequelize.query(q, {replacements: [req.decoded.id, req.params.id], raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total.total == null)?0:total.total});
	});
};

exports.money = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( YEAR(startDate) = YEAR(CURDATE())";

	if (req.query.interval === 'monthly') 
		intervalq += "AND MONTH(startDate) = MONTH(CURDATE())";

	if (req.query.interval === 'weekly') 
		intervalq += "AND WEEK(startDate) = WEEK(CURDATE())";

	intervalq += ")";

	var q = "SELECT SUM(cost) AS total FROM reservation WHERE ownerId=? AND listingId=? and finished = 1 AND "+intervalq;

	models.sequelize.query(q, {replacements: [req.decoded.id, req.params.id], raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total.total == null)?0:total.total});
	});
};

exports.books = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( YEAR(startDate) = YEAR(CURDATE())";

	if (req.query.interval === 'monthly' || req.query.interval === 'weekly') 
		intervalq += " AND MONTH(startDate) = MONTH(CURDATE())";

	if (req.query.interval === 'weekly') 
		intervalq += " AND WEEK(startDate) = WEEK(CURDATE())";

	intervalq += ")";

	var q1 = "SELECT COUNT(*) FROM reservation WHERE ownerId=? AND listingId=? and finished = 1 AND "+intervalq;
	var q2 = "SELECT COUNT(*) FROM reservation WHERE ownerId=? AND listingId=? AND "+intervalq;
	// var q2 = "1";

	var q = "SELECT (("+q1+")/("+q2+")) AS success";

	models.sequelize.query(q, {replacements: [req.decoded.id, req.params.id, req.decoded.id, req.params.id], raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total[0].success == null)?0:total[0].success});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
};


exports.guestCMS = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);
	req.assert('type', 'Invalid type').isIn(["user", "listing"]);
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( YEAR(startDate) = YEAR(CURDATE())";

	if (req.query.interval === 'monthly') 
		intervalq += "AND MONTH(startDate) = MONTH(CURDATE())";

	if (req.query.interval === 'weekly') 
		intervalq += "AND WEEK(startDate) = WEEK(CURDATE())";

	intervalq += ")";

	if (req.params.type == 'user') 
		var where = 'ownerId = ?'
		
	if (req.params.type == 'listing') 
		var where = 'listingId = ?'

	var q = "SELECT COUNT(finished) AS total FROM reservation WHERE "+where+" AND "+intervalq;

	models.sequelize.query(q, {replacements: [req.params.id], raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total.total == null)?0:total.total});
	});
};

exports.moneyCMS = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);
	req.assert('type', 'Invalid type').isIn(["user", "listing"]);
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( YEAR(startDate) = YEAR(CURDATE())";

	if (req.query.interval === 'monthly') 
		intervalq += "AND MONTH(startDate) = MONTH(CURDATE())";

	if (req.query.interval === 'weekly') 
		intervalq += "AND WEEK(startDate) = WEEK(CURDATE())";

	intervalq += ")";

	if (req.params.type == 'user') 
		var where = 'ownerId = ?'
		
	if (req.params.type == 'listing') 
		var where = 'listingId = ?'

	var q = "SELECT SUM(cost) AS total FROM reservation WHERE "+where+" and finished = 1 AND "+intervalq;

	models.sequelize.query(q, {replacements: [req.params.id], raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total.total == null)?0:total.total});
	});
};

exports.booksCMS = function(req, res) 
{
	req.assert('interval', 'Invalid interval').isIn(['weekly', 'monthly', 'yearly']);
	req.assert('type', 'Invalid type').isIn(["user", "listing"]);
	req.assert('id', 'Invalid id').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var intervalq = "( YEAR(startDate) = YEAR(CURDATE())";

	if (req.query.interval === 'monthly' || req.query.interval === 'weekly') 
		intervalq += " AND MONTH(startDate) = MONTH(CURDATE())";

	if (req.query.interval === 'weekly') 
		intervalq += " AND WEEK(startDate) = WEEK(CURDATE())";

	intervalq += ")";

	if (req.params.type == 'user') 
		var where = 'ownerId = ?'
		
	if (req.params.type == 'listing') 
		var where = 'listingId = ?'

	var q1 = "SELECT COUNT(*) FROM reservation WHERE "+where+" and finished = 1 AND "+intervalq;
	var q2 = "SELECT COUNT(*) FROM reservation WHERE "+where+" AND "+intervalq;
	// var q2 = "1";

	var q = "SELECT (("+q1+")/("+q2+")) AS success";

	models.sequelize.query(q, {replacements: [req.params.id, req.params.id], raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(total) 
	{
		return res.status(200).json({'total' : (total[0].success == null)?0:total[0].success});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
};