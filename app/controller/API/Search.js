'use strict';
var LatLon = require('geodesy').LatLonSpherical;

var models  = require(__dirname+'/../../model');

exports.listing = function(req, res) 
{
	req.assert('cursor', 'Invalid cursor').isInt({min : 0});	
	req.assert('total', 'Invalid total').isInt({min : 1, max: 40});
	
	req.assert('arrive', 'Invalid arrive').isDate();
	req.assert('depart', 'Invalid depart').isDate().isAfter(req.query.arrive);

	var reseq = "(SELECT DISTINCT count(re.listingId) from reservation as re WHERE re.listingId = li.id AND re.approved = 1 AND re.canceled = 0 AND re.declined = 0 AND re.finished = 0 AND \
	(\
	(re.startDate BETWEEN '"+req.query.arrive+"' AND '"+req.query.depart+"') OR \
	(re.endDate BETWEEN '"+req.query.arrive+"' AND '"+req.query.depart+"')\
	)) < 1 ";


	var selectq = 'SELECT li.id, li.typeId, li.dailyPrice, li.name as listingName, li.image1, li.image2, li.image4, li.image3, li.image5, li.coordinate, \
	us.email, us.firstName, us.lastName, us.picture as userImage, us.id as userId, \
	li.totalReview, li.totalRating, li.address, ex.price as specialPrice, type.name as listingType ';

	var searchq = '';

	var searchAmenities = '';

	if (req.query.keyword !=null && req.query.keyword !=undefined) 
	{
		req.assert('keyword', 'Invalid keyword').isLength({min:2});
		
		if (searchq != '') 
			searchq += ' AND ';

		if (req.query.keyword == '__OTHER__') 
		{
			var searchq = " li.city NOT LIKE '%jakarta%' ";
		}
		else
		{
			var searchq = ' (li.name like "%'+req.query.keyword+'%" OR \
			li.address like "%'+req.query.keyword+'%" OR \
			li.province like "'+req.query.keyword+'" OR \
			li.city like "'+req.query.keyword+'" OR \
			li.district like "'+req.query.keyword+'")';
		}
	}

	if (req.query.guest !=null && req.query.guest !=undefined) 
	{
		req.assert('guest', 'Invalid guest').isInt({min: 1});
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.guest >= '+req.query.guest;
	}
	
	if (req.query.bed !=null && req.query.bed !=undefined) 
	{
		req.assert('bed', 'Invalid bed').isInt();
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.bed >= '+req.query.bed;
	}
	
	if (req.query.bathroom !=null && req.query.bathroom !=undefined) 
	{
		req.assert('bathroom', 'Invalid bathroom').isInt({min: 1});
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.bathroom >= '+req.query.bathroom;
	}

	if (req.query.room !=null && req.query.room !=undefined) 
	{	
		req.assert('room', 'Invalid room').isInt({min: 1});
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.bedroom >= '+req.query.room;
	}

	if (req.query.type !=null && req.query.type !=undefined) 
	{
		req.assert('type', 'Invalid type').isInt({min: 1});
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.typeId = '+req.query.type;
	}
	
	if (req.query.pricebottom !=null && req.query.pricebottom !=undefined) 
	{
		req.assert('pricebottom', 'Invalid pricebottom').isInt({min: 1});
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.dailyPrice >= '+req.query.pricebottom;
	}

	if (req.query.pricetop !=null && req.query.pricetop !=undefined) 
	{
		req.assert('pricetop', 'Invalid pricetop').isInt({min: 1});
		
		if (searchq != '') 
			searchq += ' AND ';

		searchq += ' li.dailyPrice <= '+req.query.pricetop;
	}

	if (req.query.amenities !=null && req.query.amenities !=undefined) 
	{
		if (searchq != '') 
			searchAmenities += ' AND ';

		var amelist = '';

		var amejoin = ' JOIN listingamenities as lia ON (lia.listingId = li.id) ';

		for (var i = req.query.amenities.length - 1; i >= 0; i--) 
		{	
			req.assert('amenities['+i+']', 'Invalid amenities').isInt({min: 1});

			amelist += (amelist===''?'':', ')+req.query.amenities[i];
		}

		searchAmenities += ' lia.amenityId in ('+amelist+') GROUP BY lia.listingId HAVING COUNT(lia.listingId) = '+req.query.amenities.length;
	}	

	var nearbyq = '';

	if ((req.query.lat !=null && req.query.lat !=undefined) || (req.query.lon !=null && req.query.lon !=undefined)) 
	{
		if (req.query.range == null) {
			req.query.range = 10000;
		}

		req.assert('lat', 'Invalid lat').isFloat();
		req.assert('long', 'Invalid long').isFloat();
		req.assert('range', 'Range must be between 2 and 30000').isInt({min : 2, max: 30000});
		
		var center = LatLon(req.query.lat, req.query.long)
		, range = parseInt(req.query.range)/2;

		var p0 = center.destinationPoint(range, 0);
		var p1 = center.destinationPoint(range, 45);
		var p1a = center.destinationPoint(range, 90);
		var p2 = center.destinationPoint(range, 135);
		var p2a = center.destinationPoint(range, 180);
		var p3 = center.destinationPoint(range, 225);
		var p3a = center.destinationPoint(range, 270);
		var p4 = center.destinationPoint(range, 315);

		var polygon = "ST_GEOMFROMTEXT('POLYGON(("+totext(p0)+","+totext(p1)+","+totext(p1a)+","+totext(p2)+","+totext(p2a)+","+totext(p3)+","+totext(p3a)+","+totext(p4)+","+totext(p0)+"))')";

		nearbyq += ' ST_CONTAINS('+polygon+', li.coordinate) = 1 ';
	}

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	searchq += ((req.query.amenities !=null && req.query.amenities !=undefined)?' '+searchAmenities:'');

	var joinextra = 'LEFT OUTER JOIN extraprice as ex ON (ex.listingId = li.id AND ex.date = CURDATE())'
	var jointype = 'LEFT OUTER JOIN type as type ON (type.id = li.typeId)'

	var searchfrom = 'FROM listing as li JOIN user as us ON (us.id = li.userId) '+joinextra+jointype+' '+((req.query.amenities !=null && req.query.amenities !=undefined)?amejoin:'');

	var limit = parseInt(req.query.total);
	var offset = parseInt(req.query.total)*parseInt(req.query.cursor);

	var finalq = selectq+searchfrom+' WHERE li.unlisted=0 AND '+reseq+(searchq!=''?' AND '+searchq:'')+(nearbyq!=''?' AND '+nearbyq:'')+' ORDER BY li.createdAt DESC LIMIT '+offset+','+limit;
	
	models.sequelize.query(finalq, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(listing) 
	{
		return res.status(200).json({'listing' : listing, 'page' : parseInt(req.query.cursor), 'countperpage' : parseInt(req.query.total)});
	});

	function totext(thepoint) 
	{
		return thepoint.lon+" "+thepoint.lat;
	}
};

exports.region = function(req, res) 
{
	req.assert('keyword', 'Invalid keyword').isLength({min : 3});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.aggregate('city', 'DISTINCT', 
	{ 
		where : {
			city: {$like : '%'+req.query.keyword+'%'},
		},
		plain: false
	}).then(function (location) 
	{
		var returns = [];

		for (var i = location.length - 1; i >= 0; i--) 
		{
			returns.push(location[i].DISTINCT);
		}

		return res.status(200).json({'location' : returns});
		
	}).catch(function(error) {
		return res.status(500).json({'err' : error})
	});
};
