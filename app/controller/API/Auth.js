'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');

var bcrypt = require('bcryptjs');

var path = require("path");
var key = global.APP_CONFIGS.app.secret;

var jwt = require('jsonwebtoken');

var string  = require(__dirname+'/../../helper/string');

exports.login = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();
	req.assert('password', 'Invalid password').isLength(6);

	if (req.body.device != null) 
	{
		req.assert('device', 'Invalid device type').isIn(["android", "ios"]);
		req.assert('device_token', 'Invalid device token').notEmpty();
	}


	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var insufficient = {
		phone: true,
		email: true,
		photo: true,
		bio: true
	};

	var data = {};
	var token;

	models.User.findOne({
		attributes: ['id', 'firstName', 'lastName', 'email', 'phone', 'password', 'phoneActive', 'emailActive', 'about', 'address'],
		where: {
			email: req.body.email
		}
	})
	.then(function(user) 
	{
		if (user === null || !bcrypt.compareSync(req.body.password, user.dataValues.password)) 
			throw {
				type: 401,
				message: 'wrong email / password'
			}

		data = {
			phone: user.phone,
			email: user.email,
		}

		if (!user.emailActive)
			insufficient.email = false;

		if (!user.phoneActive)
			insufficient.phone = false;

		if (user.about == null || user.address == null)
			insufficient.bio = false;

		if (user.picture == null || user.picture == '')
			insufficient.photo = false;

		delete user.dataValues['password'];
		
		user.dataValues.identifier = string.nameInitial(user.firstName, user.lastName).toUpperCase();

		if (req.body.device != null) 
		{
			user.dataValues.device = req.body.device;
			user.dataValues.deviceToken = req.body.device_token;
		}
		
		token = jwt.sign(user.dataValues, key);
		
		return models.User.update({
			device: req.body.device,
			pushToken: req.body.device_token
		}, {
			where: {
				email: req.body.email
			}
		})
	})
	.then(function (added) {
		return res.status(201).json({'token' : token, insufficient: insufficient, data: data});
	})
	.catch(function (err) {
		return res.status(err.type).json({error: err.message})
	})
};

exports.loginFB = function(req, res) {
	req.assert('fb_id', 'Invalid fb id').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var insufficient = {
		phone: true,
		email: true,
		photo: true,
		bio: true
	};

	var data = {};
	var token;

	models.Facebook.findOne({
		where: {
			facebookId: req.body.fb_id
		}
	})
	.then(function (fb) {
		if (fb == null) 
			throw {
				type: 201,
				message: 'register'
			}
		else
			return models.User.findById(fb.userId);
	})
	.then(function (user) {
		if (user === null) 
			return res.status(401).json({'error' : 'not found'});

		insufficient = {
			phone: true,
			email: true,
			photo: true,
			bio: true
		};

		data = {
			phone: user.phone,
			email: user.email,
		}

		if (!user.emailActive)
			insufficient.email = false;

		if (!user.phoneActive)
			insufficient.phone = false;

		if (user.about == null || user.address == null)
			insufficient.bio = false;

		if (user.picture == null || user.picture == '')
			insufficient.photo = false;

		delete user.dataValues['password'];
		
		user.dataValues.identifier = string.nameInitial(user.firstName, user.lastName).toUpperCase();
		user.dataValues.device = req.body.device;
		user.dataValues.deviceToken = req.body.device_token;
		
		token = jwt.sign(user.dataValues, key);
		
		return models.User.update({
			device: req.body.device,
			pushToken: req.body.device_token
		}, {
			where: {
				id: user.dataValues.id
			}
		})
	})
	.then(function (added) {
		return res.status(201).json({'token' : token, insufficient: insufficient, data: data});
	})
	.catch(function (err) {
		return res.status(500).json({err: err})
	})
}


exports.loginGoogle = function(req, res) {
	req.assert('google_id', 'Invalid fb id').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var insufficient = {
		phone: true,
		email: true,
		photo: true,
		bio: true
	};

	var data = {};
	var token;

	models.Google.findOne({
		where: {
			googleId: req.body.google_id
		}
	})
	.then(function (google) {
		if (google == null) 
			throw {
				type: 201,
				message: 'register'
			}
		else
			return models.User.findById(google.userId);
	})
	.then(function (user) {
		if (user === null) 
			return res.status(401).json({'error' : 'not found'});

		insufficient = {
			phone: true,
			email: true,
			photo: true,
			bio: true
		};

		data = {
			phone: user.phone,
			email: user.email,
		}

		if (!user.emailActive)
			insufficient.email = false;

		if (!user.phoneActive)
			insufficient.phone = false;

		if (user.about == null || user.address == null)
			insufficient.bio = false;

		if (user.picture == null || user.picture == '')
			insufficient.photo = false;

		delete user.dataValues['password'];
		
		user.dataValues.identifier = string.nameInitial(user.firstName, user.lastName).toUpperCase();
		user.dataValues.device = req.body.device;
		user.dataValues.deviceToken = req.body.device_token;
		
		token = jwt.sign(user.dataValues, key);
		
		return models.User.update({
			device: req.body.device,
			pushToken: req.body.device_token
		}, {
			where: {
				id: user.dataValues.id
			}
		})
	})
	.then(function (added) {
		return res.status(201).json({'token' : token, insufficient: insufficient, data: data});
	})
	.catch(function (err) {
		return res.status(500).json({err: err})
	})
}



exports.forgot = function(req, res) 
{
	if (req.body.email != null || typeof req.body.email != 'undefined') 
		req.assert('email', 'Invalid email').isEmail();
	
	if (req.body.phone != null || typeof req.body.phone != 'undefined') 
		req.assert('phone', 'Invalid phone').isNumeric();
	
	if ((req.body.phone != null || typeof req.body.phone != 'undefined') && (req.body.email != null || typeof req.body.email != 'undefined'))
	{
		req.assert('phone', 'Invalid phone').isNumeric();
		req.assert('email', 'Invalid email').isEmail();
	}

	models.User.findOne({
		where: {
			email: req.body.email,
		}
	}).then(function(user) 
	{
		if (bcrypt.compareSync(req.body.password, user.dataValues.password)) 
		{
			var token = jwt.sign(user.dataValues, key);
			
			return res.status(201).json({'token' : token});
		}
		else
			return res.status(401).json({'error' : 'wrong email/password'})
	})


};

exports.nyoba = function (req, res) {

	// var mails = [{
	// 	to: 'firyal.fakhrilhadi@gmail.com',
	// 	subject: 'User email activation - RoomMe',
	// 	template: 'account/emailActivation',
	// 	data: {
	// 		name: " firyal",
	// 		actCode: 'kmzwa8awaa'
	// 	}
	// }]

	// var sendmail =  sender.email(mails);
	// return res.json({suc:1})
}

exports.nyobain = function(req, res){
	
	// req.assert('code', 'Invalid code').isLength({min:1});
	// req.assert('acceptance', 'Invalid acceptance').isIn(['accept', 'decline']);

	// var errors = req.validationErrors();

	// if (errors.length>0) 
	// 	return res.status(400).json({'error' : errors});

	// var trip;

	// models.Reservation.findOne({
	// 	where: {
	// 		code: req.params.code,
	// 		// ownerId: req.decoded.id
	// 	},
	// 	include: [{
	// 		model: models.User,
	// 		as: 'Traveller'
	// 	}, {
	// 		model: models.Listing,
	// 	}]
	// }).then(function (reservation) {
	// 	if (reservation===null) 
	// 		return res.status(404).json({error: 'not found'});

	// 	var updateObj;

	// 	trip = reservation;

	// 	if (req.params.acceptance === 'accept') 
	// 		updateObj = {approved: 0};
	// 	else if (req.params.acceptance === 'decline') 
	// 		updateObj = {canceled: 0};

	// 	reservation.update(updateObj).then(function (updated) 
	// 	{
	// 		// var log = logger.memberlog(req.decoded.id, 'book '+req.params.code+((req.params.acceptance === 'accept')?'acceptance':'decline'))

	// 		var tripstat = '';

	// 		if (req.params.acceptance === 'accept') 
	// 		{
	// 			var mails = [{
	// 				to: 'firyal.fakhrilhadi@gmail.com',
	// 				subject: 'Booking request '+trip.code+' accepted',
	// 				template: 'trip/acceptRequest',
	// 				data: {
	// 					trip: trip
	// 				}
	// 			}]
	// 			tripstat = 'accepted';
	// 		}
	// 		else if (req.params.acceptance === 'decline') 
	// 		{
	// 			var mails = [{
	// 				to: 'firyal.fakhrilhadi@gmail.com',
	// 				subject: 'Booking request '+trip.code+' declined',
	// 				template: 'trip/declineRequest',
	// 				data: {
	// 					trip: trip
	// 				}
	// 			}]

	// 			tripstat = 'declined';
	// 		}
	// 		var sendmail =  sender.email(mails);
	// 		return res.status(200).json({'message' : req.params.code+' '+tripstat});
	// 	});
	// 	// return res.status(200).json({reservation: reservation});
	// }).catch(function (err) {
	// 	return res.status(500).json({error: 'Internal server error'});
	// });

}

exports.logout = function(req, res) 
{
	models.User.update({
		device : null,
    	pushToken : null,
	}, {
		where: {
			id: req.decoded.id
		}
	})
	.then(function (updated) {
		return res.status(201).json({
			message: 'logout'
		})
	})
	.catch(function (err) {
		return res.status(500).json({err: err})
	})
}

exports.tokenUpdate = function(req, res) 
{
	req.assert('device', 'Invalid device type').isIn(["android", "ios"]);
	req.assert('device_token', 'Invalid device token').notEmpty();
	
	models.User.update({
		device : req.body.device,
    	pushToken : req.body.device_token,
	},{
		where: {
			id: req.decoded.id
		}
	})
	.then(function (updated) {
		return res.status(201).json({message: 'token updated'})
	})
	.catch(function (err) {
		return res.status(500).json({err: err})
	})

}