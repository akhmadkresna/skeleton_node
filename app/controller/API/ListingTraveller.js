'use strict';
var LatLon = require('geodesy').LatLonSpherical;

var models  = require(__dirname+'/../../model');

exports.show = function(req, res) 
{
	req.assert('id', 'Invalid id').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Listing.findById(req.params.id, {
		include: [{
			model: models.User
		}, {
			model: models.Amenities,
			attributes: ['id', 'name']
		}, {
			model: models.ExtraPrice,
			required: false,
			where: {
				date: models.sequelize.fn('CURDATE')
			},
			attributes: ["price"]
		}, {
			model: models.Type,
			attributes: ['name']
		}]
	}).then(function(listing) {
		if (listing!=null) 			
			return res.status(200).json({'listing' : listing});
		else
			return res.status(404).json({'listing' : null});

	})
};

exports.availability = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var nadates = [];

	models.Reservation.findAll({
		attributes: ['startDate', 'endDate', 'listingId'],
		where: {
			$and:{
				listingId: req.params.id,
				$or:{
					startDate: {$between: [models.sequelize.literal('CURDATE()'), models.sequelize.literal("DATE_ADD(CURDATE(), INTERVAL 3 MONTH)")]},
					endDate: {$between: [models.sequelize.literal('CURDATE()'), models.sequelize.literal("DATE_ADD(CURDATE(), INTERVAL 3 MONTH)")]}
				},
				canceled: 0,
				declined: 0
			}
		}
	}).then(function(reservation) {	

		for (var j = 0; j < reservation.length; j++) 
		{
			for (var i =  reservation[j].startDate; i <= reservation[j].endDate; i.setDate(i.getDate() + 1)) 
			{	
				nadates.push(new Date(i));
			}
		}

		models.Block.findAll({
			where: {
				$and:{
					listingId: req.params.id,
					date: {$between: [models.sequelize.literal('CURDATE()'), models.sequelize.literal("DATE_ADD(CURDATE(), INTERVAL 3 MONTH)")]},
				}
			}
		}).then(function (blocks) {

			for (var j = 0; j < blocks.length; j++) 
			{
				nadates.push(blocks[j].date);
			}

			return res.status(200).json({'unavailable' : nadates});
		}).catch(function (err) {
			return res.status(500).json({'error' : err});
		});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
};

// exports.nearby = function(req, res) 
// {
// 	req.assert('lat', 'Invalid lat').isFloat();
// 	req.assert('long', 'Invalid long').isFloat();
// 	req.assert('total', 'Invalid total').isInt({min: 1});
// 	req.assert('cursor', 'Invalid cursor').isInt({min: 0});

// 	var errors = req.validationErrors();

// 	if (errors.length>0) 
// 		return res.status(400).json({'error' : errors});

// 	var center = LatLon(req.query.lat, req.query.long);

// 	var p0 = center.destinationPoint(1400, 0);
// 	var p1 = center.destinationPoint(1400, 45);
// 	var p1a = center.destinationPoint(1400, 90);
// 	var p2 = center.destinationPoint(1400, 135);
// 	var p2a = center.destinationPoint(1400, 180);
// 	var p3 = center.destinationPoint(1400, 225);
// 	var p3a = center.destinationPoint(1400, 270);
// 	var p4 = center.destinationPoint(1400, 315);

// 	var polygon = "ST_GEOMFROMTEXT('POLYGON(("+totext(p0)+","+totext(p1)+","+totext(p1a)+","+totext(p2)+","+totext(p2a)+","+totext(p3)+","+totext(p3a)+","+totext(p4)+","+totext(p0)+"))')";

// 	var q = "SELECT li.*, us.firstName, us.lastName \
// 	FROM listing as li JOIN user as us ON (us.id = li.userId) WHERE ST_CONTAINS("+polygon+", li.coordinate) = 1;";

// 	models.sequelize.query(q, {raw: true, type: models.sequelize.QueryTypes.SELECT}).then(function(listing) 
// 	{
// 		return res.status(200).json({'listing' : listing, 'page' : parseInt(req.query.cursor), 'countperpage' : parseInt(req.query.total)});
// 	});

// 	function totext(thepoint) 
// 	{
// 		return thepoint.lon+" "+thepoint.lat;
// 	}

// };

exports.explore = function(req, res) 
{
	models.Region.findAll().then(function (location) 
	{
		return res.status(200).json({'location' : location});
	}).catch(function(error) {
		return res.status(500).json({'error' : error})
	});
};

exports.specialPrices = function(req, res) 
{
	req.assert('id', 'Invalid listing').isInt({min: 1});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var specialdates = [];

	models.ExtraPrice.findAll({
		attributes: ['date', 'price'],
		where: {
			$and:{
				listingId: req.params.id,
				date: {$between: [models.sequelize.literal('CURDATE()'), models.sequelize.literal("DATE_ADD(CURDATE(), INTERVAL 3 MONTH)")]},
			}
		},
		order: "date ASC"
	}).then(function (prices) {
		for (var j = 0; j < prices.length; j++) 
		{
			specialdates.push({
				date: prices[j].date,
				price: prices[j].price
			});
		}		
		
		return res.status(200).json({'specialPrices' : specialdates});
	});	
}