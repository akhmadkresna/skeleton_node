'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');

var bcrypt = require('bcryptjs');
var uuid = require('node-uuid');

exports.change = function(req, res) 
{
	req.assert('oldpassword', 'Invalid current password').isLength({min: 6});
	req.assert('password', 'Invalid new password, minimum 6 characters').isLength({min: 6});
	req.assert('cpassword', 'Please re-enter your password. New password and confirmation doesnt match').equals(req.body.password);

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findById( req.decoded.id,{
		attributes: ['id', 'password']
	}).then(function(user) 
	{
		if (bcrypt.compareSync(req.body.oldpassword, user.dataValues.password)) 
		{
			if (!bcrypt.compareSync(req.body.password, user.dataValues.password)) 
			{
				user.update({password: req.body.password}).then(function (updated) 
				{
					return res.status(200).json({'message' : 'success'});
				});
			}
			else
				return res.status(401).json({'message' : 'New password must be different with previous password'});			
		}
		else
			return res.status(401).json({'error' : 'Invalid current password'});
	}).catch(function (err) 
	{
		return res.status(500).json({'error' : err});
	});
}

exports.forgot = function(req, res) 
{
	req.assert('email', 'Invalid email').isEmail();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.User.findOne({
		attributes: ['id', 'resetCode'],
		where: {
			email: req.body.email
		}
	}).then(function (user) {
		if (user===null) 
			return res.status(404).json({'error' : 'not found'});

		req.baseurl = req.protocol + '://' + req.get('host'); //get base url

		var resetcode = uuid.v1();
		user.update({resetCode:resetcode}).then(function (updated) 
		{
			var mails = [{
				to: req.body.email,
				subject: 'Password Reset - RoomMe',
				template: 'account/forgotPassword',
				data: {
					name: user.firstName+" "+user.lastName,
					resetUrl: req.baseurl+'/password/reset/'+resetcode,
					email: req.body.email
				}
			}]

			var sendmail =  sender.email(mails);

			return res.status(200).json({'message' : 'reset initiated'});
		});
	})
	.catch(function (err) 
	{
		return res.status(500).json({'error' : err});
	});



}

// exports.reset = function(req, res) 
// {
// 	req.assert('id', 'Invalid id').isInt();

// 	var errors = req.validationErrors();

// 	if (errors.length>0) 
// 		return res.status(400).json({'error' : errors});

// }