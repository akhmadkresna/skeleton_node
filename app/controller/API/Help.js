'use strict';
var models  = require(__dirname+'/../../model');

exports.store = function(req, res) 
{
	if (req.body.subject!=null) 
		req.assert('subject', 'Invalid subject').isLength({min:3});
	
	req.assert('content', 'Invalid content').isLength({min:10});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});
	
	models.Help.create({
		userId: req.decoded.id,
		subject: (req.body.subject!=null)?req.body.subject:null,
		content: req.body.content
	}).then(function (help) {
		return res.status(201).json({'message' : 'success'});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}