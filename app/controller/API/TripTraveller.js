'use strict';
var models  = require(__dirname+'/../../model');

var sender  = require(__dirname+'/../../helper/sender');
var string  = require(__dirname+'/../../helper/string');
var logger  = require(__dirname+'/../../helper/logger');
var push  = require(__dirname+'/../../helper/push');

exports.index = function(req, res) 
{
	var searchq = new Object();
	var order = '';

	req.assert('type', 'Invalid type').isIn(["prev", "next"]);
	
	if (req.query.type === 'prev') 
	{
		searchq['$or'] =  {
			canceled: 1, 
			declined: 1, 
			finished: 1
		};
		order = 'createdAt ASC';
	}
	else if (req.query.type === 'next') 
	{
		searchq['$and'] =  {
			canceled: 0,
			declined: 0,
			finished: 0 
		};
		
		order = 'createdAt DESC';
	}

	req.assert('total', 'Invalid total').isInt({min: 1});
	req.assert('cursor', 'Invalid cursor').isInt({min: 0});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	searchq['userId'] = req.decoded.id;
	
	var pendq = '(SELECT COUNT(payment.id) FROM payment WHERE payment.reservationCode=Reservation.code AND payment.status = 1)'

	models.Reservation.findAll({
		where: searchq,
		order: order,
		limit: parseInt(req.query.total),
		offset: parseInt(req.query.total)*parseInt(req.query.cursor),
		attributes: [
		'code', 
		'startDate', 
		'endDate', 
		'status', 
		'createdAt', 
		'approved', 
		'paid', 
		'canceled',
		'finished', 
		'declined',
        [models.sequelize.literal(pendq), 'pending'],
		],
		// attributes: ['code', 'startDate', 'endDate', 'approved', 'paid', 'canceled', 'finished'],
		include: [{
			model: models.Listing,
			attributes: [
			"id", 
			"name", 
			"province", 
			"address", 
			"city", 
			"district", 
			"coordinate", 
			"dailyPrice", 
			"totalRating", 
			"totalReview", 
			"image1"
			],
			include: [{
				model: models.Type,
				attributes: ['name']
			}]
		}]
	}).then(function (trips) {
		var returns = [];
		for (var i = trips.length - 1; i >= 0; i--) 
		{
			trips[i].dataValues.status = string.tripStatus(trips[i]);

			if (trips[i].dataValues.status === 'Accepted' && trips[i].dataValues.pending>0) 
				trips[i].dataValues.status = 'Payment pending';

			returns.push(trips[i].dataValues);
		}
		return res.status(200).json({'trips' : returns, 'page' : req.query.cursor, 'countperpage' : req.query.total});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.show = function(req, res) 
{
	req.assert('code', 'Invalid code').isLength({min:6});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.findOne({
		where: {
			code: req.params.code,
			userId: req.decoded.id
		},
		include: [{
			model: models.Listing,
			attributes: [
			"id", 
			"name", 
			"province", 
			"address", 
			"city", 
			"district", 
			"coordinate", 
			"dailyPrice", 
			"totalRating", 
			"totalReview", 
			"image1"
			],
			include: [{
				model: models.Type,
				attributes: ['name']
			}]
		}, {
			model: models.Payment,
			where: {
				status: 1
			},
			attributes: [[models.sequelize.fn('COUNT', '*'), 'pending']] 
		}, {
			model: models.User,
			as: 'Host',
			attributes: ['firstName', 'lastName']
		}]
	}).then(function (trip) {
		if (trip == null)
			return res.status(404).json({'error' : 'not found'});

		var returns = {};
		
		trip.dataValues.status = string.tripStatus(trip);

		if (trip.dataValues.Payments.length > 0) 
		{
			if (trip.dataValues.status === 'Accepted' && trip.dataValues.Payments[0].dataValues.pending>0) 
				trip.dataValues.status = 'Payment pending';
		}

		returns = trip.dataValues;
		
		return res.status(200).json({'trip' : returns});
	})
	// .catch(function (err) {
	// 	return res.status(500).json({'error' : err});
	// });
}

exports.store = function(req, res, next)  
{
	var theidentifier = req.decoded.identifier+string.getDDMMYY();
	
	req.assert('listing', 'Invalid listing').isLength({min:1});
	req.assert('start', 'Invalid start').isDate();
	req.assert('end', 'Invalid end').isDate();
	req.assert('guest', 'Invalid guest').isInt();

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var currentlisting;
	var bookCode;
	var disc = {
		monthly:0,
		weekly:0
	};

	var start = new Date(req.body.start);
	var end = new Date(req.body.end);

	var timeDiff = Math.abs(end.getTime() - start.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

	models.Listing.findById(req.body.listing, {
		include: [{
			model: models.User,
			attributes: ['email', 'firstName', 'lastName', 'device', 'pushToken']
		}]
	}).then(function (listing) {
		if (listing === null)
			return res.status(404).json({'error' : 'listing not found'});

		if (req.body.guest > listing.guest) 
			return res.status(400).json({'error' : 'Max guest exceeded'});

		currentlisting = listing;

		disc.weekly = listing.weeklyDisc;
		disc.monthly = listing.monthlyDisc;

		if ((listing.minLimit>0 && listing.minLimit > diffDays) || (listing.maxLimit>0 && listing.maxLimit < diffDays)) 
			return res.status(400).json({'error' : 'Book period exceeded'});



		return models.Reservation.findOne({
			where: {
				code: {
					$like: theidentifier+'%'
				}
			},
			order: 'code DESC'
		})
	})
	.then(function (book) {
		bookCode = string.bookCode(theidentifier, (book == null?null:book.code.substr(9, 11)));
	
		var totalcost = diffDays * currentlisting.dailyPrice;		
		
		if (diffDays >= 30) 
			totalcost -= Math.floor(totalcost*(disc.monthly / 100))
		else if (diffDays >=7 ) 
			totalcost -= Math.floor(totalcost*(disc.weekly / 100))

		models.Reservation.create({
			code: bookCode,
			userId: req.decoded.id,
			listingId: req.body.listing,
			ownerId: currentlisting.userId,
			startDate: req.body.start,
			endDate: req.body.end,
			totalGuest: req.body.guest,
			cost: totalcost
		}).then(function (booking) {
			var mails = [{
				to: currentlisting.User.email,
				subject: 'Book Request - Roomme',
				template: 'trip/bookRequest',
				data: {
					booking: booking,
					travellerName: req.decoded.firstName+' '+req.decoded.lastName,
					hostName: currentlisting.User.firstName+' '+currentlisting.User.lastName,
					listing: currentlisting
				}
			}]

			var sendmail =  sender.email(mails);

			if (currentlisting.User.device == 'ios') 
				var test =  push.pushIOS(currentlisting.User.pushToken, 'Book request received', {});
			else if (currentlisting.User.device == 'android') 
				var test =  push.pushGCM(currentlisting.User.pushToken, 'Book request received', {});

			var log = logger.memberlog(req.decoded.id, 'book request '+bookCode)

			return res.status(201).json({'code': bookCode });

		})
	})
}


exports.destroy = function(req, res) 
{
	req.assert('code', 'Invalid code').isLength({min:6});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	models.Reservation.find({
		where: {
			code: req.params.code,
			userId: req.decoded.id
		}
	}).then(function (trip) {
		if (trip == null) 
			return res.status(404).json({'error' : 'not found'});

		trip.update({canceled:1}).then(function (updated) 
		{
			return res.status(200).json({'message' : 'trip canceled'});
		});		

	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}

exports.storeReview = function(req, res) 
{
	req.assert('code', 'Invalid code').isLength({min:6});
	req.assert('star', 'Invalid star').isInt({min:1, max:5});
	req.assert('review', 'Invalid review').isLength({min:20});

	var errors = req.validationErrors();

	if (errors.length>0) 
		return res.status(400).json({'error' : errors});

	var ownerId;

	models.Reservation.findOne({
		where: {
			code: req.params.code,
			userId: req.decoded.id,
			reviewDate: null 
		}
	}).then(function (trip) {
		if (trip==null) 
			return res.status(404).json({error: 'not found'});

		ownerId = trip.ownerId;

		return models.Reservation.update({
			star : req.body.star,
			review : req.body.review,
			reviewDate: models.sequelize.literal('CURDATE()')
		},{
			where: {
				code : req.params.code,
			}
		})
	})
	.then(function (updated) {
		return models.User.update({
			totalReview: models.sequelize.literal('totalReview + 1')
		}, {
			where: {
				id : ownerId
			}
		})
	})
	.then(function (updated) {
		var log = logger.memberlog(req.decoded.id, 'trip review '+req.params.code)

		return res.status(200).json({message: 'reviewed', trip: updated});
	})
	// .catch(function (err) {
	// 	return res.status(500).json({error: err});
	// });

};


exports.notif = function(req, res) 
{
	var searchq = new Object();
	searchq['$and'] = [ {canceled: 0}, {finished: 0 }];
	searchq['userId'] = req.decoded.id;
	
	models.Reservation.count({
		where: {
			declined: 0,
			canceled: 0,
			finished: 0,
			approved: 1,
			userId: req.decoded.id
		}
	}).then(function (trips) {
		return res.status(200).json({'count' : trips});
	}).catch(function (err) {
		return res.status(500).json({'error' : err});
	});
}