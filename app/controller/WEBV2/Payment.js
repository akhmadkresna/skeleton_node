'use strict';
const APIFormat = require(__dirname+'/../../helper/APIFormat')
, message = require(__dirname+'/../../helper/Message')
, sender = require(__dirname+'/../../helper/sender')
, models = require(__dirname+'/../../model')
, promiseReq = require('request-promise')
, config = require('./../../config/'+((process.env.NODE_ENV==='local-dev')?'local-dev/':'')+'xendit')[process.env.NODE_ENV];

module.exports = {
	dev1(req, res) {
		console.log(13123)
		return message.textNexmo('6281519040668', 'Test roomme')
		// var options = {
		// 	method: 'POST',
		// 	uri: 'https://api.xendit.co/v2/invoices',
		// 	auth: {
		// 		user: 'xnd_development_NYyBfOQi0OGoksRufbZPGDTGMNXwp4N4lH20+R1r/mXR8LagBAdwjw==',
		// 		pass: ''
		// 	},
		// 	body: {
		// 		'external_id': 'demo_1475801aa',
		// 		'payer_email': 'firyal.fakhrilhadi@gmail.com',
		// 		'description': 'a test',
		// 		'amount': 2500000,
		// 	},
		// 	json: true
		// };

		// promiseReq(options)
		.then(function (resp) {

			return res.json({success: resp})
		})
		// .catch(function (err) {
		// 	return res.json({success: err})
		// });
	},

	show(req, res){
		req.assert('bookcode', 'Invalid bookcode').notEmpty();

		var errors = req.validationErrors();

		if (errors.length>0) 
			return res.redirect('/whoops/404');

		var trip;

		return models.sequelize.transaction(function (t) {
			return models.Reservation.findOne({
				where: {
					code: req.query.bookcode,
					paid: 0,
					approved: 1
				},
				include: [{
					model: models.User,
					as: 'Traveller'
				}],
				transaction: t
			})
			.then(function (found) {

				if (found == null) {
					throw {
						status: 404,
						message: 'Not found',
						errors: {}
					}
				}

				trip = found;

				return models.Payment.create({
					reservationCode: req.query.bookcode,
					location: null,
					xendit: 1,
					status: 2
				}, { transaction: t});
			})
			.then(function (payment) {

				var options = {
					method: 'POST',
					uri: 'https://api.xendit.co/v2/invoices',
					auth: {
						user: config.secretKey,
						pass: ''
					},
					body: {
						'external_id': req.query.bookcode,
						'payer_email': trip.Traveller.email,
						'description': 'Payment for Roomme trip, '+req.query.bookcode,
						'amount': parseInt(trip.cost)+(0.04*parseInt(trip.cost)),
					},
					json: true
				};

				return promiseReq(options);
			})
		})
		.then(function (resp) {
			return res.redirect(resp.invoice_url)
		})
		.catch(function (err) {
			if (err.status != null) 
				return res.redirect('/whoops/'+err.status);
			else
				return res.redirect('/whoops/500');
		});
		
	},

	callback(req, res){
		return models.sequelize.transaction(function (t) {
			return models.Reservation.update({
				paid: true
			}, {
				where: {
					code: req.body.external_id,
					paid: false
				},
				transaction: t
			})
			.then(function (trip) {
				if (trip[0] < 1) {
					throw {
						status: 404,
						message: 'Not found',
						errors: {}
					}
				}

				return models.Payment.findOne({
					where: {
						reservationCode: {
							$like: req.body.external_id
						}
					},
					include: [{
						model: models.Reservation,
						include: [{
							model: models.User,
							as: 'Traveller'
						}, {
							model: models.Listing
						}]
					}],
					transaction: t
				})
			})
			.then(function (payment) {
				var mails = [{
					to: payment.Reservation.Traveller.email,
					subject: 'Payment for '+payment.reservationCode+' approved',
					template: 'payment/approvedDoku',
					data: {
						payment: payment
					}
				}]

				var sendmail =  sender.email(mails);

				return sendmail;
			});
		})
		.then(function (success) {
			console.log(success)
			return res.status(201).json( APIFormat.response( 'Success', {} ) );
		})
		.catch(function (err) {
			console.log(err)
			if (err.status != null) 
				return res.status(err.status).json( APIFormat.response(err.message, {}, err.errors) );
			else
				return res.status(500).json( APIFormat.response('internal server error', {}, err) );
		})

	}
}