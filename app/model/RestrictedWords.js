"use strict";

module.exports = function(sequelize, DataTypes) {
  var RestrictedWords = sequelize.define("RestrictedWords", {
    value: { type: DataTypes.STRING },
    isRestricted: { type: DataTypes.BOOLEAN },
  }, 
  {
    timestamps: false,
    paranoid: false,
    freezeTableName: true,
    tableName: 'restrict_words',
    classMethods: {
      
    }
  });

  return RestrictedWords;
};