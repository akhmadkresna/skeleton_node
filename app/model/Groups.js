"use strict";

module.exports = function(sequelize, DataTypes) {
  var Groups = sequelize.define("Groups", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    name: {type: DataTypes.STRING, allowNull:false}
  }, 
  {
    timestamps: false,
    paranoid: false,
    freezeTableName: true,
    tableName: 'groups',
    classMethods: {
      associate: function(models) {
        Groups.hasMany(models.Admin);
      }
    }
  });

  return Groups;
};