"use strict";

module.exports = function(sequelize, DataTypes) {
  var Listing = sequelize.define("Listing", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true },
    userId: { type: DataTypes.INTEGER(11).UNSIGNED},
    typeId: { type: DataTypes.INTEGER(11).UNSIGNED},
    locationTypeId: { type: DataTypes.INTEGER(11).UNSIGNED},
    name: { type: DataTypes.STRING },
    description: { type: DataTypes.TEXT },
    address: { type: DataTypes.TEXT },
    country: { type: DataTypes.STRING },
    province: { type: DataTypes.STRING },
    city: { type: DataTypes.STRING },
    district: { type: DataTypes.STRING },
    coordinate: DataTypes.GEOMETRY('POINT'),
    postal: { type: DataTypes.STRING },
    guest: { type: DataTypes.INTEGER },
    bedroom: { type: DataTypes.INTEGER },
    bed: { type: DataTypes.INTEGER },
    bathroom: { type: DataTypes.INTEGER },
    rule: { type: DataTypes.TEXT, allowNull: true },
    dailyPrice: { type: DataTypes.INTEGER },
    weeklyDisc: { type: DataTypes.INTEGER },
    monthlyDisc: { type: DataTypes.INTEGER },
    secured: { type: DataTypes.BOOLEAN },
    unlisted: { type: DataTypes.BOOLEAN, defaultValue: false },
    access: { type: DataTypes.TEXT, allowNull: true},
    neighborhood: { type: DataTypes.TEXT, allowNull: true },
    sameDay: { type: DataTypes.INTEGER },
    prepDays: { type: DataTypes.INTEGER },
    minLimit: { type: DataTypes.INTEGER },
    maxLimit: { type: DataTypes.INTEGER },
    totalRating : {type: DataTypes.FLOAT, defaultValue: 0},
    totalReview : {type: DataTypes.INTEGER, defaultValue: 0},
    imageLocation: { type: DataTypes.TEXT },
    identifier: { type: DataTypes.STRING },
    image1: { type: DataTypes.STRING, allowNull: true },
    image2: { type: DataTypes.STRING, allowNull: true },
    image3: { type: DataTypes.STRING, allowNull: true },
    image4: { type: DataTypes.STRING, allowNull: true },
    image5: { type: DataTypes.STRING, allowNull: true }
  }, 
  {
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'listing',
    classMethods: {
      associate: function(models) {
        Listing.belongsTo(models.User);
        Listing.hasMany(models.Reservation);
        Listing.hasMany(models.ExtraPrice);
        Listing.belongsTo(models.Type);
        Listing.belongsTo(models.LocationType);
        Listing.belongsToMany(models.Amenities, { through: 'ListingAmenities', foreignKey: 'listingId' });
        // console.log(Listing.belongsTo(models.User));
      }
    }
  });

  return Listing;
};