"use strict";

module.exports = function(sequelize, DataTypes) {
  var Verification = sequelize.define("Verification", {
    id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV1, primaryKey: true },
    userId: { type: DataTypes.INTEGER(11).UNSIGNED, primaryKey: true },
    type: {type: DataTypes.INTEGER, allowNull: false},
    status : {type: DataTypes.INTEGER, defaultValue: 1},
    location : {type: DataTypes.STRING}
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'verification',
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User, {foreignKey: 'userId'})
      }
    }
  });

  return Verification;
};