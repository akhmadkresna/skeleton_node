"use strict";

module.exports = function(sequelize, DataTypes) {
  var Payout = sequelize.define("Payout", {
    userId: { type: DataTypes.INTEGER(11).UNSIGNED },
    bankId: { type: DataTypes.INTEGER(11).UNSIGNED },
    name: {type: DataTypes.STRING},
    accNumber: {type: DataTypes.STRING},
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'payout',
    classMethods: {
      associate: function(models) {
        Payout.belongsTo(models.Bank);
      }
    }
  });

  return Payout;
};