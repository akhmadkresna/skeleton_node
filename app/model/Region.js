"use strict";

module.exports = function(sequelize, DataTypes) {
  var Region = sequelize.define("Region", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    name: {type: DataTypes.STRING, allowNull:false},
    type: {type: DataTypes.STRING, allowNull:false},
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'region'
  });

  return Region;
};