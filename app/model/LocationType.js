"use strict";

module.exports = function(sequelize, DataTypes) {
  var LocationType = sequelize.define("LocationType", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true },
    serviceTypeId: { type: DataTypes.INTEGER(11).UNSIGNED },
    name: { type :DataTypes.STRING},
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'locationtype',
    classMethods: {
      associate: function(models) {
        LocationType.belongsTo(models.ServiceType);
        LocationType.hasMany(models.Listing);
      }
    }
  });

  return LocationType;
};