"use strict";

module.exports = function(sequelize, DataTypes) {
  var Block = sequelize.define("Block", {
    listingId: { type: DataTypes.INTEGER(11).UNSIGNED, primaryKey:true},
    // startDate: {type: DataTypes.DATEONLY, allowNull:false},
    // endDate: {type: DataTypes.DATEONLY, allowNull:false},
    date: {type: DataTypes.DATEONLY, allowNull:false},
  }, 
  {
    timestamps: false,
    paranoid: false,
    freezeTableName: true,
    tableName: 'block',
    classMethods: {
      associate: function(models) {
        Block.belongsTo(models.Listing);
      }
    }
  });

  return Block;
};