"use strict";

module.exports = function(sequelize, DataTypes) {
  var Message = sequelize.define("Message", {
    id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV1, primaryKey: true },
    fromId: { type: DataTypes.INTEGER(11).UNSIGNED },
    toId: { type: DataTypes.INTEGER(11).UNSIGNED },
    listingId: { type: DataTypes.INTEGER(11).UNSIGNED },
    content: {type: DataTypes.TEXT},
    delivered: {type: DataTypes.BOOLEAN, defaultValue: false},
  }, 
  {
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'message',
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Listing)
        // this.belongsTo(models.User)
      }
    }
  });

  return Message;
};