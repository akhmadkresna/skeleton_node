"use strict";

module.exports = function(sequelize, DataTypes) {
  var Log = sequelize.define("Log", {
    id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV1, primaryKey: true },
    adminId: { type: DataTypes.INTEGER(11).UNSIGNED, allowNull: false},
    activity: {type: DataTypes.STRING, allowNull:false},
    userAgent: {type: DataTypes.STRING, allowNull:false},
    ipAddress: {type: DataTypes.STRING, allowNull:false},
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'log',
    classMethods: {
      associate: function(models) {
        Log.belongsTo(models.Admin);
      }
    }
  });

  return Log;
};