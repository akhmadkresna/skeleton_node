"use strict";

var bcrypt = require('bcryptjs');

var helperString = require('../helper/string');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true },
    email: { type :DataTypes.STRING, unique: true},
    password : { type :DataTypes.STRING},
    phoneActivationCode : { type :DataTypes.STRING, allowNull: true},
    emailActivationCode : { type :DataTypes.STRING, allowNull: true},
    status : { type :DataTypes.INTEGER, defaultValue: 1, allowNull:false},
    emailActive : { type :DataTypes.BOOLEAN, defaultValue: 0, allowNull:false},
    phoneActive : { type :DataTypes.BOOLEAN, defaultValue: 0, allowNull:false},
    resetCode : { type :DataTypes.STRING},
    firstName : { type :DataTypes.STRING},
    lastName : { type :DataTypes.STRING},
    bankName : { type :DataTypes.STRING, allowNull: true},
    bankAccName : { type :DataTypes.STRING, allowNull: true},
    bankAccNum : { type :DataTypes.STRING, allowNull: true},
    birthDate : { type :DataTypes.DATE, allowNull:true},
    phone : { type: DataTypes.STRING(20), defaultValue: null},
    address : { type: DataTypes.STRING(200)},
    about : { type: DataTypes.STRING(200)},
    picture : { type: DataTypes.STRING},
    totalReview : { type :DataTypes.INTEGER, defaultValue: 0, allowNull:false},
    suspended : { type :DataTypes.BOOLEAN, defaultValue: 0, allowNull:false},
    device : { type :DataTypes.STRING, allowNull:true},
    pushToken : { type :DataTypes.STRING, allowNull:true},
  }, 
  {
    timestamps: true,
    tableName: 'user',
    classMethods: {
      associate: function(models) {
        User.hasMany(models.Review, {foreignKey : 'userId', as: 'reviewer'});
        User.hasMany(models.Review, {foreignKey : 'ownerId', as: 'reviewed'});
        User.hasMany(models.Listing);
        User.hasMany(models.MemberLog);
        User.hasMany(models.Verification);
        User.hasMany(models.Reservation, {foreignKey: 'userId', as: 'Traveller'});
        User.hasMany(models.Reservation, {foreignKey: 'ownerId', as: 'Host'});
        User.hasMany(models.Payout);
        User.hasMany(models.Message, {as: 'sent', foreignKey: 'fromId'});
        User.hasMany(models.Message, {as: 'received', foreignKey: 'toId'});

        User.hasOne(models.Facebook);
        User.hasOne(models.Google);
      }
    },
    instanceMethods: {
        validPassword: function (password) {
            return bcrypt.compareSync(password, this.password);
        }
    }
  });

  var hasSecurePassword = function(user, options, callback) {
    bcrypt.hash(user.get('password'), 5, function(err, hash) {
      if (err) return callback(err);
      user.set('password', hash);
      return callback(null, options);
    });
  };

  User.beforeCreate(function(user, options, callback) {
    user.email = user.email.toLowerCase();

    if (user.password)
      hasSecurePassword(user, options, callback);
    else
      return callback(null, options);
  });
  
  User.beforeUpdate(function(user, options, callback) {
    // user.email = user.email.toLowerCase();
    console.log(user.password);
    if (user.password!= undefined)
      hasSecurePassword(user, options, callback);
    else
      return callback(null, options);
  });

  return User;
};