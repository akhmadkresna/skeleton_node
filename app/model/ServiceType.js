"use strict";

module.exports = function(sequelize, DataTypes) {
  var ServiceType = sequelize.define("ServiceType", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true },
    serviceTypeId: { type: DataTypes.INTEGER(11).UNSIGNED },
    name: { type: DataTypes.STRING},
  }, 
  {
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'servicetype',
    classMethods: {
      associate: function(models) {
        ServiceType.hasMany(models.Type);
        ServiceType.hasMany(models.LocationType);
      }
    }
  });

  return ServiceType;
};