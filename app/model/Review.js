"use strict";

module.exports = function(sequelize, DataTypes) {
  var Review = sequelize.define("Review", {
    reservationCode: { type: DataTypes.STRING, primaryKey: true},
    star: { type: DataTypes.INTEGER},
    review: { type: DataTypes.STRING},
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'review',
    classMethods: {
      associate: function(models) {
        Review.belongsTo(models.Reservation);
        Review.belongsTo(models.Listing);
        Review.belongsTo(models.User, {foreignKey: 'ownerId', as: 'reviewed'});
        Review.belongsTo(models.User, {foreignKey: 'userId', as: 'reviewer'});
      }
    }
  });

  return Review;
};