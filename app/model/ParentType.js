"use strict";

module.exports = function(sequelize, DataTypes) {
  var ParentType = sequelize.define("ParentType", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true },
    name: { type :DataTypes.STRING},
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'parenttype',
    classMethods: {
      associate: function(models) {
        // this.hasMany(models.Type)
      }
    }
  });

  return ParentType;
};