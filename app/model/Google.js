"use strict";

module.exports = function(sequelize, DataTypes) {
  var Google = sequelize.define("Google", {
    userId: { type: DataTypes.INTEGER(11).UNSIGNED, primaryKey: true},
    fname: DataTypes.STRING,
    lname: DataTypes.STRING,
    email: DataTypes.STRING,
    gender: DataTypes.STRING,
    locale: DataTypes.STRING,
    gpluslink: DataTypes.STRING,
    picture: DataTypes.STRING,
    googleId: DataTypes.STRING
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'google',
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User)
      }
    }
  });

  return Google;
};