"use strict";
var bcrypt = require('bcryptjs');

module.exports = function(sequelize, DataTypes) {
  var Admin = sequelize.define("Admin", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    groupId: { type: DataTypes.INTEGER(11).UNSIGNED},
    email: {type: DataTypes.STRING, allowNull:false},
    password: {type: DataTypes.STRING, allowNull:false},
    fname: {type: DataTypes.STRING, allowNull:false},
    lname: {type: DataTypes.STRING, allowNull:false},
    reset: {type: DataTypes.STRING, allowNull:true, defaultValue: null},
  }, 
  {
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'admin',
    classMethods: {
      associate: function(models) {
        Admin.hasMany(models.Log);
        Admin.hasMany(models.Help);
        Admin.belongsTo(models.Groups);
      }
    },
    instanceMethods: {
      validPassword: function (password) {
        return bcrypt.compareSync(password, this.password);
      }
    }
  });

  var hasSecurePassword = function(admin, options, callback) {
    bcrypt.hash(admin.get('password'), 5, function(err, hash) {
      if (err) return callback(err);
      admin.set('password', hash);
      return callback(null, options);
    });
  };

  Admin.beforeCreate(function(admin, options, callback) {
    admin.email = admin.email.toLowerCase();

    if (admin.password)
      hasSecurePassword(admin, options, callback);
    else
      return callback(null, options);
  });
  
  Admin.beforeUpdate(function(admin, options, callback) {
    if (admin.password!= undefined)
      hasSecurePassword(admin, options, callback);
    else
      return callback(null, options);
  });

  return Admin;
};