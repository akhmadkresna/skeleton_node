"use strict";

module.exports = function(sequelize, DataTypes) {
  var Payment = sequelize.define("Payment", {
    id: {type: DataTypes.UUID(), primaryKey: true, defaultValue: DataTypes.UUIDV4()},
    reservationCode: { type: DataTypes.STRING},
    location: {type: DataTypes.STRING},
    status: { type: DataTypes.INTEGER(11).UNSIGNED, defaultValue: 1 },
    doku: {type: DataTypes.BOOLEAN, defaultValue:false},
    xendit: {type: DataTypes.BOOLEAN, defaultValue:false},
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'payment',
    classMethods: {
      associate: function(models) {
        Payment.belongsTo(models.Reservation, {foreignKey: 'reservationCode'});
      }
    }
  });

  return Payment;
};