"use strict";

module.exports = function(sequelize, DataTypes) {
  var Type = sequelize.define("Type", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true },
    serviceTypeId: { type: DataTypes.INTEGER(11).UNSIGNED },
    name: { type: DataTypes.STRING},
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'type',
    classMethods: {
      associate: function(models) {
        Type.hasMany(models.Listing);
      }
    }
  });

  return Type;
};