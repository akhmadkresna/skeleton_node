"use strict";

module.exports = function(sequelize, DataTypes) {
  var Help = sequelize.define("Help", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    userId: { type: DataTypes.INTEGER(11).UNSIGNED},
    adminId: { type: DataTypes.INTEGER(11).UNSIGNED},
    subject: {type: DataTypes.STRING, allowNull:true},
    content: {type: DataTypes.STRING, allowNull:false},
    reply: {type: DataTypes.STRING, allowNull:true, defaultValue:null},
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'help',
    classMethods: {
      associate: function(models) {
        Help.belongsTo(models.Admin);
        Help.belongsTo(models.User);
      }
    },
  });

  return Help;
};