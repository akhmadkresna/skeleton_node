"use strict";

module.exports = function(sequelize, DataTypes) {
  var Amenities = sequelize.define("Amenities", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    name: {type: DataTypes.STRING, allowNull:false},
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'amenities',
    classMethods: {
      associate: function(models) {
        Amenities.belongsToMany(models.Listing, { 
          through: 'ListingAmenities',
          foreignKey: 'amenityId'
        });
      }
    }
  });

  return Amenities;
};