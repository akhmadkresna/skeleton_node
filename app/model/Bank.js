"use strict";

module.exports = function(sequelize, DataTypes) {
  var Bank = sequelize.define("Bank", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    bankName: {type: DataTypes.STRING, allowNull:true},
    accountName: {type: DataTypes.STRING, allowNull:true},
    accountNum: {type: DataTypes.STRING, allowNull:true}
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'bank'
  });

  return Bank;
};