"use strict";

module.exports = function(sequelize, DataTypes) {
  var Article = sequelize.define("Article", {
    id: { type: DataTypes.INTEGER(11).UNSIGNED, autoIncrement: true, primaryKey: true},
    slug: {type: DataTypes.STRING, allowNull:false},
    title: {type: DataTypes.STRING, allowNull:false},
    content: {type: DataTypes.TEXT, allowNull:false}
  }, 
  {
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'article',
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return Article;
};