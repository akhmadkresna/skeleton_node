"use strict";

module.exports = function(sequelize, DataTypes) {
  var Config = sequelize.define("Config", {
    name: {type: DataTypes.STRING, allowNull:false, primaryKey: true},
    value: {type: DataTypes.STRING, allowNull:false},
  }, 
  {
    timestamps: false,
    paranoid: false,
    freezeTableName: true,
    tableName: 'config',
    classMethods: {}
  });

  return Config;
};