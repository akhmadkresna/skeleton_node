"use strict";

module.exports = function(sequelize, DataTypes) {
  var ListingAmenities = sequelize.define("ListingAmenities", {
    listingId: { type: DataTypes.INTEGER(11).UNSIGNED},
    amenityId: { type: DataTypes.INTEGER(11).UNSIGNED},
  }, 
  {
    timestamps: false,
    paranoid: true,
    freezeTableName: true,
    tableName: 'listingamenities',
  });

  return ListingAmenities;
};