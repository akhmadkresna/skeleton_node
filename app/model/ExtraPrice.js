"use strict";

module.exports = function(sequelize, DataTypes) {
  var ExtraPrice = sequelize.define("ExtraPrice", {
    listingId: { type: DataTypes.INTEGER(11).UNSIGNED, primaryKey: true },
    // startDate: {type: DataTypes.DATEONLY, allowNull:true},
    // endDate: {type: DataTypes.DATEONLY, allowNull:true},
    date: {type: DataTypes.DATEONLY, allowNull:false},
    price :{ type: DataTypes.INTEGER(11).UNSIGNED },
  }, 
  {
    timestamps: false,
    paranoid: false,
    freezeTableName: true,
    tableName: 'extraprice',
    classMethods: {
      associate: function(models) {
        ExtraPrice.belongsTo(models.Listing);
      }
    }
  });

  return ExtraPrice;
};