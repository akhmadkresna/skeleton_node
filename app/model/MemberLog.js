"use strict";

module.exports = function(sequelize, DataTypes) {
  var MemberLog = sequelize.define("MemberLog", {
    id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV1, primaryKey: true },
    userId: { type: DataTypes.INTEGER(11).UNSIGNED },
    action: { type: DataTypes.STRING},
    type: { type: DataTypes.INTEGER(11).UNSIGNED, allowNull:true}
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'memberlog',
    classMethods: {
      associate: function(models) {
        MemberLog.belongsTo(models.User);
      }
    }
  });

  return MemberLog;
};