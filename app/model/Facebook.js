"use strict";

module.exports = function(sequelize, DataTypes) {
  var Facebook = sequelize.define("Facebook", {
    userId: { type: DataTypes.INTEGER(11).UNSIGNED, primaryKey:true },
    facebookId: { type: DataTypes.BIGINT.UNSIGNED },
    facebookName: DataTypes.STRING,
    facebookToken: DataTypes.STRING,
    facebookSecret: DataTypes.STRING,
  }, 
  {
    timestamps: false,
    paranoid: false,
    freezeTableName: true,
    tableName: 'facebook',
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.User)
      }
    }
  });

  return Facebook;
};