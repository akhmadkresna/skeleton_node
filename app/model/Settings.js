"use strict";

module.exports = function(sequelize, DataTypes) {
  var Settings = sequelize.define("Settings", {
    userId: { type: DataTypes.INTEGER(11).UNSIGNED, primaryKey: true},
    notifApp: { type: DataTypes.BOOLEAN, defaultValue: 1},
    notifSMS: { type: DataTypes.BOOLEAN, defaultValue: 0},
    notifMail: { type: DataTypes.BOOLEAN, defaultValue: 1},
  }, 
  {
    timestamps: true,
    paranoid: false,
    freezeTableName: true,
    tableName: 'settings'
  });

  return Settings;
};