"use strict";
const  moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  var Reservation = sequelize.define("Reservation", {
    code: { type: DataTypes.STRING, primaryKey: true},
    userId: { type: DataTypes.INTEGER(11).UNSIGNED },
    ownerId: { type: DataTypes.INTEGER(11).UNSIGNED },
    listingId: { type: DataTypes.INTEGER(11).UNSIGNED },
    startDate: { type: DataTypes.DATEONLY},
    endDate: { type: DataTypes.DATEONLY},
    totalGuest: { type: DataTypes.INTEGER},
    cost: { type: DataTypes.INTEGER},
    approved: { type: DataTypes.BOOLEAN},
    paid: { type: DataTypes.BOOLEAN},
    canceled: { type: DataTypes.BOOLEAN},
    declined: { type: DataTypes.BOOLEAN},
    finished: { type: DataTypes.BOOLEAN},
    star: { type: DataTypes.INTEGER(11).UNSIGNED, defaultValue:null, allowNull:true },
    review: { type: DataTypes.STRING, defaultValue:null, allowNull:true },
    reviewDate: { type: DataTypes.DATEONLY, defaultValue:null, allowNull:true },
    status: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get: function() {
        if (this.canceled) { 
          return "Canceled";
        } else if (this.declined) { 
          return "Declined";
        } else if ( this.finished && this.reviewDate != null) {
          return "Completed";
        } else if ( this.finished ) {
          return "Finished";
        } else if ( this.approved && this.paid && (this.endDate <= Date.now()) && this.star == null ) {
          return "Finished";
        } else if ( this.approved && this.paid && (this.startDate <= Date.now())) {
          return "On-going";
        } else if (this.approved && this.paid && (this.startDate > Date.now()) ) {
          return "Paid";
        } else if (this.approved && !this.paid ) {
          return "Accepted";
        } else if (!this.approved) { 
          return "Pending";
        }
      }
    },
    startDateOnly: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get: function() {
        return moment(this.startDate).format('MMMM Do YYYY');
      }
    },
    endDateOnly: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get: function() {
        return moment(this.endDate).format('MMMM Do YYYY');
      }
    }
  }, 
  {
    timestamps: true,
    paranoid: true,
    freezeTableName: true,
    tableName: 'reservation',
    classMethods: {
      associate: function(models) {
        Reservation.belongsTo(models.User, {as: 'Traveller', foreignKey: 'userId'});
        Reservation.belongsTo(models.User, {as: 'Host', foreignKey: 'ownerId'});
        // Reservation.hasMany(models.Reservation, {as: 'Reviews'});
        Reservation.belongsTo(models.Listing);
        Reservation.hasMany(models.Payment, {foreignKey: 'reservationCode'});
        Reservation.hasOne(models.Review);
      }
    }
  });

  return Reservation;
};