function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, '-');
  str = str.toLowerCase();
  
  var from = "ÃÃ¡Ã¤Ã¢Ã¨Ã©Ã«ÃªÃ¬Ã­Ã¯Ã®Ã²Ã³Ã¶Ã´Ã¹ÃºÃ¼Ã»Ã±Ã§Â·/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '-')
  .replace(/\s+/g, '-')
  .replace(/-+/g, '-');

  return str;
}